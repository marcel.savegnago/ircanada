# -*- coding: utf-8 -*-
{
    'name': "email_check",

    'summary': """
        Check email against validate_email library""",

    'description': """
        Checks email against python validate_email library
    """,

    'author': "Cloudypedia",
    'website': "http://www.cloudypedia.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}