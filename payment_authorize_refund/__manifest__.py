# -*- coding: utf-8 -*-
# Copyright 2018 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

{
    'name': 'Payment Authorize Refund',
    'version': '1.0.0',
    'license': 'OPL-1',
    'description': """
Provides Authorize.net API for credit card refund in Odoo backend.
    """,
    'author': 'Sodexis, Inc',
    'website': 'http://www.sodexis.com',
    'depends': [
        'account_cancel',
        'payment_authorize',
        'payment_authorize_backend',
    ],
    'data': [
        'data/account_data.xml',
        'views/payment_transaction_view.xml',
        'views/account_payment_view.xml',
    ],
}
