# -*- coding: utf-8 -*-
# Copyright 2018 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from odoo import models, fields, api
from odoo.addons.payment_authorize.models.authorize_request import AuthorizeAPI
from odoo.tools.safe_eval import safe_eval


class PaymentTransaction(models.Model):
    _inherit = 'payment.transaction'

    refund_amount = fields.Float(
        digits=(16, 2),
        copy=False,
        help='Refund Amount',
    )
    refund_acquirer_reference = fields.Char(
        'Refund Acquirer Reference',
        copy=False,
        help='Reference of the TX as stored in the acquirer database',
    )

    @api.multi
    def action_authorize_void(self):
        for tx in self:
            tx.s2s_void_transaction()

    @api.multi
    def authorize_s2s_refund_transaction(self):
        self.ensure_one()
        transaction = AuthorizeAPI(self.acquirer_id)
        tree = transaction.credit(
            self.payment_token_id,
            self.refund_amount,
            self.acquirer_reference,
        )
        return self._authorize_s2s_validate_tree(tree)

    @api.multi
    def _authorize_s2s_validate(self, tree):
        self.ensure_one()
        if self.state != 'done':
            return super(PaymentTransaction,
                         self)._authorize_s2s_validate(tree)
        status_code = int(tree.get('x_response_code', '0'))
        if status_code == self._authorize_valid_tx_status:
            if tree.get('x_type').lower() == 'void':
                self.write({
                    'state': 'cancel',
                })
            if tree.get('x_type').lower() == 'refund':
                self.write({
                    'state': 'refunded',
                    'refund_acquirer_reference': tree.get('x_trans_id'),
                })
            return True
        else:
            error = tree.get('x_response_reason_text')
            if tree.get('x_type').lower() == 'void':
                self.write({
                    'state': 'error',
                    'state_message': error,
                })
            elif tree.get('x_type').lower() == 'refund':
                self.write({
                    'state': 'error',
                    'state_message': error,
                    'refund_acquirer_reference': tree.get('x_trans_id'),
                })
            else:
                self.write({
                    'state': 'error',
                    'state_message': error,
                    'acquirer_reference': tree.get('x_trans_id'),
                })
            return False
