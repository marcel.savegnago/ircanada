# -*- coding: utf-8 -*-
# Copyright 2018 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from odoo import models, api


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None,
                        date=None, description=None, journal_id=None):
        vals = super(AccountInvoice, self)._prepare_refund(
            invoice, date_invoice=date_invoice, date=date,
            description=description, journal_id=journal_id)
        transactions = self.env['payment.transaction'].search(
            [('invoice_id', '=', invoice.id),
             ('state', 'not in', ['cancel', 'refunding', 'refunded'])],
        )
        vals.update({
            'payment_method_id': invoice.payment_method_id.id,
            'payment_acquirer_id': invoice.payment_acquirer_id.id,
            'payment_token_id': invoice.payment_token_id.id,
            'payment_tx_id': transactions and transactions[0].id or invoice.payment_tx_id.id,
        })
        return vals
