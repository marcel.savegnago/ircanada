# -*- coding: utf-8 -*-
# Copyright 2018 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from lxml import etree

from odoo.addons.payment_authorize.models.authorize_request import AuthorizeAPI, error_check


def credit(self, token, amount, transaction_id):
    """ Refund a payment for the given amount.

    :param record token: the payment.token record that must be refunded.
    :param str amount: transaction amount
    :param str transaction_id: the reference of the transacation that is going to be refunded.

    :return: a dict containing the response code, transaction id and transaction type
    :rtype: dict
    """
    root = self._base_tree('createTransactionRequest')
    tx = etree.SubElement(root, "transactionRequest")
    etree.SubElement(tx, "transactionType").text = "refundTransaction"
    etree.SubElement(tx, "amount").text = str(amount)
    payment = etree.SubElement(tx, "payment")
    credit_card = etree.SubElement(payment, "creditCard")
    idx = token.name.find(' - ')
    etree.SubElement(credit_card, "cardNumber").text = token.name[idx-4:idx] # shitty hack, but that's the only way to get the 4 last digits
    etree.SubElement(credit_card, "expirationDate").text = "XXXX"
    etree.SubElement(tx, "refTransId").text = transaction_id
    response = self._authorize_request(root)
    res = dict()
    err_code = response.find('transactionResponse/errors/error/errorCode')
    if err_code is not None:
        code = response.find('transactionResponse/errors/error/errorCode').text
        text = response.find('transactionResponse/errors/error/errorText').text
        res['x_response_reason_text'] =  "Error code ({0}) Error message: {1}".format(code,text)
    res['x_response_code'] = response.find(
        'transactionResponse/responseCode').text
    res['x_trans_id'] = response.find('transactionResponse/transId').text
    res['x_type'] = 'refund'
    return res

def void(self, transaction_id):
    """Void a previously authorized payment.

    :param str transaction_id: the id of the authorized transaction in the
                               Authorize.net backend

    :return: a dict containing the response code, transaction id and transaction type
    :rtype: dict
    """
    root = self._base_tree('createTransactionRequest')
    tx = etree.SubElement(root, "transactionRequest")
    etree.SubElement(tx, "transactionType").text = "voidTransaction"
    etree.SubElement(tx, "refTransId").text = transaction_id
    response = self._authorize_request(root)
    res = dict()
    (has_error, error_msg) = error_check(response)
    if has_error:
        res['x_response_code'] = self.AUTH_ERROR_STATUS
        res['x_response_reason_text'] = error_msg
        res['x_type'] = 'void'
        return res
    res['x_response_code'] = response.find('transactionResponse/responseCode').text
    res['x_trans_id'] = response.find('transactionResponse/transId').text
    res['x_type'] = 'void'
    return res

AuthorizeAPI.credit = credit
AuthorizeAPI.void = void
