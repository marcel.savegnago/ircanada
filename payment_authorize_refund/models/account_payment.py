# -*- coding: utf-8 -*-
# Copyright 2018 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools import float_compare

import logging
_logger = logging.getLogger(__name__)


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.model
    def _default_refund_payment_transaction_id(self):
        transaction = False
        if self._context.get('active_ids') and self._context.get('active_model') == 'account.invoice':
            invoice_ids = self._context.get('active_ids', [])
            for invoice in self.env['account.invoice'].browse(invoice_ids):
                if invoice.type == 'out_refund':
                    transaction = invoice.payment_tx_id
                    if transaction.state in ['cancel', 'refunding', 'refunded','error']:
                        transaction = self.env['payment.transaction'].search(
                            [('partner_id', 'child_of', invoice.partner_id.commercial_partner_id.id),
                             ('state', '=', 'done')],
                            limit=1,
                        )
        return transaction

    partner_id = fields.Many2one(
        'res.partner'
    )
    refund_payment_transaction_id = fields.Many2one(
        'payment.transaction',
        'Refund Transaction',
        default=_default_refund_payment_transaction_id,
        copy=False,
    )

    @api.multi
    def cancel(self):
        for payment in self:
            super(AccountPayment, payment).cancel()
            try:
                if payment.payment_transaction_id:
                    payment.payment_transaction_id.action_authorize_void()
            except ValidationError as e:
                _logger.info('Authorize.net error: %s' % e)
                raise ValidationError(
                    'Transaction is already Settled in Authorize.net!\nInitiate Refund process'
                )

    @api.multi
    def post(self):
        for payment in self:
            if payment.payment_transaction_id.state in ['cancel', 'refunding', 'refunded']:
                payment.payment_transaction_id = False
                payment._do_payment()
            super(AccountPayment, payment).post()
            if payment.payment_type == 'outbound' and payment.refund_payment_transaction_id:
                company_currency = payment.journal_id.company_id.currency_id
                if float_compare(
                        payment.refund_payment_transaction_id.amount,
                        payment.amount,
                        precision_digits=company_currency.rounding) < 0:
                    raise UserError(
                        'Payment amount exceeds the Refund Transaction amount')
                payment.refund_payment_transaction_id.write({
                    'refund_amount': payment.amount,
                })
                if self._context.get('active_ids', []):
                    self.env['account.invoice'].browse(self._context.get('active_ids', [])).write({
                        'payment_tx_id': payment.refund_payment_transaction_id.id,
                        'payment_method_id': payment.journal_id.id,
                        'payment_token_id': payment.refund_payment_transaction_id.payment_token_id.id,
                    })
                if not payment.refund_payment_transaction_id.acquirer_reference:
                    raise ValidationError(_("The Acquirer reference should not be empty."))
                payment.refund_payment_transaction_id.authorize_s2s_refund_transaction()
                if payment.refund_payment_transaction_id.state != 'refunded':
                    raise ValidationError(_("Authorize.net error message: \n {0} ".format(payment.refund_payment_transaction_id.state_message)))
