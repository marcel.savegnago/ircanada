# -*- coding: utf-8 -*-
# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


from odoo import api, fields, models, _
from odoo.exceptions import ValidationError



class ResPartner(models.Model):
    _inherit = 'res.partner'

    full_address = fields.Char(
        string='Full Address',
        compute='_full_address')
    
    eligible_donor = fields.Boolean(
        string='Eligible Donor (Name/Address)',
        compute='_eligible_donor')
    
    manual_eligible_donor = fields.Boolean(
        string='Eligible Donor (Name/Address)',
        track_visibility='onchange')
    
    manual_override = fields.Boolean(
        string='Manually Override Invalid Name/Address',
        track_visibility='onchange')
    
    @api.multi
    def write(self, values):
        partner = super(ResPartner, self).write(values)
        if 'manual_override' in values:
            if values['manual_override']:
                msg_body = 'Manual override is activated for invalid name and address field'
            else:
                msg_body = 'Manual override is cancelled for invalid name and address field'
            self.message_post(body=msg_body)
        return partner
        

    
    @api.multi
    def _full_address(self):
        for donor in self:
            full_address = ""
            if donor.street:
                full_address = donor.street
            
            if donor.street2:
                if full_address:
                    full_address += " "+donor.street2
                else:
                    full_address += donor.street2
            
            if donor.city:
                if full_address:
                    full_address += " "+donor.city
                else:
                    full_address += donor.city
            
            if donor.state_id.code:
                if full_address:
                    full_address += " "+donor.state_id.code
                else:
                    full_address += donor.state_id.code
            
            if donor.zip:
                if full_address:
                    full_address += " "+donor.zip
                else:
                    full_address += donor.zip
            
            if donor.country_id.name:
                if full_address:
                    full_address += " "+donor.country_id.name
                else:
                    full_address += donor.country_id.name
            
            if not full_address:
                full_address += "Donor: " + donor.code + " Missing Address"
            
            donor.full_address = full_address
    
    @api.multi
    def _eligible_donor(self):
        for donor in self:
            eligible = False
            donor_types = ['Registered Charity','Federal Government','Provincial Government','Municipal Government','Other Organization']

            if not donor.manual_override and donor.x_studio_field_UsVrp not in donor_types:
                if donor.is_company:
                    if (donor.name and donor.street and donor.city and donor.state_id and donor.zip and donor.country_id):
                        eligible = True
                else:
                    if (donor.firstname and donor.lastname and donor.street and donor.city and donor.state_id and donor.zip and donor.country_id):
                        eligible = True
            donor.eligible_donor = eligible

    @api.multi
    def action_view_invoice(self):
        invoices = self.env['account.invoice'].search([('partner_id','in',self.ids)])
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) >= 1:
            action['domain'] = [('id', 'in', invoices.ids)]
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]            
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action
    

    @api.multi
    @api.constrains('firstname', 'lastname')
    def _name_length_check(self):
        for partner in self:
            if (partner.firstname and len(partner.firstname) <= 1) or (partner.lastname and len(partner.lastname) <= 1):
                raise ValidationError(_('First name / Last name must be greater than one character')) 