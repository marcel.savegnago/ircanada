# -*- coding: utf-8 -*-
# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from . import account
from . import partner
from . import helpdesk
from . import subscription
from . import res_config_settings
