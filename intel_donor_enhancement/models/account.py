# -*- coding: utf-8 -*-
# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


from odoo import api, fields, models, _
import datetime
from werkzeug import url_encode
from odoo.exceptions import ValidationError, Warning
from dateutil.relativedelta import relativedelta
import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    sale_subscription_count = fields.Integer(compute='_compute_sale_subscription_count')
    
    donor_log_ids = fields.One2many(
        comodel_name='intel.change.donor.log',
        inverse_name='donation_id',
        string='Changed Donor')

    def copy(self, default=None):
        default = default or {}
        default['name'] = self.name
        default['payment_method_id'] = self.payment_method_id.id
        default['payment_token_id'] = self.payment_token_id.id
        default['payment_acquirer_id'] = self.payment_acquirer_id.id
        return super(AccountInvoice, self).copy(default=default)
    
    def _compute_sale_subscription_count(self):
        for invoice in self:
            invoice.sale_subscription_count = len(self.mapped('invoice_line_ids.subscription_id.id'))

    @api.multi
    def action_view_pledge(self):
        self.ensure_one()
        subscriptions = self.invoice_line_ids.mapped('subscription_id')
        action = self.env.ref('sale_subscription.sale_subscription_action').read()[0]
        if len(subscriptions) > 1:
            action['domain'] = [('id', 'in', subscriptions.ids)]
        elif len(subscriptions) == 1:
            action['views'] = [(self.env.ref('sale_subscription.sale_subscription_view_form').id, 'form')]
            action['res_id'] = subscriptions.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action
    
    @api.multi
    def action_change_donor(self):
        self.ensure_one()
        return {
            'name': _('Change Donor'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'intel.invoice.donor',
            'views': [(self.env.ref('intel_donor_enhancement.view_intel_invoice_donor').id, 'form')],
            'view_id': self.env.ref('intel_donor_enhancement.view_intel_invoice_donor').id,
            'target': 'new',
        }
    

class ChangeDonorLog(models.Model):
    _name = 'intel.change.donor.log'
    _description = 'Donor Change logs'
    _order = 'create_date DESC'

    donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='Old Donor',
        required=True)
    new_donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='New Donor',
        required=True)
    reason = fields.Text(
        string='Reason for change',
        required=True)
    relationship_id = fields.Many2one(
        comodel_name='res.partner.relation.type.selection',
        string='Set Relationship (Optional)')
    donation_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Donation',
        required=True)


class AccountPayment(models.Model):
    _inherit = "account.payment"

    def action_validate_invoice_payment(self):
        super(AccountPayment, self).action_validate_invoice_payment()
        default_invoice_ids = self._context.get('default_invoice_ids', False)
        if default_invoice_ids:
            invoice_id = default_invoice_ids[0][1]
            invoice_line = self.env['account.invoice.line'].search([('invoice_id','=',invoice_id)],limit=1)
            if invoice_line:
                imd_res = self.env['ir.model.data']
                template_res = self.env['mail.template']
                email_context = self.env.context.copy()
                email_context.update({
                    'email_to': invoice_line.invoice_id.partner_id.email,
                })
                if not invoice_line.invoice_id.partner_id.email:
                    _logger.info('Invoice id %s has no donor email.'%invoice_line.invoice_id.id)
                    return True
                if invoice_line.invoice_id.type == 'out_refund':
                    a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_refund')
                    msg_body = 'Donation Refund'
                    template = template_res.browse(template_id)
                    template.with_context(email_context).send_mail(invoice_id)
                    msg_body += ' E-mail sent to donor.'
                elif invoice_line.account_id:
                    if invoice_line.account_id.id in invoice_line.invoice_id.company_id.eligible_donation_account_ids.ids:
                        a, template_id = imd_res.get_object_reference('sale_subscription', 'email_payment_success')
                        msg_body = 'Donation Acknowledgement'
                        template = template_res.browse(template_id)
                        template.with_context(email_context).send_mail(invoice_id)
                        msg_body += ' E-mail sent to donor.'
                        invoice_line.invoice_id.message_post(body=msg_body)
                    elif invoice_line.account_id.id in invoice_line.invoice_id.company_id.eligible_non_donation_account_ids.ids:
                        a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_non_donation')
                        msg_body = 'Non-Donation'
                        template = template_res.browse(template_id)
                        template.with_context(email_context).send_mail(invoice_id)
                        msg_body += ' E-mail invoice sent.'
                        invoice_line.invoice_id.message_post(body=msg_body)
            return True



    
