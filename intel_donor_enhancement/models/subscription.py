# -*- coding: utf-8 -*-
# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


from odoo import api, fields, models
import datetime
import time
from odoo.exceptions import ValidationError, UserError
import uuid
import traceback
import logging

from collections import Counter
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import format_date

from odoo.addons import decimal_precision as dp

_logger = logging.getLogger(__name__)


class SaleSubscription(models.Model):
    _inherit = 'sale.subscription'

    makeup_payment_ids = fields.One2many(
        comodel_name='intel.makeup.payment',
        inverse_name='subscription_id',
        string='Makeup Payment(s)')
    
    donor_log_ids = fields.One2many(
        comodel_name='intel.change.donor.subscription.log',
        inverse_name='subscription_id',
        string='Donor Change Logs')
    
    state = fields.Selection(selection_add=[('failed', 'Failed')])

    @api.multi
    def action_change_donor(self):
        self.ensure_one()
        return {
            'name': _('Change Donor'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'intel.subscription.donor',
            'views': [(self.env.ref('intel_donor_enhancement.view_intel_subscription_donor').id, 'form')],
            'view_id': self.env.ref('intel_donor_enhancement.view_intel_subscription_donor').id,
            'target': 'new',
        }
    
    def set_open(self):
        recurring_next_date = datetime.datetime.strptime(self.recurring_next_date, '%Y-%m-%d')
        today = datetime.datetime.strptime(fields.Date.today(), '%Y-%m-%d')
        if recurring_next_date >= today:
            return super(SaleSubscription, self).set_open()
        raise UserError(_('You cannot change the pledge to start recurring because the date of next invoice is less than today.'))

    @api.multi
    def reconcile_pending_transaction(self, tx, invoice=False):
        self.ensure_one()
        if not invoice:
            invoice = tx.invoice_id
        if tx.state in ['done', 'authorized']:
            invoice.write({'reference': tx.reference, 'name': tx.reference})
            if tx.acquirer_id.journal_id and tx.state == 'done':
                invoice.action_invoice_open()
                journal = tx.acquirer_id.journal_id
                invoice.with_context(default_ref=tx.reference, default_currency_id=tx.currency_id.id).pay_and_reconcile(journal, pay_amount=tx.amount)
            if not self.env.context.get('makeup_payment', False):
                self.increment_period()
            #self.write({'state': 'open', 'date': False})
        else:
            invoice.action_cancel()
            invoice.unlink()
    
    @api.model
    def cron_account_analytic_account(self):
        today = fields.Date.today()
        next_month = fields.Date.to_string(fields.Date.from_string(today) + relativedelta(months=1))

        # set to pending if date is in less than a month
        """domain_pending = [('date', '<', next_month), ('state', '=', 'open')]
        subscriptions_pending = self.search(domain_pending)
        subscriptions_pending.write({'state': 'pending'})"""

        # set to close if data is passed
        domain_close = [('date', '<', today), ('state', 'in', ['pending', 'open'])]
        subscriptions_pending = self.search(domain_close)
        subscriptions_pending.write({'state': 'pending'})

        return dict(pending=subscriptions_pending.ids)
    
    @api.multi
    def _recurring_create_invoice(self, automatic=False):
        auto_commit = self.env.context.get('auto_commit', True)
        cr = self.env.cr
        invoices = self.env['account.invoice']
        current_date = time.strftime('%Y-%m-%d')
        imd_res = self.env['ir.model.data']
        template_res = self.env['mail.template']

        self.cron_account_analytic_account()
        cr.commit()
        
        makeup_payments = self.env['intel.makeup.payment'].search([('payment_date','=',current_date),('state','=','in_progress')])
        makeup_subscriptions = makeup_payments.filtered(lambda s:s.subscription_id.state not in ('draft','failed','closed')).mapped('subscription_id')
        _logger.info('Makeup payment and subscription')
        
        if makeup_subscriptions:
            sub_data = makeup_subscriptions.read(fields=['id', 'company_id'])
            for company_id in set(data['company_id'][0] for data in sub_data):
                sub_ids = [s['id'] for s in sub_data if s['company_id'][0] == company_id]
                subs = self.with_context(company_id=company_id, force_company=company_id).browse(sub_ids)
                context_company = dict(self.env.context, company_id=company_id, force_company=company_id)
                for subscription in subs:
                    mmp = self.env['intel.makeup.payment'].search([('state','=','in_progress'),('payment_date','=',current_date),('subscription_id','=',subscription.id)],limit=1)
                    #if automatic and auto_commit:
                    #    cr.commit()
                    # payment + invoice (only by cron)
                    if subscription.template_id.payment_mandatory and subscription.recurring_total and automatic:
                        try:
                            failed_transaction = False
                            payment_token = subscription.payment_token_id
                            tx = None
                            if payment_token:
                                invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                                new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                                if mmp:
                                    new_invoice.write({'x_studio_field_XJgdz':mmp.donation_date})
                                new_invoice.message_post_with_view('mail.message_origin_link',
                                    values = {'self': new_invoice, 'origin': subscription},
                                    subtype_id = self.env.ref('mail.mt_note').id)
                                new_invoice.with_context(context_company).compute_taxes()
                                tx = subscription.with_context({'makeup_payment':True})._do_payment(payment_token, new_invoice, two_steps_sec=False)[0][0]
                                # commit change as soon as we try the payment so we have a trace somewhere
                                #if auto_commit:
                                #    cr.commit()
                                if tx.state in ['done', 'authorized']:
                                    subscription.send_success_mail(tx, new_invoice)
                                    msg_body = 'Automatic makeup payment succeeded. Payment reference: <a href=# data-oe-model=payment.transaction data-oe-id=%d>%s</a>; Amount: %s. Invoice <a href=# data-oe-model=account.invoice data-oe-id=%d>View Invoice</a>.' % (tx.id, tx.reference, tx.amount, new_invoice.id)
                                    subscription.message_post(body=msg_body)
                                    if mmp:
                                        mmp.write({'state':'complete'})
                                    #if auto_commit:
                                    #    cr.commit()
                                else:
                                    _logger.error('Failed to create recurring invoice for subscription %s', subscription.code)
                                    #if auto_commit:
                                    #    cr.rollback()
                                    new_invoice.unlink()
                                    failed_transaction = True
                            if tx is None or tx.state != 'done':
                                failed_transaction = True
                                amount = subscription.recurring_total
                                date_close = datetime.datetime.strptime(subscription.recurring_next_date, "%Y-%m-%d") + relativedelta(days=15)
                                close_subscription = current_date >= date_close.strftime('%Y-%m-%d')
                                email_context = self.env.context.copy()
                                email_context.update({
                                    'payment_token': subscription.payment_token_id and subscription.payment_token_id.name,
                                    'renewed': False,
                                    'total_amount': amount,
                                    'email_to': subscription.partner_id.email,
                                    'code': subscription.code,
                                    'currency': subscription.pricelist_id.currency_id.name,
                                    'date_end': subscription.date,
                                    'date_close': date_close.date()
                                })
                                if close_subscription:
                                    if subscription.partner_id.email:
                                        _, template_id = imd_res.get_object_reference('sale_subscription', 'email_payment_close')
                                        template = template_res.browse(template_id)
                                        template.with_context(email_context).send_mail(subscription.id)
                                    _logger.debug("Sending Subscription Closure Mail to %s for subscription %s and closing subscription", subscription.partner_id.email, subscription.id)
                                    msg_body = 'Automatic makeup payment failed after multiple attempts. Subscription closed automatically.'
                                    subscription.message_post(body=msg_body)
                                else:
                                    _, template_id = imd_res.get_object_reference('sale_subscription', 'email_payment_reminder')
                                    msg_body = 'Automatic makeup payment failed. Subscription set to "Failed".'
                                    #if (datetime.datetime.today() - datetime.datetime.strptime(subscription.recurring_next_date, '%Y-%m-%d')).days in [0, 3, 7, 14]:
                                    if subscription.partner_id.email:
                                        template = template_res.browse(template_id)
                                        template.with_context(email_context).send_mail(subscription.id)
                                    _logger.debug("Sending Makeup Payment Failure Mail to %s for subscription %s and setting subscription to pending", subscription.partner_id.email, subscription.id)
                                    msg_body += ' E-mail sent to customer.'
                                    subscription.message_post(body=msg_body)
                                if failed_transaction:
                                    subscription.write({'state': 'failed'})
                                    if mmp:
                                        mmp.write({'state':'failed'})
                                else:
                                    subscription.write({'state': 'close' if close_subscription else 'pending'})
                            #if auto_commit:
                            cr.commit()
                        except Exception:
                            #if auto_commit:
                            cr.rollback()
                            # we assume that the payment is run only once a day
                            traceback_message = traceback.format_exc()
                            _logger.error(traceback_message)
                            if mmp:
                                mmp.write({'state':'failed'})
                            last_tx = self.env['payment.transaction'].search([('reference', 'like', 'SUBSCRIPTION-%s-%s' % (subscription.id, datetime.date.today().strftime('%y%m%d')))], limit=1)
                            error_message = "Error during renewal of subscription %s (%s)" % (subscription.code, 'Payment recorded: %s' % last_tx.reference if last_tx and last_tx.state == 'done' else 'No payment recorded.')
                            _logger.error(error_message)

                    # invoice only
                    else:
                        try:
                            invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                            new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                            if mmp:
                                new_invoice.write({'x_studio_field_XJgdz':mmp.donation_date})
                            new_invoice.message_post_with_view('mail.message_origin_link',
                                values = {'self': new_invoice, 'origin': subscription},
                                subtype_id = self.env.ref('mail.mt_note').id)
                            new_invoice.with_context(context_company).compute_taxes()
                            invoices += new_invoice
                            next_date = datetime.datetime.strptime(subscription.recurring_next_date or current_date, "%Y-%m-%d")
                            periods = {'daily': 'days', 'weekly': 'weeks', 'monthly': 'months', 'yearly': 'years'}
                            invoicing_period = relativedelta(**{periods[subscription.recurring_rule_type]: subscription.recurring_interval})
                            new_date = next_date + invoicing_period
                            subscription.write({'recurring_next_date': new_date.strftime('%Y-%m-%d')})
                            if mmp:
                                mmp.write({'state':'complete'})
                            #if automatic and auto_commit:
                            cr.commit()
                        except Exception:
                            #if automatic and auto_commit:
                            cr.rollback()
                            if mmp:
                                mmp.write({'state':'failed'})
                            _logger.exception('Fail to create recurring invoice for subscription %s', subscription.code)
                
        
        if len(self) > 0:
            subscriptions = self.filtered(lambda s: s.state in ('open'))
        else:
            domain = [('recurring_next_date', '<=', current_date),
                      ('state', 'in', ['open'])]
            subscriptions = self.search(domain)
           
        if subscriptions:
            sub_data = subscriptions.read(fields=['id', 'company_id'])
            for company_id in set(data['company_id'][0] for data in sub_data):
                sub_ids = [s['id'] for s in sub_data if s['company_id'][0] == company_id]
                subs = self.with_context(company_id=company_id, force_company=company_id).browse(sub_ids)
                context_company = dict(self.env.context, company_id=company_id, force_company=company_id)
                for subscription in subs:
                    #if automatic and auto_commit:
                    #    cr.commit()
                    # payment + invoice (only by cron)
                    if subscription.template_id.payment_mandatory and subscription.recurring_total and automatic:
                        try:
                            failed_transaction = False
                            payment_token = subscription.payment_token_id
                            tx = None
                            if payment_token:
                                invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                                new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                                new_invoice.message_post_with_view('mail.message_origin_link',
                                    values = {'self': new_invoice, 'origin': subscription},
                                    subtype_id = self.env.ref('mail.mt_note').id)
                                new_invoice.with_context(context_company).compute_taxes()
                                tx = subscription._do_payment(payment_token, new_invoice, two_steps_sec=False)[0][0]
                                # commit change as soon as we try the payment so we have a trace somewhere
                                #if auto_commit:
                                #    cr.commit()
                                _logger.info(tx)
                                if tx.state in ['done', 'authorized']:
                                    subscription.send_success_mail(tx, new_invoice)
                                    msg_body = 'Automatic payment succeeded. Payment reference: <a href=# data-oe-model=payment.transaction data-oe-id=%d>%s</a>; Amount: %s. Invoice <a href=# data-oe-model=account.invoice data-oe-id=%d>View Invoice</a>.' % (tx.id, tx.reference, tx.amount, new_invoice.id)
                                    subscription.message_post(body=msg_body)
                                    #if auto_commit:
                                    #    cr.commit()
                                else:
                                    _logger.error('Fail to create recurring invoice for subscription %s', subscription.code)
                                    #if auto_commit:
                                    #    cr.rollback()
                                    new_invoice.unlink()
                                    failed_transaction = True
                            if tx is None or tx.state != 'done':
                                failed_transaction = True
                                amount = subscription.recurring_total
                                date_close = datetime.datetime.strptime(subscription.recurring_next_date, "%Y-%m-%d") + relativedelta(days=15)
                                close_subscription = current_date >= date_close.strftime('%Y-%m-%d')
                                email_context = self.env.context.copy()
                                email_context.update({
                                    'payment_token': subscription.payment_token_id and subscription.payment_token_id.name,
                                    'renewed': False,
                                    'total_amount': amount,
                                    'email_to': subscription.partner_id.email,
                                    'code': subscription.code,
                                    'currency': subscription.pricelist_id.currency_id.name,
                                    'date_end': subscription.date,
                                    'date_close': date_close.date()
                                })
                                if close_subscription:
                                    _, template_id = imd_res.get_object_reference('sale_subscription', 'email_payment_close')
                                    if subscription.partner_id.email:
                                        template = template_res.browse(template_id)
                                        template.with_context(email_context).send_mail(subscription.id)
                                    _logger.debug("Sending Subscription Closure Mail to %s for subscription %s and closing subscription", subscription.partner_id.email, subscription.id)
                                    msg_body = 'Automatic payment failed after multiple attempts. Subscription closed automatically.'
                                    subscription.message_post(body=msg_body)
                                else:
                                    _, template_id = imd_res.get_object_reference('sale_subscription', 'email_payment_reminder')
                                    msg_body = 'Automatic payment failed. Subscription set to "To Failed".'
                                    #if (datetime.datetime.today() - datetime.datetime.strptime(subscription.recurring_next_date, '%Y-%m-%d')).days in [0, 3, 7, 14]:
                                    if subscription.partner_id.email:
                                        template = template_res.browse(template_id)
                                        template.with_context(email_context).send_mail(subscription.id)
                                    _logger.debug("Sending Payment Failure Mail to %s for subscription %s and setting subscription to pending", subscription.partner_id.email, subscription.id)
                                    msg_body += ' E-mail sent to customer.'
                                    subscription.message_post(body=msg_body)
                                if failed_transaction:
                                    subscription.write({'state': 'failed'})
                                else:
                                    subscription.write({'state': 'close' if close_subscription else 'pending'})
                            #if auto_commit:
                            cr.commit()
                        except Exception:
                            #if auto_commit:
                            cr.rollback()
                            # we assume that the payment is run only once a day
                            traceback_message = traceback.format_exc()
                            _logger.error(traceback_message)
                            last_tx = self.env['payment.transaction'].search([('reference', 'like', 'SUBSCRIPTION-%s-%s' % (subscription.id, datetime.date.today().strftime('%y%m%d')))], limit=1)
                            error_message = "Error during renewal of subscription %s (%s)" % (subscription.code, 'Payment recorded: %s' % last_tx.reference if last_tx and last_tx.state == 'done' else 'No payment recorded.')
                            _logger.error(error_message)

                    # invoice only
                    else:
                        try:
                            invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                            new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                            new_invoice.message_post_with_view('mail.message_origin_link',
                                values = {'self': new_invoice, 'origin': subscription},
                                subtype_id = self.env.ref('mail.mt_note').id)
                            new_invoice.with_context(context_company).compute_taxes()
                            invoices += new_invoice
                            next_date = datetime.datetime.strptime(subscription.recurring_next_date or current_date, "%Y-%m-%d")
                            periods = {'daily': 'days', 'weekly': 'weeks', 'monthly': 'months', 'yearly': 'years'}
                            invoicing_period = relativedelta(**{periods[subscription.recurring_rule_type]: subscription.recurring_interval})
                            new_date = next_date + invoicing_period
                            subscription.write({'recurring_next_date': new_date.strftime('%Y-%m-%d')})
                            #if automatic and auto_commit:
                            cr.commit()
                        except Exception:
                            #if automatic and auto_commit:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for subscription %s', subscription.code)
        
        today = fields.Date.today()
        domain_close = [('date', '<=', today), ('state', 'in', ['pending', 'open'])]
        subscriptions_pending = self.search(domain_close)
        subscriptions_pending.write({'state': 'pending'})
        return invoices
    
    def send_success_mail(self, tx, invoice):
        _logger.info('SEND SUCCESS MAIL')
        imd_res = self.env['ir.model.data']
        template_res = self.env['mail.template']
        current_date = time.strftime('%Y-%m-%d')
        next_date = datetime.datetime.strptime(self.recurring_next_date or current_date, "%Y-%m-%d")
        periods = {'daily': 'days', 'weekly': 'weeks', 'monthly': 'months', 'yearly': 'years'}
        invoicing_period = relativedelta(**{periods[self.recurring_rule_type]: self.recurring_interval})
        new_date = next_date + invoicing_period
        if self.recurring_rule_type == 'daily':
            a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_daily_recurring_success')
        elif self.recurring_rule_type == 'weekly':
            a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_weekly_recurring_success')
        elif self.recurring_rule_type == 'monthly':
            a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_monthly_recurring_success')
        elif self.recurring_rule_type == 'yearly':
            a, template_id = imd_res.get_object_reference('intel_donor_enhancement', 'email_payment_yearly_recurring_success')
        _logger.info('SEND SUCCESS MAIL 2')
        email_context = self.env.context.copy()
        email_context.update({
            'payment_token': self.payment_token_id.name,
            'renewed': True,
            'total_amount': tx.amount,
            'next_date': new_date.date(),
            'previous_date': self.recurring_next_date,
            'email_to': self.partner_id.email,
            'code': self.code,
            'currency': self.pricelist_id.currency_id.name,
            'date_end': self.date,
        })
        _logger.debug("Sending Payment Confirmation Mail to %s for subscription %s", self.partner_id.email, self.id)
        template = template_res.browse(template_id)
        _logger.info('SEND SUCCESS MAIL 3')
        if self.partner_id.email:
            _logger.info('SEND SUCCESS MAIL 4')
            template.with_context(email_context).send_mail(invoice.id)
        return True

class MakeupPayment(models.Model):
    _name = 'intel.makeup.payment'
    _description = 'Makeup Payments'

    donation_date = fields.Date(
        string='Donation Date', 
        required=True)
    payment_date = fields.Date(
        string='Date of Payment', 
        required=True)
    notes = fields.Text(
        string='Notes')
    subscription_id = fields.Many2one(
        comodel_name='sale.subscription',
        string='Subscription')
    state = fields.Selection(
        [('in_progress','In Progress'),('complete','Complete'),('pause','Pause'),('failed','Failed')], 
        default='in_progress',
        string='Status')
    donor_id = fields.Many2one(
        related='subscription_id.partner_id', 
        store=True)
    donor_code = fields.Char(
        related='donor_id.code', 
        store=True)
    
    _sql_constraints = [('intel_makeup_payment_uniq', 'unique(subscription_id,payment_date)', 'Each makeup payment date in a pledge must be unique.')]
    
    @api.multi
    def pause(self):
        self.write({'state':'pause'})
    
    @api.multi
    def in_progress(self):
        self.write({'state':'in_progress'})
    
    @api.onchange('donation_date', 'payment_date')
    def _check_date(self):
        current_date = datetime.date.today()
        if self.donation_date:
            donation_date = self.donation_date.split('-')
            donation_date = datetime.date(int(donation_date[0]), int(donation_date[1]), int(donation_date[2]))
            if donation_date >= current_date:
                self.donation_date = False
                raise ValidationError('Donation date must be prior to today.')
        
        if self.payment_date:
            payment_date = self.payment_date.split('-')
            payment_date = datetime.date(int(payment_date[0]), int(payment_date[1]), int(payment_date[2]))
            if payment_date <= current_date:
                self.payment_date = False
                raise ValidationError('Payment date must be a date after today.')
    
    @api.multi
    def unlink(self):
        for payment in self:
            if payment.state in ['complete']:
                raise UserError(_('You cannot delete a makeup payment is in %s state.') % (payment.state,))
        return super(MakeupPayment, self).unlink()
    
    
class ChangeDonorSubscriptionLog(models.Model):
    _name = 'intel.change.donor.subscription.log'
    _description = 'Donor Change logs'
    _order = 'create_date DESC'

    donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='Old Donor',
        required=True)
    new_donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='New Donor',
        required=True)
    reason = fields.Text(
        string='Reason for change',
        required=True)
    relationship_id = fields.Many2one(
        comodel_name='res.partner.relation.type.selection',
        string='Set Relationship (Optional)')
    subscription_id = fields.Many2one(
        comodel_name='sale.subscription',
        string='Pledge',
        required=True)