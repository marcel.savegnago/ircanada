# -*- coding: utf-8 -*-
# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


from odoo import api, fields, models

class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'

    department_ids = fields.Many2many(
        'account.analytic.tag', 'intel_donor_account_analytic_tag_rel', 'ticket_id', 'department_id',
        string='Departments')
    donor_phone = fields.Char(related='partner_id.phone')
    donor_street = fields.Char(related='partner_id.street')
    donor_city = fields.Char(related='partner_id.city')
    donor_zip = fields.Char(related='partner_id.zip')
    donor_state_id = fields.Many2one(
        comodel_name='res.country.state',
        related='partner_id.state_id')
    donor_country_id = fields.Many2one(
        comodel_name='res.country',
        related='partner_id.country_id')
    donation_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Donation')
    donation_trace = fields.Char(related='donation_id.x_studio_field_7PrQt.x_name')
    donation_appeal = fields.Char(related='donation_id.team_id.name')
    donation_type = fields.Char(related='donation_id.x_studio_field_ZdhFd.x_name')
    donation_amount = fields.Monetary(related='donation_id.amount_total', currency_field='currency_id')
    pledge_id = fields.Many2one(
        comodel_name='sale.subscription',
        string='Pledge')
    pledge_trace = fields.Char(related='pledge_id.x_studio_field_Ad3LC.x_name')
    pledge_appeal = fields.Char(related='pledge_id.x_studio_field_D9BNs.name')
    pledge_type = fields.Char(related='pledge_id.x_studio_field_qkC0P.x_name')
    pledge_amount = fields.Float(related='pledge_id.recurring_total')
    currency_id = fields.Many2one(
        string='Currency',
        readonly=False,
        comodel_name='res.currency',
        default=lambda self: self.env['res.currency'].sudo().search([],limit=1).id)
    

    @api.model
    def create(self, values):
        ticket = super(HelpdeskTicket, self).create(values)
        if ticket.partner_id:
            ticket.message_unsubscribe(partner_ids=ticket.partner_id.ids)
        return ticket
    
    @api.multi
    def write(self, vals):
        res = super(HelpdeskTicket, self).write(vals)
        if vals.get('partner_id'):
            self.message_unsubscribe([vals['partner_id']])
        return res
    
    @api.multi
    def message_update(self, msg, update_vals=None):
        update = super(HelpdeskTicket, self).message_update(msg, update_vals=update_vals)
        partner_ids = [x for x in self._find_partner_from_emails(self._ticket_email_split(msg)) if x]
        if partner_ids:
            self.message_unsubscribe(partner_ids)
        return update
    
    @api.model
    def message_new(self, msg, custom_values=None):
        ticket = super(HelpdeskTicket, self).message_new(msg, custom_values=custom_values)
        partner_ids = [x for x in ticket._find_partner_from_emails(self._ticket_email_split(msg)) if x]
        if partner_ids:
            ticket.message_unsubscribe(partner_ids)
        return ticket
