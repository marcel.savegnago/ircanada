# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class ResCompany(models.Model):
    _inherit = 'res.company'

    eligible_donation_account_ids = fields.Many2many(
        comodel_name='account.account', 
        relation='intel_eligible_donation_account_rel', 
        column1='company_id', 
        column2='account_id',
        string='Eligible Donation GL Accounts',
        help='Select all the Eligible Donation GL Accounts',
    )

    eligible_non_donation_account_ids = fields.Many2many(
        comodel_name='account.account', 
        relation='intel_eligible_non_donation_account_rel', 
        column1='company_id', 
        column2='account_id',
        string='Eligible Non-Donation GL Accounts',
        help='Select all the Eligible Non-Donation GL Accounts',
    )


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    eligible_donation_account_ids = fields.Many2many(related='company_id.eligible_donation_account_ids')
    eligible_non_donation_account_ids = fields.Many2many(related='company_id.eligible_non_donation_account_ids')
        
