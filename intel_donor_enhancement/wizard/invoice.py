# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
import time
from odoo.tools.safe_eval import safe_eval

class DonorInv(models.TransientModel):
    _name = "intel.donor.inv"
    _description = "Intel Donor Donations"

    @api.multi
    def create_invoices(self):
        donors = self.env['res.partner'].browse(self._context.get('active_ids', []))
        inv_obj = self.env['account.invoice']

        for donor in donors:
            inv_obj.create({
                'partner_id':donor.id
            })
        
        if self._context.get('open_invoices', False):
            return donors.action_view_invoice()
        return {'type': 'ir.actions.act_window_close'}


class InvoiceDonor(models.TransientModel):
    _name = 'intel.invoice.donor'

    def _get_years(self):
        current_year = int(time.strftime('%Y'))
        year_list = []
        for i in range(4):
            year_list.append((current_year+i, current_year+i))
        return year_list
    

    donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='New Donor Assignment',
        required=True)
    reason = fields.Text(
        string='Reason for Re-assignment',
        required=True)
    relationship_id = fields.Many2one(
        comodel_name='res.partner.relation.type.selection',
        string='Set Relationship (Optional)')
    year = fields.Selection(
        selection=_get_years, 
        string='Start Year', 
        required=True)


    @api.multi
    def change_donor(self):
        donations = self.env['account.invoice'].browse(self._context.get('active_ids', []))
        log_obj = self.env['intel.change.donor.log']
        for donation in donations:
            old_donor = donation.partner_id
            new_donor = self.donor_id
            if donation.partner_id:
                donation.partner_id = new_donor.id 
            
            if donation.payment_tx_id.partner_id:
                donation.payment_tx_id.partner_id = new_donor.id
            
            if donation.payment_tx_id.sale_order_id:
                donation.payment_tx_id.sale_order_id.partner_id = new_donor.id
            
            #token = self.env['payment.token'].search([('partner_id','=',new_donor.id)], limit=1)
            """if token:
                donation.payment_token_id = token.id
                if donation.payment_tx_id.payment_token_id:
                    donation.payment_tx_id.payment_token_id = token.id
            else:
                donation.payment_token_id = False
            """
            
            """if donation.move_id and donation.move_id.partner_id:
                donation.move_id.partner_id = new_donor.id
                if donation.move_id.line_ids:
                    donation.move_id.line_ids.write({'partner_id':new_donor.id})
                    full_reconcile_id = donation.move_id.line_ids[0].full_reconcile_id
                    self.env['account.move.line'].search([('full_reconcile_id','=',full_reconcile_id.id)]).write({'partner_id':new_donor.id}) 
        
            if donation.payment_ids:
                donation.payment_ids.write({'partner_id':new_donor.id})
            """
            if donation.invoice_line_ids:
                donation.invoice_line_ids.write({'partner_id':new_donor.id})
            
            if self.relationship_id:
                relation_obj = self.env['res.partner.relation.all']
                relation = relation_obj.search([('this_partner_id','=',old_donor.id),('type_selection_id','=',self.relationship_id.id),('other_partner_id','=',new_donor.id)], limit=1)
                if not relation:
                    relation_obj.create({
                        'this_partner_id':old_donor.id,
                        'type_selection_id':self.relationship_id.id,
                        'other_partner_id':new_donor.id
                    })
            log_obj.create({
                'donor_id':old_donor.id,
                'new_donor_id':new_donor.id,
                'reason':self.reason +" for year:"+str(self.year),
                'relationship_id':self.relationship_id and self.relationship_id.id or False,
                'donation_id':donation.id
            })


class AccountInvoiceRefund(models.TransientModel):
    """Credit Notes"""

    _inherit = "account.invoice.refund"

    @api.multi
    def compute_refund(self, mode='refund'):
        inv_obj = self.env['account.invoice']
        inv_tax_obj = self.env['account.invoice.tax']
        inv_line_obj = self.env['account.invoice.line']
        context = dict(self._context or {})
        xml_id = False

        for form in self:
            created_inv = []
            date = False
            description = False
            for inv in inv_obj.browse(context.get('active_ids')):
                if inv.state in ['draft', 'cancel']:
                    raise UserError(_('Cannot create credit note for the draft/cancelled invoice.'))
                if inv.reconciled and mode in ('cancel', 'modify'):
                    raise UserError(_('Cannot create a credit note for the invoice which is already reconciled, invoice should be unreconciled first, then only you can add credit note for this invoice.'))

                date = form.date or False
                description = form.description or inv.name
                refund = inv.refund(form.date_invoice, date, description, inv.journal_id.id)
                refund.write({'x_studio_field_ZdhFd':inv.x_studio_field_ZdhFd.id,
                            'x_studio_field_7PrQt':inv.x_studio_field_7PrQt.id, 
                            'x_studio_field_XJgdz':inv.x_studio_field_XJgdz})

                created_inv.append(refund.id)
                if mode in ('cancel', 'modify'):
                    movelines = inv.move_id.line_ids
                    to_reconcile_ids = {}
                    to_reconcile_lines = self.env['account.move.line']
                    for line in movelines:
                        if line.account_id.id == inv.account_id.id:
                            to_reconcile_lines += line
                            to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)
                        if line.reconciled:
                            line.remove_move_reconcile()
                    refund.action_invoice_open()
                    for tmpline in refund.move_id.line_ids:
                        if tmpline.account_id.id == inv.account_id.id:
                            to_reconcile_lines += tmpline
                    to_reconcile_lines.filtered(lambda l: l.reconciled == False).reconcile()
                    if mode == 'modify':
                        invoice = inv.read(inv_obj._get_refund_modify_read_fields())
                        invoice = invoice[0]
                        del invoice['id']
                        invoice_lines = inv_line_obj.browse(invoice['invoice_line_ids'])
                        invoice_lines = inv_obj.with_context(mode='modify')._refund_cleanup_lines(invoice_lines)
                        tax_lines = inv_tax_obj.browse(invoice['tax_line_ids'])
                        tax_lines = inv_obj._refund_cleanup_lines(tax_lines)
                        invoice.update({
                            'type': inv.type,
                            'date_invoice': form.date_invoice,
                            'state': 'draft',
                            'number': False,
                            'invoice_line_ids': invoice_lines,
                            'tax_line_ids': tax_lines,
                            'date': date,
                            'origin': inv.origin,
                            'fiscal_position_id': inv.fiscal_position_id.id,
                            'x_studio_field_ZdhFd':inv.x_studio_field_ZdhFd.id,
                            'x_studio_field_7PrQt':inv.x_studio_field_7PrQt.id, 
                            'x_studio_field_XJgdz':inv.x_studio_field_XJgdz
                        })
                        for field in inv_obj._get_refund_common_fields():
                            if inv_obj._fields[field].type == 'many2one':
                                invoice[field] = invoice[field] and invoice[field][0]
                            else:
                                invoice[field] = invoice[field] or False
                        inv_refund = inv_obj.create(invoice)
                        if inv_refund.payment_term_id.id:
                            inv_refund._onchange_payment_term_date_invoice()
                        created_inv.append(inv_refund.id)
                xml_id = inv.type == 'out_invoice' and 'action_invoice_out_refund' or \
                         inv.type == 'out_refund' and 'action_invoice_tree1' or \
                         inv.type == 'in_invoice' and 'action_invoice_in_refund' or \
                         inv.type == 'in_refund' and 'action_invoice_tree2'
                # Put the reason in the chatter
                subject = _("Credit Note")
                body = description
                refund.message_post(body=body, subject=subject)
        if xml_id:
            result = self.env.ref('account.%s' % (xml_id)).read()[0]
            invoice_domain = safe_eval(result['domain'])
            invoice_domain.append(('id', 'in', created_inv))
            result['domain'] = invoice_domain
            return result
        return True

        

