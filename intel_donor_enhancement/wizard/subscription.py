# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models
import time

class SubscriptionDonor(models.TransientModel):
    _name = 'intel.subscription.donor'

    def _get_years(self):
        current_year = int(time.strftime('%Y'))
        year_list = []
        for i in range(4):
            year_list.append((current_year+i, current_year+i))
        return year_list
    

    donor_id = fields.Many2one(
        comodel_name='res.partner',
        string='New Donor Assignment',
        required=True)
    reason = fields.Text(
        string='Reason for Re-assignment',
        required=True)
    relationship_id = fields.Many2one(
        comodel_name='res.partner.relation.type.selection',
        string='Set Relationship (Optional)')
    year = fields.Selection(
        selection=_get_years, 
        string='Start Year', 
        required=False)

    @api.multi
    def change_donor(self):
        subscriptions = self.env['sale.subscription'].browse(self._context.get('active_ids', []))
        log_obj = self.env['intel.change.donor.subscription.log']
        Invoice = self.env['account.invoice']
        invoice_donor_obj = self.env['intel.invoice.donor']
        for subscription in subscriptions:
            old_donor = subscription.partner_id
            new_donor = self.donor_id
            if subscription.partner_id:
                subscription.partner_id = new_donor.id
            """token = self.env['payment.token'].search([('partner_id','=',new_donor.id)], limit=1)
            if token:
                subscription.payment_token_id = token.id
            else:
                subscription.payment_token_id = False
            """
            
            invoices = Invoice.search([('invoice_line_ids.subscription_id', '=', subscription.id),('x_studio_field_XJgdz','>=',time.strftime('%s-01-01 00:00:00'%self.year))])

            invoice_donor = invoice_donor_obj.create({
                'donor_id':new_donor.id,
                'reason':self.reason,
                'relationship_id':self.relationship_id and self.relationship_id.id or False,
                'year':int(self.year)
            })
            invoice_donor.with_context({'active_ids':invoices.ids}).change_donor()

            log_obj.create({
                'donor_id':old_donor.id,
                'new_donor_id':new_donor.id,
                'reason':self.reason +" for year:"+str(self.year),
                'relationship_id':self.relationship_id and self.relationship_id.id or False,
                'subscription_id':subscription.id,
            })