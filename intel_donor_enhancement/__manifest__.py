# -*- coding: utf-8 -*-
# © 2018 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Intel Donor Enhancement',
    'summary': 'Intel Donor Enhancement',
    'description': 'Intel Donor Enhancement',
    'author': 'Intelligenti.io',
    'category': 'Tools',
    'license': 'AGPL-3',
    'website': 'http://www.intelligenti.io',
    'version': '1.0.72',
    'application': True,
    'installable': True,
    'auto_install': False,
    'depends': ['helpdesk', 'mail', 'sodexis_enhancement'],
    'data': [
        'security/ir.model.access.csv',
        'security/donor_security.xml',
        'wizard/invoice.xml',
        'wizard/subscription.xml',
        'views/account.xml',
        'views/helpdesk.xml',
        'views/subscription.xml',
        'views/filters.xml',
        'views/partner.xml',
        'data/template.xml',
        'views/res_config_settings.xml'
    ],
}
