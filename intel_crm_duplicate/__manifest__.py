# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Intel CRM Duplicate",
    "summary": "Intel CRM Duplicate",
    "description": "Intel CRM Duplicate",
    "author": "Intelligenti.io",
    "website": "http://www.intelligenti.io",
    "license": "AGPL-3",
    "category": "Tools",
    "version": "3.5",
    "depends": [
        "custom_crm_duplicates",
        "intel_donor_enhancement",
    ],
    "data": [
        # 'security/ir.model.access.csv',
        "views/views.xml",
        "wizards/custom_partner_merge_automatic.xml",
        "views/merge_view.xml",
        "views/partner.xml",
    ],
    # only loaded in demonstration mode
    "demo": ["demo/demo.xml"],
}
