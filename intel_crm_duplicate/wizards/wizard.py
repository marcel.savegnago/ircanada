from odoo import api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError, UserError


class CustomPartnerMergeAutomaticWizard(models.TransientModel):
    """adds view_donors method to custom.partner.merge.automatic.wizard"""

    _inherit = "custom.partner.merge.automatic.wizard"

    created_on = fields.Date()
    dummy = fields.Char(default="At least one duplicate check must be selected")

    @api.multi
    def view_donors(self):
        return {
            "name": "Donors",
            "view_mode": "tree, form",
            "view_id": False,
            "view_type": "form",
            "res_model": "res.partner",
            "type": "ir.actions.act_window",
            "target": "new",
            "domain": "[('id', 'in', %s)]" % self.partner_ids.ids,
            "views": [(False, "tree")],
            "context": {},
        }

    @api.multi
    def action_merge(self):
        """ Merge Contact button. Merge the selected partners, and redirect to
            the end screen (since there is no other wizard line to process.
        """

        company_id = self.env.user.company_id.sudo()
        special_values = dict()
        values_email = dict()
        partner_ids = self.partner_ids
        partner_date_list = []
        oldest_since_date = None
        if partner_ids:
            for partner in partner_ids:
                date = partner.x_studio_field_chJhB
                date = datetime.strptime(date, '%Y-%m-%d').date()
                partner_date_list.append(date)
            oldest_since_date = min(partner_date_list)

        if not self.partner_ids:
            self.write({'state': 'finished'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_mode': 'form',
                'target': 'new',
            }
        self._merge_records_save(company_id, partner_ids)
        if self.dst_partner_id.id and self.dst_partner_id.id in self.partner_ids.ids:
            src_partners = partner_ids - self.dst_partner_id
            dst_partner = self.dst_partner_id
        else:
            ordered_partners = self._get_ordered_partner(partner_ids.ids)
            dst_partner = ordered_partners[0]
            src_partners = ordered_partners[1:]

        if not self.dst_partner_id.x_studio_field_uzzbN:
            for record in src_partners:
                if record['x_studio_field_uzzbN']:
                    raise UserError(_("Is it recommended to always merge into the original Donor Record from Raisers Edge."))
        # put all phone, email and address as other/ historical

        if oldest_since_date:
            dst_partner.write({'x_studio_field_chJhB': oldest_since_date})

        for record in partner_ids:
            try:
                street = record['street'] if 'street' in record else ''
                street2 = record['street2'] if 'street2' in record else ''
                city = record['city'] if 'city' in record else ''
                state_id = record['state_id'].id if 'state_id' in record else ''
                zip = record['zip'] if 'zip' in record else ''
                country_id = record['country_id'].id if 'country_id' in record else ''
                phone = record['phone'] if 'phone' in record else ''
                mobile = record['mobile'] if 'mobile' in record else ''
                email = record['email'] if 'email' in record else ''
                add_phone = record['x_studio_field_zbWQS'] if 'x_studio_field_zbWQS' in record else ''
                add_email = record['x_studio_field_Vvzww'] if 'x_studio_field_Vvzww' in record else ''

                self.env['merged.partner'].merge_partner(dst_partner, record)
                self.env['res.partner'].main_merge(city, country_id, email, record, dst_partner, phone, state_id, street,\
                                                   street2, add_phone, add_email, zip, mobile)
                if record != dst_partner:
                    if record.x_studio_field_J1nRO:
                        for notes in record.x_studio_field_J1nRO:
                            vals = {
                                'x_name': notes.x_name,
                                'x_studio_field_udyMp': dst_partner.id,
                                'x_studio_field_dJSgS': notes.x_studio_field_dJSgS,
                                'x_studio_field_ai8rO': notes.x_studio_field_ai8rO.id,
                                'x_studio_field_r7R1I': notes.x_studio_field_r7R1I,
                                'x_studio_field_wvejL': dst_partner.code,
                            }
                            self.env['x_donor_notes'].create(vals)


                if record != dst_partner:
                    if record.address_ids:
                        for address_id in record.address_ids:
                            vals = {'note':record.code}
                            if address_id.type != 'a':
                                vals.update({'partner_id':dst_partner.id})
                                address_id.write(vals)

                    if record.phone_number_ids:
                        for phone in record.phone_number_ids:
                            vals = {'note':record.code}
                            if phone.type != '0' and phone.type != '7':
                                vals.update({'partner_id':dst_partner.id})
                                phone.write(vals)
                else:
                    record.address_ids.write({'note':record.code})
                                

                if self.current_line_id:
                    self.current_line_id.unlink()
            except Exception as e:
                print(e)
                print(' There is an exception ')
        
        if self.communication_data.id in partner_ids.ids:
            values = self.env['res.partner'].communication_data_merge(self.communication_data, dst_partner)
            special_values.update(values)

        if self.personal_information.id in partner_ids.ids:
            values = self.env['res.partner'].personal_info_merge(self.personal_information, dst_partner)
            special_values.update(values)
        if self.donor_attribute.id in partner_ids.ids:
            values = self.env['res.partner'].donor_attribute_merge(self.donor_attribute, dst_partner)
            special_values.update(values)

        #merge other fields
        x_studio_field_dLazD = partner_ids.filtered(lambda p: p.x_studio_field_dLazD == 'No Tax Receipt')
        x_studio_field_83P92 = partner_ids.filtered(lambda p: p.x_studio_field_83P92 == True)
        x_studio_field_nNA3x = partner_ids.filtered(lambda p: p.x_studio_field_nNA3x == True)
        x_studio_field_sOhhB = partner_ids.filtered(lambda p: p.x_studio_field_sOhhB == True)
        x_studio_field_3YKjQ = partner_ids.filtered(lambda p: p.x_studio_field_3YKjQ == True)
        x_studio_field_MQaLQ = partner_ids.filtered(lambda p: p.x_studio_field_MQaLQ != False).mapped('x_studio_field_MQaLQ')
        

        if x_studio_field_dLazD:
            special_values.update({'x_studio_field_dLazD':'No Tax Receipt'})
        if x_studio_field_83P92:
            special_values.update({'x_studio_field_83P92':True})
        if x_studio_field_nNA3x:
            special_values.update({'x_studio_field_nNA3x':True})
        if x_studio_field_sOhhB:
            special_values.update({'x_studio_field_sOhhB':True})
        if x_studio_field_3YKjQ:
            special_values.update({'x_studio_field_3YKjQ':True})
        if x_studio_field_MQaLQ:
            min_donation_date = x_studio_field_MQaLQ[0]
            for d in x_studio_field_MQaLQ:
                if d <= min_donation_date:
                    min_donation_date = d
            special_values.update({'x_studio_field_MQaLQ':min_donation_date})
        
        self._merge(self.partner_ids.ids, self.dst_partner_id)
        
        dst_partner.write(special_values)

        if dst_partner:
            if dst_partner.phone_number_ids:
                for phone_data in dst_partner.phone_number_ids:
                    if phone_data.type == '10':
                        phone_data.unlink()

                for phone_data in dst_partner.phone_number_ids:
                    if phone_data.type == '9':
                        duplicate_email = self.env['prt.phone'].search([('id', '!=', phone_data.id),
                                                                           ('partner_id', '=', dst_partner.id),
                                                                           ('type', '=', '9'),
                                                                           ('number', '=', phone_data.number)])
                        if duplicate_email:
                            phone_data.unlink()        


        return self._action_next_screen()
