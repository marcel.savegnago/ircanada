# -*- coding: utf-8 -*-

from odoo import models, fields, api


class MergePartner(models.Model):

    _inherit = "merged.partner"

    @api.multi
    def show_merge_list(self):
        self.ensure_one()
        action = self.env.ref('custom_crm_duplicates.merge_list_action').read()[0]
        merge_list = self.env['merge.list'].search([('primary_contact_merge','=',self.destination_donor_id.id),('secondary_customer_code','=',self.merged_donor_id)])
        if len(merge_list) > 1:
            action['domain'] = [('id', 'in', merge_list.ids)]
        elif len(merge_list) == 1:
            action['res_id'] = merge_list.id
            action['views'] = [(self.env.ref('custom_crm_duplicates.merge_view_form').id, 'form')]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action


class MergeList(models.Model):

    _inherit = "merge.list"
    _order = "date_merge desc"

    @api.multi
    def view_details(self):
        self.ensure_one()
        action = self.env.ref('custom_crm_duplicates.merged_partner_action').read()[0]
        merged_partner = self.env['merged.partner'].search([('destination_donor_id','=',self.primary_contact_merge.id),('merged_donor_id','=',self.secondary_customer_code)])
        if len(merged_partner) > 1:
            action['domain'] = [('id', 'in', merged_partner.ids)]
        elif len(merged_partner) == 1:
            action['res_id'] = merged_partner.id
            action['views'] = [(self.env.ref('custom_crm_duplicates.merged_partner_view_form').id, 'form')]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action



class RemoveMatch(models.Model):

    _inherit = "remove.match"
    _order = "date_remove desc"

    date_remove = fields.Datetime(string="Date Remove", default=lambda self: fields.Datetime.now())


class MergedPartner(models.Model):

    _inherit = "merged.partner"
    _order = "create_date desc"

    donor_since = fields.Datetime(string="Donor Since")


class MergeList(models.Model):

    _inherit = "merge.list"
    _order = "date_merge desc"

    date_merge = fields.Datetime(string="Date Merge", default=lambda self: fields.Datetime.now())

