from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
from odoo.http import request
_logger = logging.getLogger(__name__)


ADDRESS_FIELDS_CHAR = ['street', 'street2', 'city', 'zip']
ADDRESS_FIELDS_MANY2ONE = ['state_id', 'country_id']
ADDRESS_FIELDS = ADDRESS_FIELDS_CHAR + ADDRESS_FIELDS_MANY2ONE
ADDRESS_FIELDS_COUNT = len(ADDRESS_FIELDS)


class ResPartner(models.Model):

    _inherit = "res.partner"

    # x_studio_field_nNA3x = fields.Boolean(string="Do Not Call")
    # x_studio_field_sOhhB = fields.Boolean(string="Do Not Mail")

    def personal_info_merge(self, source, destination):
            values = dict()
            values['x_studio_field_UsVrp'] = source['x_studio_field_UsVrp']
            dest_city = destination.city
            dest_street = destination.street
            if source['title']:
                values['title'] = source['title'].id
            values['street'] = source['street']
            values['street2'] = source['street2']
            values['city'] = source['city']
            values['state_id'] = source['state_id'].id
            values['zip'] = source['zip']
            values['country_id'] = source['country_id'].id
            values['note'] = source['code']
            address_vals = {}
            empty_vals = 0
            for key in ADDRESS_FIELDS:
                val = values.get(key, False)
                address_vals.update({key: val})
                if not val:
                    empty_vals += 1

            if len(address_vals) == empty_vals:
                return {}
            else:
                rec = request.env['cx.address'].search([('partner_id', '=', destination.id),
                                                        ('city', '=', values['city']), ('street', '=', values['street'])])
                result = rec.unlink()
                rec = request.env['cx.address'].search([('partner_id', '=', destination.id), ('type', '=', 'a'),
                                                        ('city', '=', dest_city), ('street', '=', dest_street)])
                result = rec.unlink()

                return values
            # destination.write(values)

    def communication_data_merge(self, source, destination):
            #
        values = dict()
        values['email'] = source['email']
        values['phone'] = source['phone']
        rec = request.env['prt.phone'].search(
            [('partner_id', '=', destination.id), ('type', '=', '0')])
        result = rec.write({'type': '6','note':destination.code})
        rec = request.env['prt.phone'].search(
            [('partner_id', '=', destination.id), ('number', '=', values['phone'])])
        result = rec.unlink()
        if values['phone']:
            vals = dict()
            vals['partner_id'] = destination.id
            vals['type'] = '0'
            vals['note'] = source.code
            vals['number'] = values['phone']
            result = request.env['prt.phone'].create(vals)
        
        rec = request.env['prt.phone'].search(
            [('partner_id', '=', destination.id), ('type', '=', '7')])
        # result = rec.unlink()
        rec.write({'type': '9','note':source.code})
        rec = request.env['prt.phone'].search(
            [('partner_id', '=', destination.id), ('number', '=', values['email'])])
        result = rec.unlink()
        # For Email
        if values['email']:
            vals = dict()
            vals['partner_id'] = destination.id
            vals['type'] = '7'
            vals['number'] = values['email']
            vals['note'] = source.code
            result = request.env['prt.phone'].create(vals)

        return {}
    
    def main_merge(self, city, country_id, email, parent_id, partner_id, phone, state_id, street1, street2, add_phone,
                   add_email, zip, mobile):
        val_address = {}
        val_address['street'] = street1
        val_address['street2'] = street2
        val_address['city'] = city
        val_address['state_id'] = state_id
        val_address['zip'] = zip
        val_address['country_id'] = country_id
        val_address['partner_id'] = partner_id.id
        val_address['type'] = 'd'
        val_address['note'] = parent_id.code

        address_vals = {}
        empty_vals = 0
        for key in ADDRESS_FIELDS:
            val = val_address.get(key, False)
            address_vals.update({key: val})
            if not val:
                empty_vals += 1

        if len(address_vals) == empty_vals:
            pass
            # raise ValidationError(_("Please fill at list one address field to continue "
            #                         "or remove the address from address list to delete in permanently!"))
        else:
            request.env['cx.address'].create(val_address)
        domain = [
            ('res_id', '=', partner_id.id),
            ('res_model', '=', 'res.partner'),
        ]
        attachment = self.env['ir.attachment'].search(domain)
        if attachment:
            attachment.write({'red_id': partner_id.id})
        if phone:
            vals = {}
            if partner_id.id != parent_id.id:
                vals['partner_id'] = partner_id.id
                vals['type'] = '6'
                vals['number'] = phone
                vals['note'] = parent_id.code
                request.env['prt.phone'].create(vals)
            else:
                vals['partner_id'] = partner_id.id
                vals['type'] = '10'
                vals['number'] = phone
                vals['note'] = parent_id.code
                request.env['prt.phone'].create(vals)
        if email:
            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '9'
            vals['number'] = email
            vals['note'] = parent_id.code
            request.env['prt.phone'].create(vals)
        if add_phone:  # Additional Phone number
            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '6'
            vals['number'] = add_phone
            vals['note'] = parent_id.code
            request.env['prt.phone'].create(vals)
        # if mobile:  # Mobile Phone number
        #     vals = {}
        #     if partner_id.id != parent_id.id:
        #         vals['partner_id'] = partner_id.id
        #         vals['type'] = '6'
        #         vals['number'] = mobile
        #         request.env['prt.phone'].create(vals)
        #     else:
        #         vals['partner_id'] = partner_id.id
        #         vals['type'] = '10'
        #         vals['number'] = mobile
        #         request.env['prt.phone'].create(vals)

        if add_email:  # Additional Email

            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '9'
            vals['number'] = add_email
            vals['note'] = parent_id.code
            request.env['prt.phone'].create(vals)
            
