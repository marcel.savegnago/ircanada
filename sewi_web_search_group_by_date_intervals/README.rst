**********************************
Web search group by date intervals
**********************************

This module extends the **Group By - Add custom group** section by a dropdown to choose the date interval you want
to group by a date or datetime field.

Available intervals
###################

   - Days *(Warning: Loading unlimited results in a graph view may lead to freezing the browser)*
   - Weeks
   - Months
   - Quarters
   - Years


Notes
#####

   - Tested on Odoo 11 **CE** and **EE**!
   - **Graph** views are limited to **2** groups, that means if you select more the first 2 groups will be applied.
   - **Kanban** views are limited to **1** group!
   - Loading a huge amount of records in a graph view will take a very long loading time which can lead to freezing the browser (or it's tab). This is based on default graph implementation, which will not limit the returned result. Be sure, that you select a group filter like "Years" or "Quarters" before switching to graph view.


Examples
########

This feature can used in **List**, **Kanban**, **Pivot** and **Graph** views.

List View
*********

.. image:: /sewi_web_search_group_by_date_intervals/static/description/example_order_grouped_list.png
   :width: 100%


Graph View
**********

.. image:: /sewi_web_search_group_by_date_intervals/static/description/example_order_grouped_graph.png
   :width: 100%


Kanban View
***********

.. image:: /sewi_web_search_group_by_date_intervals/static/description/example_order_grouped_kanban.png
   :width: 100%
