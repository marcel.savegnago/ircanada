{
    # name of the app must be explicit and no more than 25 characters. Avoid adjectives, or putting the name of your company in the app name.
    'name': "Group by date intervals",

    'summary': """Adds a dropdown to group by menu which allows to select a date interval.""",

    'author': 'sewisoft',
    'license': 'OPL-1',
    'website': "https://sewisoft.de",
    'sequence': 1,

    'category': 'Technical Settings',
    'version': '11.0.1.0.6',

    # Odoo app store properties
    'price': 49.90,
    'currency': 'EUR',
    'support': 'support@sewisoft.de',
    'images': [
        'static/description/main.png',
        'static/description/preview_screenshot.png'
    ],

    'depends': [
        'base',
        'web'
    ],

    "qweb": [
        'static/src/xml/base.xml',
    ],

    'data': [
        'views/templates.xml',
    ],

    'installable': True
}