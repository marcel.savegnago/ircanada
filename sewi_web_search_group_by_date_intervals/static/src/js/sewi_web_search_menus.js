odoo.define('sewi_web_search_group_by_date_intervals.GroupByMenu', function (require) {
    "use strict";

    var core = require('web.core');
    var GroupByMenu = require('web.GroupByMenu');
    var search_inputs = require('web.search_inputs');
    var _t = core._t;

    return GroupByMenu.include({
        init: function (parent, groups, fields) {
            this._super.apply(this, arguments);
            this.dateIntervals = [
                {val: ':day', str: _t('(days)'), opt: _t('Days')},
                {val: ':week', str: _t('(weeks)'), opt: _t('Weeks')},
                {val: ':month', str: _t('(months)'), opt: _t('Months')},
                {val: ':quarter', str: _t('(quarters)'), opt: _t('Quarters')},
                {val: ':year', str: _t('(years)'), opt: _t('Years')}
            ];
        },

        start: function () {
            var self = this;
            this._super.apply(this, arguments);
            this.$dateIntervalSelectLi = this.$('.sewi_group_selector_dateinterval_li');
            this.$group_selector.change(function () {
                self.update_interval_selector_visibility()
            });
            this.update_interval_selector_visibility()
        },

        update_interval_selector_visibility: function () {
            var field = this.$group_selector.find(':selected').data('name');
            var selectedField = _.find(this.groupableFields, {name: field});
            if (selectedField.type === 'date' || selectedField.type === 'datetime') {
                this.$dateIntervalSelectLi.css('display', 'block')
            } else {
                this.$dateIntervalSelectLi.css('display', 'none')
            }
        },

        add_groupby_to_menu: function (field_name) {
            var selectedField = _.find(this.groupableFields, {name: field_name});
            var fieldString = selectedField.string;
            var fieldName = field_name;
            if (selectedField.type === 'date' || selectedField.type === 'datetime') {
                var intervalValue = this.$dateIntervalSelectLi.find(":selected").val();
                fieldString += " " + _.find(this.dateIntervals, {val: intervalValue}).str;
                fieldName += intervalValue;
            }
            var filter = new search_inputs.Filter({
                attrs: {
                    context: "{'group_by':'" + fieldName + "''}",
                    name: fieldString,
                }
            }, this.searchview);
            var group = new search_inputs.FilterGroup([filter], this.searchview),
                divider = this.$('.divider').show();
            group.insertBefore(divider);
            group.toggle(filter);
        }
    });

});


