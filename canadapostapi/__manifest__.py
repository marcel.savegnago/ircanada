# -*- coding: utf-8 -*-
{
    'name': "canadapostapi",

    'summary': """
        Integrate Canada Post lookup into our backend contact page""",

    'description': """
        As the user enters the street address in field [street] the Canada Post addon should propose the address. If an address is selected it should populate the Street, City, Province, Postal Code, and Country fields
    """,

    'author': "KNYSYS LLC",
    'website': "http://www.knysys.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/res_config_settings.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}