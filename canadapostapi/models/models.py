# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import xml.dom.minidom
from urllib.parse import urlencode
from urllib.request import urlopen
import re

class CanadianAddresses(models.Model):
    _name = 'canadian.addresses'

    name = fields.Char(index=True)
    address = fields.Char()
    street = fields.Char()
    zip = fields.Char()
    city = fields.Char()
    state = fields.Char()


    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
       return self.name_get(name)

    @api.multi
    def name_get(self, name=None):
        """ Return the tags' display name, including their direct parent. """
        if name and name != '':
            address_list, add_list, streets, zip_list, cities, states = ([] for i in range(6))
            add_index = -1

            ir_params = self.env['ir.config_parameter']
            api_key = ir_params.sudo().get_param('canadapostapi.api_key')
            results = CanadianAddresses.addressComplete_interactive_find_v2_10(api_key, str(name), None, 'Everything', 'Can', 'en', 40, 100)
            for result in results:
                address = str(result['Text'] + ', ' + result['Description'])
                if CanadianAddresses.address_is_valid(address):
                    address_list.append(str(result['Text'] + ', ' + result['Description']))
                    states.append(CanadianAddresses.extract_state(address))
                    zip_list.append(CanadianAddresses.extract_zip_postal(address))
                    streets.append(result['Text'])
                    cities.append(result['Description'].split(',')[0])

            max_addresses = len(address_list) if len(address_list) <= 7 else 7
            for x in range(0, max_addresses):
                add_list.append((x+1, address_list[x]))

            address_recs = self.search([], order="id asc", limit=len(add_list))

            if address_recs and add_list:
                for address in address_recs:
                    add_index = add_index + 1
                    address.sudo().write({"address": add_list[add_index][1], "street": streets[add_index],
                                          "zip": zip_list[add_index], "city": cities[add_index],
                                          "state": states[add_index]})
            else:
                for address in add_list:
                    self.env['canadian.addresses'].sudo().create(
                        {"address": address[1], "street": streets[add_index], "zip": zip_list[add_index],
                         "city": cities[add_index], "state": states[add_index]})

            return add_list

    @staticmethod
    def address_is_valid(address):
        try:
            if re.search("[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]", address,
                         re.IGNORECASE | re.DOTALL) and CanadianAddresses.extract_zip_postal(address) and CanadianAddresses.extract_state(address):
                return True
            else:
                return False
        except Exception:
            return False

    @staticmethod
    def extract_zip_postal(address):
        start_index =  re.search("[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]", address,
                     re.IGNORECASE | re.DOTALL).start()
        end_index = re.search("[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]",
                             address,
                             re.IGNORECASE | re.DOTALL).end()
        return address[start_index:end_index]

    @staticmethod
    def extract_state(address):
        regex = re.search("[,]\s[A-Z]{2}[,]\s", address, re.DOTALL)
        start_index = regex.regs[0][0] + 2
        end_index = regex.regs[0][1] - 2
        return address[start_index:end_index]

    @staticmethod
    def addressComplete_interactive_find_v2_10(Key, SearchTerm, LastId, SearchFor, Country, LanguagePreference, MaxSuggestions, MaxResults):

      #Build the url
      requestUrl = "http://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Find/v2.10/xmla.ws?"
      requestUrl += "&" + urlencode({"Key": Key})
      requestUrl += "&" + urlencode({"SearchTerm": SearchTerm})
      requestUrl += "&" + urlencode({"LastId": LastId})
      requestUrl += "&" + urlencode({"SearchFor": SearchFor})
      requestUrl += "&" + urlencode({"Country": Country})
      requestUrl += "&" + urlencode({"LanguagePreference": LanguagePreference})
      requestUrl += "&" + urlencode({"MaxSuggestions": MaxSuggestions})
      requestUrl += "&" + urlencode({"MaxResults": MaxResults})

      #Get the data
      dataDoc = xml.dom.minidom.parseString(urlopen(requestUrl).read())

      #Get references to the schema and data
      schemaNodes = dataDoc.getElementsByTagName("Column")
      dataNotes = dataDoc.getElementsByTagName("Row")

      #Check for an error
      if len(schemaNodes) == 4 and schemaNodes[0].attributes["Name"].value == "Error":
         raise Exception(dataNotes[0], dataNotes[0].attributes["Description"].value)

      #Work though the items in the response
      results = []
      for dataNode in dataNotes:
         rowData = dict()
         for schemaNode in schemaNodes:
              key = schemaNode.attributes["Name"].value
              value = dataNode.attributes[key].value
              rowData[key] = value
         results.append(rowData)

      return results


class Partner(models.Model):
    _inherit = "res.partner"

    address_autocomplete = fields.Many2one('canadian.addresses', string='Street Adress Auto-complete')

    @api.onchange('address_autocomplete')
    def onchange_address_autocomplete(self):
        if self.address_autocomplete:
            address = self.address_autocomplete

            self.street = address.street
            self.zip = address.zip
            self.city = address.city
            country = self.env['res.country'].sudo().search([('code', '=', 'CA')])
            state = self.env['res.country.state'].sudo().search(
                ['&', ('code', '=', address.state),
                 ('country_id', '=', country.id)])
            if not state and country:
                state_obj = self.env['res.country.state'].sudo().create({
                    'country_id': country.id,
                    'name': address.state,
                    'code': address.state,
                })
                self.state_id = state_obj.id
            else:
                self.state_id = state.id
            self.country_id = country.id

    @api.model
    def create(self, values):
        values['address_autocomplete'] = None
        return super(Partner, self).create(values)

    @api.multi
    def write(self, values):
        values['address_autocomplete'] = None
        return super(Partner, self).write(values)