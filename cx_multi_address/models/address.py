from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

ADDRESS_FIELDS_CHAR = ['street', 'street2', 'city', 'zip']
ADDRESS_FIELDS_MANY2ONE = ['state_id', 'country_id']
ADDRESS_FIELDS = ADDRESS_FIELDS_CHAR + ADDRESS_FIELDS_MANY2ONE
ADDRESS_FIELDS_COUNT = len(ADDRESS_FIELDS)


################
# Address tags #
################
class AddressTag(models.Model):
    _name = "cx.address.tag"

    name = fields.Char(string="Name", required=True, translate=True)
    color = fields.Integer(string="Color index",
                           help="Odoo color index from 0 to 9")


#############
# Addresses #
#############
class Address(models.Model):
    _name = 'cx.address'
    _order = 'partner_id, type, id desc'
    _rec_name = 'partner_id'
    _description = 'Addresses'

    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner', required=True, ondelete='cascade')
    type = fields.Selection([
        ('a', 'Main'),
        ('b', 'Work'),
        ('c', 'Alternate'),
        ('d', 'Historical'),
    ], string="Type", required=True)

    note = fields.Char(string="Note", help="Put your note or comment here", translate=True)
    tags = fields.Many2many(string="Tags",
                            comodel_name='cx.address.tag',
                            relation='cx_address_tag_rel',
                            column1='address_id',
                            column2='tag_id',
                            help="Any tags like")
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')

    # -- Compute number of runs need to init all data
    @api.model
    def configure_cron(self):
        self._cr.execute(""" SELECT COUNT (id) FROM (SELECT id, street, street2, zip, city, state_id, country_id FROM res_partner prt                            
                                            WHERE (SELECT COUNT(id) FROM cx_address WHERE cx_address.partner_id = prt.id) = 0
                                            AND (length(prt.street)>0 OR length(prt.street2)>0 OR length(prt.city)>0 
                                            OR length(prt.zip)>0 OR prt.state_id>0 or prt.country_id>0)) AS preselect """)
        rec_count = self._cr.fetchall()[0][0]
        if rec_count == 0:
            _logger.info(" >>> ADDRESS IMPORT: No records to import is found. Cron job will be disabled")
            self.env.ref('cx_multi_address.cx_store_address_data').sudo().write({'numbercall': -1})
        batch_num = rec_count // 15000 + 1
        _logger.info(" >>> ADDRESS IMPORT: Total %s records found. Will do import in %s batches" % (
            str(rec_count), str(batch_num)))
        self.env.ref('cx_multi_address.cx_store_address_data').sudo().write({'numbercall': batch_num})

    # -- Store existing addresses
    @api.model
    def init_data_direct(self):

        _logger.info(" >>> ADDRESS IMPORT STARTED")
        self._cr.execute(""" SELECT id, street, street2, zip, city, state_id, country_id FROM res_partner prt                            
                                            WHERE (SELECT COUNT(id) FROM cx_address WHERE cx_address.partner_id = prt.id) = 0
                                            AND (length(prt.street)>0 OR length(prt.street2)>0 OR length(prt.city)>0 
                                            OR length(prt.zip)>0 OR prt.state_id>0 or prt.country_id>0) 
                                            LIMIT 15000""")
        # Get existing data
        res_len = 0
        for res in self._cr.fetchall():
            # Main
            self.sudo().create({
                'partner_id': res[0],
                'type': 'a',
                'street': res[1],
                'street2': res[2],
                'zip': res[3],
                'city': res[4],
                'state_id': res[5],
                'country_id': res[6]
            })
            res_len += 1

        _logger.info(" >>> ADDRESS IMPORT COMPLETE. NUMBER OF RECORDS IMPORTED = %s", str(res_len))

    # -- Store existing addresses (SQL)
    @api.model
    def init_data(self):

        _logger.info(" >>> ADDRESS IMPORT STARTED")
        # Get list of ids of
        self._cr.execute(""" SELECT id, street, street2, zip, city, state_id, country_id FROM res_partner prt                            
                                            WHERE (SELECT COUNT(id) FROM cx_address WHERE cx_address.partner_id = prt.id) = 0
                                            AND (length(prt.street)>0 OR length(prt.street2)>0 OR length(prt.city)>0 
                                            OR length(prt.zip)>0 OR prt.state_id>0 or prt.country_id>0) 
                                            LIMIT 15000""")
        # Get existing data
        res_len = 0
        for res in self._cr.fetchall():
            # Main
            partner_id = res[0]
            self._cr.execute("""INSERT INTO cx_address (partner_id, type, street, street2, zip, city, state_id, country_id)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""", (partner_id, 'a', res[1], res[2], res[3], res[4], res[5], res[6]))
            res_len += 1

        # Invalidate cache
        self.invalidate_cache()

        _logger.info(" >>> ADDRESS IMPORT COMPLETE. NUMBER OF RECORDS IMPORTED = %s", str(res_len))

    # -- Create
    @api.model
    def create(self, vals):

        # Check address fields and raise an exception in case all of them are empty
        address_vals = {}
        empty_vals = 0
        for key in ADDRESS_FIELDS:
            val = vals.get(key, False)
            address_vals.update({key: val})
            if not val:
                empty_vals += 1

        if len(address_vals) == empty_vals:
            raise ValidationError(_("Please fill at list one address field to continue "
                                    "or remove the address from address list to delete in permanently!"))

        # Check if main address is created
        if not vals.get('type') == 'a':
            return super(Address, self).create(vals)

        # Main address
        # Check is there is any other 'Main' address for this partner already
        partner_id = vals.get('partner_id')
        existing_main = self.search([('partner_id', '=', partner_id), ('type', '=', 'a')], limit=1)

        # Change current address status and update address in Partner
        if len(existing_main) > 0:
            existing_main.with_context(skip_type_check=True).write({'type': 'd'})
        self.env['res.partner'].browse([partner_id]).address_update(address_vals)

        return super(Address, self).create(vals)

    # -- Write
    @api.multi
    def write(self, vals):

        # Skip check is updating address type to Historical
        if self._context.get('skip_type_check', False):
            return super(Address, self).write(vals)

        # If address type is changed to main save current Main address as Historical
        change_main = True if vals.get('type', False) == 'a' else False

        # Save current Main addresses
        former_main_ids = []
        if change_main:
            for rec in self:
                current_main = self.search([('partner_id', '=', rec.partner_id.id), ('type', '=', 'a')], limit=1)
                if not len(current_main) == 0:
                    former_main_ids.append(current_main.id)

        # Write
        res = super(Address, self).write(vals)

        # If Main address updated or address type is changed to Main
        for rec in self:
            if rec.type == 'a' or change_main:
                # Update all vals in Partner's address
                address_vals = {}
                for key in ADDRESS_FIELDS_CHAR:
                    address_vals.update({key: rec[key]})
                for key in ADDRESS_FIELDS_MANY2ONE:
                    address_vals.update({key: rec[key].id})
                rec.partner_id.address_update(address_vals)

        # Set former main addresses to Historical
        if not len(former_main_ids) == 0:
            self.browse(former_main_ids).with_context(skip_type_check=True).write({'type': 'd'})

        return res

    # -- Delete
    @api.multi
    def unlink(self):

        # If Main address is deleted set other address as main or clear address in Partner
        for rec in self:
            if rec.type == 'a':
                new_main = False

                # Try to find the last Historical
                for address in rec.partner_id.address_ids:
                    if address.type == 'd':
                        new_main = address
                        break

                # If no Historical is found use any other
                if not new_main:
                    for address in rec.partner_id.address_ids:
                        if not address.type == 'a':
                            new_main = address
                            break

                # Change type of address found to Main else clear the address vals in Partner
                address_vals = {}

                if new_main:
                    for key in ADDRESS_FIELDS_CHAR:
                        address_vals.update({key: new_main[key]})
                    for key in ADDRESS_FIELDS_MANY2ONE:
                        address_vals.update({key: new_main[key].id})
                    new_main.with_context(skip_type_check=True).write({'type': 'a'})
                else:
                    for key in ADDRESS_FIELDS:
                        address_vals.update({key: False})

                rec.partner_id.address_update(address_vals)

        return super(Address, self).unlink()


###########
# Partner #
###########
class Partner(models.Model):
    _inherit = "res.partner"

    address_ids = fields.One2many(string="Addresses", comodel_name='cx.address', inverse_name='partner_id',
                                  auto_join=True)

    # -- Update address in partner
    @api.multi
    def address_update(self, address_vals):
        self.with_context(skip_address_check=True).write(address_vals)

    # -- Save address to address line. If all vals=False delete address
    @api.multi
    def address_save(self):
        for rec in self:
            address_vals = {}
            for key in ADDRESS_FIELDS_CHAR:
                address_vals.update({key: rec[key]})
            for key in ADDRESS_FIELDS_MANY2ONE:
                address_vals.update({key: rec[key].id})

            # Create new address if real values in vals
            address_vals.update({'partner_id': rec.id, 'type': 'a'})
            self.env['cx.address'].create(address_vals)

    # -- Create
    @api.model
    def create(self, vals):
        res = super(Partner, self).create(vals)
        for key in ADDRESS_FIELDS:
            if vals.get(key):
                res.address_save()
                break
        return res

    # -- Write
    @api.multi
    def write(self, vals):

        # Skip address check if needed
        if self._context.get('skip_address_check', False):
            return super(Partner, self).write(vals)

        # Write does not return anything. But just in case it will in the future Odoo versions..
        res = super(Partner, self).write(vals)

        # Check id address vals are updated
        for key in ADDRESS_FIELDS:
            if key in vals:
                self.address_save()
                break

        # Update address vals
        return res


