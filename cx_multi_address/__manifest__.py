# -*- coding: utf-8 -*-
{
    'name': 'Improved Contacts: Several Addresses for contact',
    'version': '11.0.0.0',
    'summary': """Several addresses for partner""",
    'author': 'Ivan Sokolov',
    'category': 'Sales',
    'license': 'OPL-1',
    'support': 'odooapps@cetmix.com',
    'website': 'https://demo.cetmix.com',
    'live_test_url': 'https://demo.cetmix.com',
    'description': """
Multiple Email addresses Several Phone Numbers vCard and QRCode export contacts for Partners
============
1. Add several addresses to contacts
2. Choose which to use by default
3. Add tags and notes for easier navigation

""",
    'depends': ['base'],
    'images': ['static/description/banner_contacts.png'],
    'data': [
        'security/ir.model.access.csv',
        'data/defaults.xml',
        'views/res_partner.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
