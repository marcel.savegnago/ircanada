# -*- coding: utf-8 -*-
# Copyright 2017 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from . import authorize_request
from . import res_partner
from . import sale
from . import account_invoice
