odoo.define('custom_crm_duplicates.many2many_select', function (require) {
"use strict";

var registry = require('web.field_registry');
var AbstractField = require('web.AbstractField');
var core = require('web.core');
//var Model = require('web.Model');
var relationalFields = require('web.relational_fields');

var _t = core._t;
var QWeb = core.qweb;
//var FieldMany2Many = core.form_widget_registry.get('many2many');


console.log(' I my custom widget file ');

var FieldMany2ManyCheckBoxes = AbstractField.extend({
    template: 'FieldMany2Manyselect',
    events: _.extend({}, AbstractField.prototype.events, {
        change: '_onChange',
    }),
    specialData: "_fetchSpecialRelation",
    supportedFieldTypes: ['many2many'],
    init: function () {
        console.log('in inint function ');

        this._super.apply(this, arguments);
        console.log(arguments)
//        console.log(this.record.specialData[this.name]);
        console.log(this);
        this.m2mValues = this.record.specialData[this.name];
    },

    //--------------------------------------------------------------------------
    // Public
    //--------------------------------------------------------------------------

    isSet: function () {
        return true;
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @private
     */
    _render: function () {
    console.log(' in render function ')
        var self = this;
        this._super.apply(this, arguments);
        _.each(this.value.res_ids, function (id) {
            self.$('input[data-record-id="' + id + '"]').prop('checked', true);
        });
    },
    /**
     * @private
     */
    _renderReadonly: function () {
    console.log(' in render  redaronlyu')
        this.$("input").prop("disabled", true);
    },

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    /**
     * @private
     */
    _onChange: function () {
        var ids = _.map(this.$('input:checked'), function (input) {
            return $(input).data("record-id");
        });
        this._setValue({
            operation: 'REPLACE_WITH',
            ids: ids,
        });
    },
});


//var FieldMany2Many_select = FieldX2Many.extend({
//    className: 'o_field_many2many',
//    supportedFieldTypes: ['many2many'],
//    template: 'FieldMany2Manyselect',
//
//
//    /**
//     * @override
//     */
//    init: function () {
//        this._super.apply(this, arguments);
//        this.nodeOptions = _.defaults(this.nodeOptions, {
//            create_text: _t('Add'),
//        });
//    },
//
//    //--------------------------------------------------------------------------
//    // Handlers
//    //--------------------------------------------------------------------------
//
//    /**
//     * Opens a SelectCreateDialog.
//     *
//     * @override
//     * @private
//     * @param {OdooEvent|MouseEvent} ev this event comes either from the 'Add
//     *   record' link in the list editable renderer, or from the 'Create' button
//     *   in the kanban view
//     */
//    _onAddRecord: function (ev) {
//        var self = this;
//        ev.stopPropagation();
//
//        var domain = this.record.getDomain({fieldName: this.name});
//
//        new dialogs.SelectCreateDialog(this, {
//            res_model: this.field.relation,
//            domain: domain.concat(["!", ["id", "in", this.value.res_ids]]),
//            context: this.record.getContext(this.recordParams),
//            title: _t("Add: ") + this.string,
//            no_create: this.nodeOptions.no_create || !this.activeActions.create,
//            fields_view: this.attrs.views.form,
//            on_selected: function (records) {
//                var resIDs = _.pluck(records, 'id');
//                var newIDs = _.difference(resIDs, self.value.res_ids);
//                if (newIDs.length) {
//                    var values = _.map(newIDs, function (id) {
//                        return {id: id};
//                    });
//                    self._setValue({
//                        operation: 'ADD_M2M',
//                        ids: values,
//                    });
//                }
//            }
//        }).open();
//    },
//    /**
//     * Intercepts the 'open_record' event to edit its data and lets it bubble up
//     * to the form view.
//     *
//     * @private
//     * @param {OdooEvent} ev
//     */
//    _onOpenRecord: function (ev) {
//        var self = this;
//        _.extend(ev.data, {
//            context: this.record.getContext(this.recordParams),
//            domain: this.record.getDomain(this.recordParams),
//            fields_view: this.attrs.views && this.attrs.views.form,
//            on_saved: function () {
//                self._setValue({operation: 'TRIGGER_ONCHANGE'}, {forceChange: true});
//                self.trigger_up('reload', {db_id: ev.data.id});
//            },
//            readonly: this.mode === 'readonly',
//            string: this.string,
//        });
//    },
//});
//
//
//registry
//    .add('many2many_selection', FieldMany2Many_select)
//

var Many2ManySelectable = relationalFields.FieldMany2Many.extend({
		multi_selection: true,
		template: 'FieldMany2Manyselect',
		init: function() {
		    console.log('Many2ManySelectable init')
	        this._super.apply(this, arguments);
	    },
	    start: function()
	    {
	    	this._super.apply(this, arguments);
	    	var self=this;
	    	this.$el.prepend(QWeb.render("Many2ManySelectable", {widget: this}));
	        this.$el.find(".ep_button_delete").click(function(){

	        	var result = confirm("Do you really want to delete this record?");
				if (result) {
	        	self.action_delete_selected_lines();
				}

	        });
	   },

	   action_delete_selected_lines:function()
	   {
		   	var self = this;
		   	var data = self.get_selected_ids_one2many();
			var selected_ids = data[0];
			var selected_state = data[2];
			if (selected_ids.length === 0)
			{
					this.do_warn(_t("You must choose at least one record."));
					return false;
			}
//			var model_obj=new Model(this.dataset.model);
//			model_obj.call('delete_analyses_and_ws',[selected_ids],{context:self.dataset.context})
//				.then(function(result){
//				location.reload();
//				});
	   },
	   get_selected_ids_one2many: function ()
	   {
	       var ids =[];
	       var results = [];
	       var states = [];
	       this.$el.find('th.oe_list_record_selector input:checked')
	               .closest('tr').each(function () {
					ids.push(parseInt($(this).context.dataset.id));
					states.push($(this).find('[data-field]').filter(function() {
					    return $(this).data('field').toLowerCase() == 'state';
					}).text());
					results.push({id:parseInt($(this).context.dataset.id), result:$(this).find('[data-field]').filter(function() {
					    return $(this).data('field').toLowerCase() == 'result';
					}).text()});

	       });
	       results.sort(function(a, b) {
			return a.id - b.id;
				});
	       return [ids,results,states];
	   },
	});
//	core.form_widget_registry.add('many2many_selectable', Many2ManySelectable);
//	return Many2ManySelectable;
registry
    .add('many2many_selectable', FieldMany2ManyCheckBoxes)


});
