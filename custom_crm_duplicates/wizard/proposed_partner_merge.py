# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from ast import literal_eval
import functools
import itertools
import logging
import psycopg2
import datetime

from odoo import api, fields, models, exceptions
from odoo import SUPERUSER_ID, _
from odoo.http import request
from odoo.exceptions import ValidationError, UserError
from odoo.tools import mute_logger
from odoo.addons.custom_crm_duplicates.models.custom_partner import HelperMethods
from datetime import datetime


_logger = logging.getLogger('custom.partner.merge')

class MergePartnerLine(models.TransientModel):

    _name = 'custom.partner.merge.line'
    _order = 'min_id asc'

    wizard_id = fields.Many2one('custom.partner.merge.automatic.wizard', 'Wizard')
    min_id = fields.Integer('MinID')
    aggr_ids = fields.Char('Ids', required=True)
    check = fields.Char('Check')


class MergePartnerAutomatic(models.TransientModel):
    """
        The idea behind this wizard is to create a list of potential partners to
        merge. We use two objects, the first one is the wizard for the end-user.
        And the second will contain the partner list to merge.
    """

    _name = 'custom.partner.merge.automatic.wizard'


    @api.model
    def default_get(self, fields):
        desired_group_name = self.env['res.groups'].search([('name', '=', 'Allow Contact merge')])
        if self.env.user.id not in desired_group_name.users.ids:
            raise UserError(_("You are not allowed to perform this operation."))

        res = super(MergePartnerAutomatic, self).default_get(fields)
        active_ids = self.env.context.get('active_ids')
        if self.env.context.get('active_model') == 'res.partner' and active_ids:
            res['state'] = 'selection'
            res['partner_ids'] = list(self._get_ordered_partner(active_ids)._ids)
            res['dst_partner_id'] = self._get_ordered_partner(active_ids)[0].id
            res['personal_information'] = res['dst_partner_id']
            res['communication_data'] = res['dst_partner_id']
            res['donor_attribute'] = res['dst_partner_id']
            res['min_id'] = res['dst_partner_id']
            res['check'] = 'check_bypass'
        return res

    # Group by
    group_by_email = fields.Boolean('Email')
    show_remove = fields.Boolean('Show Remove', default=False)
    group_by_name = fields.Boolean('Name')
    group_by_is_company = fields.Boolean('Is Company')
    group_by_vat = fields.Boolean('VAT')
    group_by_parent_id = fields.Boolean('Parent Company')

    state = fields.Selection([
        ('option', 'Option'),
        ('selection', 'Selection'),
        ('finished', 'Finished')
    ], readonly=True, required=True, string='State', default='option')

    offset = fields.Integer('Offset')
    limit = fields.Integer('Limit')
    created_on = fields.Date(string='Date Created On')
    check_1 = fields.Boolean('Check 1')
    check_2 = fields.Boolean('Check 2')
    check_3 = fields.Boolean('Check 3')
    check_4 = fields.Boolean('Check 4')

    number_group = fields.Integer('Group of Contacts', readonly=True)
    current_line_id = fields.Many2one('custom.partner.merge.line', string='Current Line')
    line_ids = fields.One2many('custom.partner.merge.line', 'wizard_id', string='Lines')
    partner_ids = fields.Many2many('res.partner', string='Contacts')
    dst_partner_id = fields.Many2one('res.partner', string='Destination Contact')
    filter_partner_id = fields.Many2one('res.partner', string='Donor Name')
    partner_ids_range = fields.Char(string='Donor ID Range')
    min_id = fields.Many2one('res.partner', string='Primary Contact', readonly=True)
    code_min_id = fields.Char(string='Primary Contact code', readonly=True, compute="_compute_code")
    personal_information = fields.Many2one('res.partner', string='Personal Information')
    communication_data = fields.Many2one('res.partner', string='Communication Data')
    donor_attribute = fields.Many2one('res.partner', string='Donor Attribute')
    type_relation_id = fields.Many2one(
        comodel_name='res.partner.relation.type.selection',
        string='Relation Type')

    exclude_contact = fields.Boolean('A user associated to the contact')
    exclude_journal_item = fields.Boolean('Journal Items associated to the contact')
    maximum_group = fields.Integer('Maximum of Group of Contacts')
    check = fields.Char('Check')



    # ----------------------------------------
    # Update method. Core methods to merge steps
    # ----------------------------------------

    @api.multi
    @api.depends('min_id')
    def _compute_code(self):
        for rec in self:
            rec.code_min_id = rec.min_id.code

    def _get_fk_on(self, table):
        """ return a list of many2one relation with the given table.
            :param table : the name of the sql table to return relations
            :returns a list of tuple 'table name', 'column name'.
        """
        query = """
            SELECT cl1.relname as table, att1.attname as column
            FROM pg_constraint as con, pg_class as cl1, pg_class as cl2, pg_attribute as att1, pg_attribute as att2
            WHERE con.conrelid = cl1.oid
                AND con.confrelid = cl2.oid
                AND array_lower(con.conkey, 1) = 1
                AND con.conkey[1] = att1.attnum
                AND att1.attrelid = cl1.oid
                AND cl2.relname = %s
                AND att2.attname = 'id'
                AND array_lower(con.confkey, 1) = 1
                AND con.confkey[1] = att2.attnum
                AND att2.attrelid = cl2.oid
                AND con.contype = 'f'
        """
        self._cr.execute(query, (table,))
        return self._cr.fetchall()

    @api.model
    def _update_foreign_keys(self, src_partners, dst_partner):
        """ Update all foreign key from the src_partner to dst_partner. All many2one fields will be updated.
            :param src_partners : merge source res.partner recordset (does not include destination one)
            :param dst_partner : record of destination res.partner
        """
        _logger.debug('_update_foreign_keys for dst_partner: %s for src_partners: %s', dst_partner.id, str(src_partners.ids))

        # find the many2one relation to a partner
        Partner = self.env['res.partner']
        relations = self._get_fk_on('res_partner')

        for table, column in relations:
            if 'custom_partner_merge_' in table:  # ignore tables
                continue
            if 'prt_phone' in table or 'cx_address' in table:
                continue

            # get list of columns of current table (exept the current fk column)
            query = "SELECT column_name FROM information_schema.columns WHERE table_name LIKE '%s'" % (table)
            self._cr.execute(query, ())
            columns = []
            for data in self._cr.fetchall():
                if data[0] != column:
                    columns.append(data[0])

            # do the update for the current table/column in SQL
            query_dic = {
                'table': table,
                'column': column,
                'value': columns[0],
            }
            if len(columns) <= 1:
                # unique key treated
                query = """
                    UPDATE "%(table)s" as ___tu
                    SET %(column)s = %%s
                    WHERE
                        %(column)s = %%s AND    
                        NOT EXISTS (
                            SELECT 1
                            FROM "%(table)s" as ___tw
                            WHERE
                                %(column)s = %%s AND
                                ___tu.%(value)s = ___tw.%(value)s
                        )""" % query_dic
                for partner in src_partners:
                    self._cr.execute(query, (dst_partner.id, partner.id, dst_partner.id))
            else:
                try:
                    with mute_logger('odoo.sql_db'), self._cr.savepoint():
                        query = 'UPDATE "%(table)s" SET %(column)s = %%s WHERE %(column)s IN %%s' % query_dic
                        self._cr.execute(query, (dst_partner.id, tuple(src_partners.ids),))

                        # handle the recursivity with parent relation
                        if column == Partner._parent_name and table == 'res_partner':
                            query = """
                                WITH RECURSIVE cycle(id, parent_id) AS (
                                        SELECT id, parent_id FROM res_partner
                                    UNION
                                        SELECT  cycle.id, res_partner.parent_id
                                        FROM    res_partner, cycle
                                        WHERE   res_partner.id = cycle.parent_id AND
                                                cycle.id != cycle.parent_id
                                )
                                SELECT id FROM cycle WHERE id = parent_id AND id = %s
                            """
                            self._cr.execute(query, (dst_partner.id,))
                            # NOTE JEM : shouldn't we fetch the data ?
                except psycopg2.Error:
                    # updating fails, most likely due to a violated unique constraint
                    # keeping record with nonexistent partner_id is useless, better delete it
                    query = 'DELETE FROM "%(table)s" WHERE "%(column)s" IN %%s' % query_dic
                    self._cr.execute(query, (tuple(src_partners.ids),))

    @api.model
    def _update_reference_fields(self, src_partners, dst_partner):
        """ Update all reference fields from the src_partner to dst_partner.
            :param src_partners : merge source res.partner recordset (does not include destination one)
            :param dst_partner : record of destination res.partner
        """
        _logger.debug('_update_reference_fields for dst_partner: %s for src_partners: %r', dst_partner.id, src_partners.ids)

        def update_records(model, src, field_model='model', field_id='res_id'):
            Model = self.env[model] if model in self.env else None
            if Model is None:
                return
            records = Model.sudo().search([(field_model, '=', 'res.partner'), (field_id, '=', src.id)])
            try:
                with mute_logger('odoo.sql_db'), self._cr.savepoint():
                    return records.sudo().write({field_id: dst_partner.id})
            except psycopg2.Error:
                # updating fails, most likely due to a violated unique constraint
                # keeping record with nonexistent partner_id is useless, better delete it
                return records.sudo().unlink()

        update_records = functools.partial(update_records)

        for partner in src_partners:
            update_records('calendar', src=partner, field_model='model_id.model')
            update_records('ir.attachment', src=partner, field_model='res_model')
            update_records('mail.followers', src=partner, field_model='res_model')
            update_records('mail.message', src=partner)
            update_records('ir.model.data', src=partner)

        records = self.env['ir.model.fields'].search([('ttype', '=', 'reference')])
        for record in records.sudo():
            try:
                Model = self.env[record.model]
                field = Model._fields[record.name]
            except KeyError:
                # unknown model or field => skip
                continue

            if field.compute is not None:
                continue

            for partner in src_partners:
                records_ref = Model.sudo().search([(record.name, '=', 'res.partner,%d' % partner.id)])
                values = {
                    record.name: 'res.partner,%d' % dst_partner.id,
                }
                records_ref.sudo().write(values)

    @api.model
    def _update_values(self, src_partners, dst_partner):
        """ Update values of dst_partner with the ones from the src_partners.
            :param src_partners : recordset of source res.partner
            :param dst_partner : record of destination res.partner
        """
        _logger.debug('_update_values for dst_partner: %s for src_partners: %r', dst_partner.id, src_partners.ids)

        model_fields = dst_partner.fields_get().keys()

        def write_serializer(item):
            if isinstance(item, models.BaseModel):
                return item.id
            else:
                return item
        # get all fields that are not computed or x2many
        values = dict()
        for column in model_fields:
            field = dst_partner._fields[column]
            if field.type not in ('many2many', 'one2many') and field.compute is None:
                for item in itertools.chain(src_partners, [dst_partner]):
                    if item[column]:
                        values[column] = write_serializer(item[column])
        # remove fields that can not be updated (id and parent_id)
        values.pop('id', None)
        parent_id = values.pop('parent_id', None)
        dst_partner.write(values)
        # try to update the parent_id
        if parent_id and parent_id != dst_partner.id:
            try:
                dst_partner.write({'parent_id': parent_id})
            except ValidationError:
                _logger.info('Skip recursive partner hierarchies for parent_id %s of partner: %s', parent_id, dst_partner.id)

    def _merge(self, partner_ids, dst_partner=None, auto_merge=False):
        """ private implementation of merge partner
            :param partner_ids : ids of partner to merge
            :param dst_partner : record of destination res.partner
        """
        Partner = self.env['res.partner']
        partner_ids = Partner.browse(partner_ids).exists()
        if len(partner_ids) < 2:
            return

        # if len(partner_ids) > 3:
        #     raise UserError(_("For safety reasons, you cannot merge more than 3 contacts together. You can re-open the wizard several times if needed."))

        # check if the list of partners to merge contains child/parent relation
        child_ids = self.env['res.partner']
        for partner_id in partner_ids:
            child_ids |= Partner.search([('id', 'child_of', [partner_id.id])]) - partner_id
        if partner_ids & child_ids and auto_merge is False:

            raise UserError(_("You cannot merge a contact with one of his parent."))
        else:
            partner_ids = partner_ids - child_ids

        # check only admin can merge partners with different emails
        # if SUPERUSER_ID != self.env.uid and len(set(partner.email for partner in partner_ids)) > 1:
        #     raise UserError(_("All contacts must have the same email. Only the Administrator can merge contacts with different emails."))

        # remove dst_partner from partners to merge
        if dst_partner and dst_partner in partner_ids:
            src_partners = partner_ids - dst_partner
        else:
            ordered_partners = self._get_ordered_partner(partner_ids.ids)
            dst_partner = ordered_partners[-1]
            src_partners = ordered_partners[:-1]
        _logger.info("dst_partner: %s", dst_partner.id)

        # FIXME: is it still required to make and exception for account.move.line since accounting v9.0 ?
        # if SUPERUSER_ID != self.env.uid and 'account.move.line' in self.env and self.env['account.move.line'].sudo().search([('partner_id', 'in', [partner.id for partner in src_partners])]):
        #     raise UserError(_("Only the destination contact may be linked to existing Journal Items. Please ask the Administrator if you need to merge several contacts linked to existing Journal Items."))

        # call sub methods to do the merge
        self._update_foreign_keys(src_partners, dst_partner)
        self._update_reference_fields(src_partners, dst_partner)
        # self._update_values(src_partners, dst_partner)

        _logger.info('(uid = %s) merged the partners %r with %s', self._uid, src_partners.ids, dst_partner.id)
        for p in src_partners:
            code = str(p.code)+'' if p.code else " "
            name = str(p.name)+'<br/>' if p.name else " "
            address = str(p.x_studio_field_VfR7W)+'<br/>' if p.x_studio_field_VfR7W else " "
            email = str(p.email)+'<br/>' if p.email else " "
            phone = str(p.phone)+'<br/>' if p.phone else " "

            dst_partner.message_post(body='Merged with the following partner: ' + code + '-' + name + '' + ' ' + address +\
                                          ' ' + email + ' ' + phone)

        # delete source partner, since they are merged
        src_partners.unlink()

    # ----------------------------------------
    # Helpers
    # ----------------------------------------

    @api.model
    def _generate_query(self, fields, maximum_group=100):
        """ Build the SQL query on res.partner table to group them according to given criteria
            :param fields : list of column names to group by the partners
            :param maximum_group : limit of the query
        """
        # make the list of column to group by in sql query
        sql_fields = []
        for field in fields:
            if field in ['email', 'name']:
                sql_fields.append('lower(%s)' % field)
            elif field in ['vat']:
                sql_fields.append("replace(%s, ' ', '')" % field)
            else:
                sql_fields.append('"'+field+'"')
        group_fields = ', '.join(sql_fields)

        # where clause : for given group by columns, only keep the 'not null' record
        filters = []
        for field in fields:
            if field in ['email', 'name', 'vat']:
                filters.append((field, 'IS NOT', 'NULL'))
        criteria = ' AND '.join('%s %s %s' % (field, operator, value) for field, operator, value in filters)
        if criteria:
            criteria += ' AND active is true'
        else:
            criteria = '  active is true'

        criteria += ' AND code !=\'\' '
        if self.created_on:
            criteria += ' AND date(create_date) =  \''+str(self.created_on)+'\''

        # build the query
        text = [
            "SELECT min(id), array_agg(id)",
            "FROM res_partner",
        ]

        if criteria:
            text.append('WHERE %s' % criteria)

        text.extend([
            "GROUP BY %s" % group_fields,
            # "HAVING COUNT(*) >= 2",
            "ORDER BY min(id)",
        ])
        #
        # if maximum_group:
        #     text.append("LIMIT %s" % maximum_group,)
        if self.limit != 0:
            text.append("LIMIT %s" % self.limit,)

        if self.offset != 0:
            text.append("OFFSET %s" % self.offset,)
        print(' '.join(text))
        return ' '.join(text)

    @api.model
    def _generate_records_for_range(self, range):
        """ Build the SQL query on res.partner table to group them according to given criteria
            :param fields : list of column names to group by the partners
            :param maximum_group : limit of the query
        """
        # make the list of column to group by in sql query

        # build the query
        query = []

        text = [
            "SELECT min(id), array_agg(id)",
            "FROM res_partner",
        ]
        text.append('WHERE id <= %s' % range[0]+' AND '+'id >= %s' % range[0])

        print(' '.join(text))
        return ' '.join(text)


    @api.model
    def _generate_query_auto_merge(self, fields, maximum_group=100):
        """ Build the SQL query on res.partner table to group them according to given criteria
            :param fields : list of column names to group by the partners
            :param maximum_group : limit of the query
        """
        # make the list of column to group by in sql query
        sql_fields = []
        for field in fields:
            if field in ['email', 'name']:
                sql_fields.append('lower(%s)' % field)
            elif field in ['vat']:
                sql_fields.append("replace(%s, ' ', '')" % field)
            else:
                sql_fields.append('"'+field+'"')
        group_fields = ', '.join(sql_fields)

        # where clause : for given group by columns, only keep the 'not null' record
        filters = []
        for field in fields:
            if field in ['email', 'name', 'vat']:
                filters.append((field, 'IS NOT', 'NULL'))
        criteria = ' AND '.join('%s %s %s' % (field, operator, value) for field, operator, value in filters)
        if criteria:
            criteria += ' AND active is true'
        else:
            criteria = '  active is true'

        criteria += ' AND code !=\'\' AND create_date > current_timestamp - interval \' 1 day \''
        # build the query
        text = [
            "SELECT min(id), array_agg(id)",
            "FROM res_partner",
        ]


        text.append('WHERE %s' % criteria)

        text.extend([
            "GROUP BY %s" % group_fields,
            # "HAVING COUNT(*) >= 2",
            "ORDER BY min(id)",
        ])
        #
        # if maximum_group:
        #     text.append("LIMIT %s" % maximum_group,)
        if self.limit != 0:
            text.append("LIMIT %s" % self.limit,)

        if self.offset != 0:
            text.append("OFFSET %s" % self.offset,)
        print(' '.join(text))
        return ' '.join(text)


    @api.model
    def _compute_selected_groupby(self, check_fields):
        """ Returns the list of field names the partner can be grouped (as merge
            criteria) according to the option checked on the wizard
        """
        groups = []
        group_by_prefix = 'group_by_'
        for field in check_fields:
            groups.append(field.name)
        #
        # for field_name in self._fields:
        #     if field_name.startswith(group_by_prefix):
        #         if getattr(self, field_name, False):
        #             groups.append(field_name[len(group_by_prefix):])

        # if not groups:
        #     raise UserError(_("You have to specify a filter for your selection"))

        return groups

    @api.model
    def _partner_use_in(self, aggr_ids, models):
        """ Check if there is no occurence of this group of partner in the selected model
            :param aggr_ids : stringified list of partner ids separated with a comma (sql array_agg)
            :param models : dict mapping a model name with its foreign key with res_partner table
        """
        return any(
            self.env[model].search_count([(field, 'in', aggr_ids)])
            for model, field in models.items()
        )

    @api.model
    def _get_ordered_partner(self, partner_ids):
        """ Helper : returns a `res.partner` recordset ordered by create_date/active fields
            :param partner_ids : list of partner ids to sort
        """
        # return self.env['res.partner'].browse(partner_ids).sorted(
        #     key=lambda p: (p.active, (p.x_studio_field_chJhB or '')),
        #     reverse=True,
        # )
        return self.env['res.partner'].search([('id', 'in', partner_ids)], order="id asc")

    @api.multi
    def _compute_models(self):
        """ Compute the different models needed by the system if you want to exclude some partners. """
        model_mapping = {}
        if self.exclude_contact:
            model_mapping['res.users'] = 'partner_id'
        if 'account.move.line' in self.env and self.exclude_journal_item:
            model_mapping['account.move.line'] = 'partner_id'
        return model_mapping

    # ----------------------------------------
    # Actions
    # ----------------------------------------

    @api.multi
    def action_skip(self):
        """ Skip this wizard line. Don't compute any thing, and simply redirect to the new step."""
        if self.current_line_id:
            self.current_line_id.unlink()
        return self._action_next_screen()

    @api.multi
    def _action_next_screen(self):
        """ return the action of the next screen ; this means the wizard is set to treat the
            next wizard line. Each line is a subset of partner that can be merged together.
            If no line left, the end screen will be displayed (but an action is still returned).
        """
        self.invalidate_cache() # FIXME: is this still necessary?
        values = {}
        if self.line_ids:
            # in this case, we try to find the next record.
            current_line = self.line_ids[0]
            current_partner_ids = literal_eval(current_line.aggr_ids)
            min_id = current_line.min_id
            if len(current_partner_ids) == 2:
                show_remove = True
            else:
                show_remove = False
            values.update({
                'current_line_id': current_line.id,
                'partner_ids': [(6, 0, current_partner_ids)],
                'dst_partner_id': self._get_ordered_partner(current_partner_ids)[0].id,
                'personal_information': self._get_ordered_partner(current_partner_ids)[-0].id,
                'communication_data': self._get_ordered_partner(current_partner_ids)[-0].id,
                'donor_attribute': self._get_ordered_partner(current_partner_ids)[-0].id,
                'state': 'selection',
                'min_id': min_id,
                'show_remove':show_remove,
                'check':current_line.check
            })
        else:
            values.update({
                'current_line_id': False,
                'partner_ids': [],
                'state': 'finished',
            })

        self.write(values)
        if self.state == 'selection':
            self.type_relation_id = False
        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'res_id': self.id,
            'view_mode': 'form',
            'target': 'new',

        }

    @api.multi
    def _process_query(self, query):
        """ Execute the select request and write the result in this wizard
            :param query : the SQL query used to fill the wizard line
        """
        self.ensure_one()
        model_mapping = self._compute_models()

        # group partner query
        self._cr.execute(query)

        counter = 0
        for min_id, aggr_ids in self._cr.fetchall():
            # To ensure that the used partners are accessible by the user
            partners = self.env['res.partner'].search([('id', 'in', aggr_ids)])
            if len(partners) < 2:
                continue

            # exclude partner according to options
            if model_mapping and self._partner_use_in(partners.ids, model_mapping):
                continue

            self.env['custom.partner.merge.line'].create({
                'wizard_id': self.id,
                'min_id': min_id,
                'aggr_ids': partners.ids,
            })
            counter += 1

        self.write({
            'state': 'selection',
            'number_group': counter,
        })

        _logger.info("counter: %s", counter)

    @api.multi
    def action_start_manual_process(self):
        """ Start the process 'Merge with Manual Check'. Fill the wizard according to the group_by and exclude
            options, and redirect to the first step (treatment of first wizard line). After, for each subset of
            partner to merge, the wizard will be actualized.
                - Compute the selected groups (with duplication)
                - If the user has selected the 'exclude_xxx' fields, avoid the partners
        """
        self.ensure_one()
        # company_id = self.env.user.company_id.sudo()
        # check1_field = company_id.check1_fields
        if not self.check_1 and not self.check_2 and not self.check_3 and not self.check_4:
            raise UserError(_("Please select at least one check"))

        self._process_query_fuzzy()
        return self._action_next_screen()

    @api.multi
    def _process_query_fuzzy(self):
        checked_ids = []
        company_id = self.env.user.company_id.sudo()
        check1_field = company_id.check1_fields
        check1_percent = company_id.check1_proposed_merge

        if not check1_field:
            raise UserError(_("Please configure the fields for matching "))

        check2_fields = company_id.check2_fields
        check2_percent = company_id.check2_proposed_merge

        if not check2_fields:
            raise UserError(_("Please configure the fields for matching "))

        check3_fields = company_id.check3_fields
        check3_percent = company_id.check3_proposed_merge
        if not check3_fields:
            raise UserError(_("Please configure the fields for matching "))

        check4_fields = company_id.check4_fields
        check4_percent = company_id.check4_proposed_merge
        if not check4_fields:
            raise UserError(_("Please configure the fields for matching "))

        counter = 0
        ignore_matches = self._get_ignore_match()

        if self.check_1:
            # if self.partner_ids_range:
            #     range = self.partner_ids_range.split('-')
            #     query1 = self._generate_records_for_range(range)
            #     self._cr.execute(query1)
            #     main_records = self._cr.fetchall()
            if self.filter_partner_id:
                main_records = [(self.filter_partner_id.id, {})]
            else:
                groups1 = self._compute_selected_groupby(check1_field)
                query1 = self._generate_query(groups1, self.maximum_group)
                self._cr.execute(query1)
                main_records = self._cr.fetchall()

            for min_id, aggr_ids in main_records:
                primary_record = request.env['res.partner'].search([('id', '=', min_id)], limit=1)

                matched_records = primary_record._get_similarity_result_fuzzy_percent(check1_field, primary_record.id,\
                                                                                      check1_percent)
                ids = []
                if len(matched_records) >= 2:
                    for rec in matched_records:
                        if primary_record.id in ignore_matches.keys() and rec['id'] in ignore_matches[primary_record.id]:
                            pass
                        else:
                            ids.append(rec['id'])
                            checked_ids.append(rec['id'])

                    sorted_ids = sorted(ids)

                    if len(sorted_ids) >= 2:
                        self.env['custom.partner.merge.line'].create({
                            'wizard_id': self.id,
                            'min_id': primary_record.id,
                            'aggr_ids': sorted_ids,
                            'check': 'check1'
                        })
                        counter += 1

        if self.check_2:
            if not self.filter_partner_id:
                groups2 = self._compute_selected_groupby(check2_fields)
                query2 = self._generate_query(groups2, self.maximum_group)
                self._cr.execute(query2)
                main_records = self._cr.fetchall()
            else:
                main_records = [(self.filter_partner_id.id,{})]
            for min_id, aggr_ids in main_records:
                primary_record = request.env['res.partner'].search([('id', '=', min_id)], limit=1)
                matched_records = primary_record._get_similarity_result_fuzzy_percent(check2_fields, primary_record.id,\
                                                                                      check2_percent)
                ids = []
                if len(matched_records) >= 2:
                    for rec in matched_records:
                        if primary_record.id in ignore_matches.keys() and rec['id'] in ignore_matches[primary_record.id]:
                            pass
                        else:
                            if rec['id'] not in ids and rec['id'] not in checked_ids:
                                ids.append(rec['id'])
                                checked_ids.append(rec['id'])
                    sorted_ids = sorted(ids)
                    if len(sorted_ids) >= 2:
                        self.env['custom.partner.merge.line'].create({
                            'wizard_id': self.id,
                            'min_id': primary_record.id,
                            'aggr_ids': sorted_ids,
                            'check': 'check2'
                        })
                        counter += 1
        if self.check_3:
            if not self.filter_partner_id:
                groups3 = self._compute_selected_groupby(check3_fields)
                query3 = self._generate_query(groups3, self.maximum_group)
                self._cr.execute(query3)
                main_records = self._cr.fetchall()
            else:
                main_records = [(self.filter_partner_id.id,{})]

            for min_id, aggr_ids in main_records:
                primary_record = request.env['res.partner'].search([('id', '=', min_id)], limit=1)
                matched_records = primary_record._get_similarity_result_fuzzy_percent(check3_fields, primary_record.id,\
                                                                                      check3_percent)
                ids = []
                if len(matched_records) >= 2:
                    for rec in matched_records:
                        if primary_record.id in ignore_matches.keys() and rec['id'] in ignore_matches[primary_record.id]:
                            pass
                        else:
                            if rec['id'] not in ids and rec['id'] not in checked_ids:
                                ids.append(rec['id'])
                                checked_ids.append(rec['id'])

                    sorted_ids = sorted(ids)
                    if len(sorted_ids) >= 2:
                        self.env['custom.partner.merge.line'].create({
                            'wizard_id': self.id,
                            'min_id': primary_record.id,
                            'aggr_ids': sorted_ids,
                            'check': 'check3'
                        })
                        counter += 1

        if self.check_4:
            if not self.filter_partner_id:
                groups4 = self._compute_selected_groupby(check4_fields)
                query4 = self._generate_query(groups4, self.maximum_group)
                self._cr.execute(query4)
                main_records = self._cr.fetchall()
            else:
                main_records = [(self.filter_partner_id.id,{})]

            for min_id, aggr_ids in main_records:
                primary_record = request.env['res.partner'].search([('id', '=', min_id)], limit=1)
                matched_records = primary_record._get_similarity_result_fuzzy_percent(check4_fields, primary_record.id,\
                                                                                      check4_percent)
                ids = []
                if len(matched_records) >= 2:
                    for rec in matched_records:
                        if primary_record.id in ignore_matches.keys() and rec['id'] in ignore_matches[primary_record.id]:
                            pass
                        else:
                            if rec['id'] not in ids and rec['id'] not in checked_ids:
                                ids.append(rec['id'])
                                checked_ids.append(rec['id'])

                    sorted_ids = sorted(ids)
                    if len(sorted_ids) >= 2:
                        self.env['custom.partner.merge.line'].create({
                            'wizard_id': self.id,
                            'min_id': primary_record.id,
                            'aggr_ids': sorted_ids,
                            'check': 'check4'
                        })
                        counter += 1
        self.write({
            'state': 'selection',
            'number_group': counter,
        })

    def _get_ignore_match(self):
        ignore_matches = {}
        removed_matchs = self.env['remove.match'].search([])
        if removed_matchs:
            for removed_match in removed_matchs:
                if removed_match.primary_contact.id in ignore_matches.keys():
                    ignore_matches[removed_match.primary_contact.id].append(removed_match.secondary_contact.id)
                else:
                    ignore_matches[removed_match.primary_contact.id] = [removed_match.secondary_contact.id]
        merge_matchs = self.env['merge.list'].search([])
        if merge_matchs:
            for merge_match in merge_matchs:
                if merge_match.primary_contact_merge.id in ignore_matches.keys():
                    ignore_matches[merge_match.primary_contact_merge.id].append(merge_match.secondary_contact)
                else:
                    ignore_matches[merge_match.primary_contact_merge.id] = [merge_match.secondary_contact]
        return ignore_matches

    @api.multi
    def action_start_automatic_process(self):
        """ Start the process 'Merge Automatically'. This will fill the wizard with the same mechanism as 'Merge
            with Manual Check', but instead of refreshing wizard with the current line, it will automatically process
            all lines by merging partner grouped according to the checked options.
        """
        self.ensure_one()
        self.action_start_manual_process()  # here we don't redirect to the next screen, since it is automatic process
        self.invalidate_cache() # FIXME: is this still necessary?

        for line in self.line_ids:
            partner_ids = literal_eval(line.aggr_ids)
            self._merge(partner_ids)
            line.unlink()
            self._cr.commit()  # TODO JEM : explain why

        self.write({'state': 'finished'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'res_id': self.id,
            'view_mode': 'form',
            'target': 'new',

        }

    @api.multi
    def parent_migration_process_cb(self):
        self.ensure_one()

        query = """
            SELECT
                min(p1.id),
                array_agg(DISTINCT p1.id)
            FROM
                res_partner as p1
            INNER join
                res_partner as p2
            ON
                p1.email = p2.email AND
                p1.name = p2.name AND
                (p1.parent_id = p2.id OR p1.id = p2.parent_id)
            WHERE
                p2.id IS NOT NULL
            GROUP BY
                p1.email,
                p1.name,
                CASE WHEN p1.parent_id = p2.id THEN p2.id
                    ELSE p1.id
                END
            HAVING COUNT(*) >= 2
            ORDER BY
                min(p1.id)
        """

        self._process_query(query)

        for line in self.line_ids:
            partner_ids = literal_eval(line.aggr_ids)
            self._merge(partner_ids)
            line.unlink()
            self._cr.commit()

        self.write({'state': 'finished'})

        self._cr.execute("""
            UPDATE
                res_partner
            SET
                is_company = NULL,
                parent_id = NULL
            WHERE
                parent_id = id
        """)

        return {
            'type': 'ir.actions.act_window',
            'res_model': self._name,
            'res_id': self.id,
            'view_mode': 'form',
            'target': 'new',
        }

    @api.multi
    def action_update_all_process(self):
        self.ensure_one()
        self.parent_migration_process_cb()

        # NOTE JEM : seems louche to create a new wizard instead of reuse the current one with updated options.
        # since it is like this from the initial commit of this wizard, I don't change it. yet ...
        wizard = self.create({'group_by_vat': True, 'group_by_email': True, 'group_by_name': True})
        wizard.action_start_automatic_process()

        # NOTE JEM : no idea if this query is usefull
        self._cr.execute("""
            UPDATE
                res_partner
            SET
                is_company = NULL
            WHERE
                parent_id IS NOT NULL AND
                is_company IS NOT NULL
        """)

        return self._action_next_screen()

    @api.multi
    def action_merge(self):
        """ Merge Contact button. Merge the selected partners, and redirect to
            the end screen (since there is no other wizard line to process.
        """

        company_id = self.env.user.company_id.sudo()
        special_values = dict()
        values_email = dict()
        partner_ids = self.partner_ids
        partner_date_list = []
        oldest_since_date = None
        if partner_ids:
            for partner in partner_ids:
                date = partner.x_studio_field_chJhB
                date = datetime.strptime(date, '%Y-%m-%d').date()
                partner_date_list.append(date)
            oldest_since_date = min(partner_date_list)

        if not self.partner_ids:
            self.write({'state': 'finished'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_mode': 'form',
                'target': 'new',
            }
        self._merge_records_save(company_id, partner_ids)
        if self.dst_partner_id.id and self.dst_partner_id.id in self.partner_ids.ids:
            src_partners = partner_ids - self.dst_partner_id
            dst_partner = self.dst_partner_id
        else:
            ordered_partners = self._get_ordered_partner(partner_ids.ids)
            dst_partner = ordered_partners[0]
            src_partners = ordered_partners[1:]

        if not self.dst_partner_id.x_studio_field_uzzbN:
            for record in src_partners:
                if record['x_studio_field_uzzbN']:
                    raise UserError(_("Is it recommended to always merge into the original Donor Record from Raisers Edge."))
        # put all phone, email and address as other/ historical

        if oldest_since_date:
            dst_partner.write({'x_studio_field_chJhB': oldest_since_date})

        for record in partner_ids:
            try:
                street = record['street'] if 'street' in record else ''
                street2 = record['street2'] if 'street2' in record else ''
                city = record['city'] if 'city' in record else ''
                state_id = record['state_id'].id if 'state_id' in record else ''
                zip = record['zip'] if 'zip' in record else ''
                country_id = record['country_id'].id if 'country_id' in record else ''
                phone = record['phone'] if 'phone' in record else ''
                mobile = record['mobile'] if 'mobile' in record else ''
                email = record['email'] if 'email' in record else ''
                add_phone = record['x_studio_field_zbWQS'] if 'x_studio_field_zbWQS' in record else ''
                add_email = record['x_studio_field_Vvzww'] if 'x_studio_field_Vvzww' in record else ''

                self.env['merged.partner'].merge_partner(dst_partner, record)
                self.env['res.partner'].main_merge(city, country_id, email, record, dst_partner, phone, state_id, street,\
                                                   street2, add_phone, add_email, zip, mobile)
                if record != dst_partner:
                    if record.x_studio_field_J1nRO:
                        for notes in record.x_studio_field_J1nRO:
                            vals = {
                                'x_name': notes.x_name,
                                'x_studio_field_udyMp': dst_partner.id,
                                'x_studio_field_dJSgS': notes.x_studio_field_dJSgS,
                                'x_studio_field_ai8rO': notes.x_studio_field_ai8rO.id,
                                'x_studio_field_r7R1I': notes.x_studio_field_r7R1I,
                                'x_studio_field_wvejL': dst_partner.code,
                            }
                            self.env['x_donor_notes'].create(vals)


                if record != dst_partner:
                    if record.address_ids:
                    #     address_list = []
                        for address_id in record.address_ids:
                            if address_id.type != 'a':
                                address_id.partner_id = dst_partner.id
                    #         val_address = {}
                    #         val_address['street'] = address_id.street
                    #         val_address['street2'] = address_id.street2
                    #         val_address['city'] = address_id.city
                    #         val_address['state_id'] = address_id.state_id.id
                    #         val_address['zip'] = address_id.zip
                    #         val_address['country_id'] = address_id.country_id.id
                    #         val_address['partner_id'] = dst_partner.id
                    #         val_address['type'] = address_id.type
                    #         created_address = request.env['cx.address'].create(val_address)
                    #         address_list.append(created_address)
                        # for add in address_list:
                        #     rec = request.env['cx.address'].search(
                        #         [('id', '!=', add.id),('partner_id', '=', dst_partner.id), ('street', '=', add.street),
                        #          ('street2', '=', add.street2), ('zip', '=', add.city), ('state_id', '=', add.state_id.id),
                        #          ('country_id', '=', add.country_id.id)])
                        # if rec:
                        #     for new_address in rec:
                        #         new_address.unlink()

                    if record.phone_number_ids:
                        for phone in record.phone_number_ids:
                            if phone.type != '0' and phone.type != '7':
                                phone.partner_id = dst_partner.id
                            # vals = {}
                            # vals['partner_id'] = dst_partner.id
                            # vals['type'] = phone.type
                            # vals['number'] = phone.number
                            # request.env['prt.phone'].create(vals)

                if self.current_line_id:
                    self.current_line_id.unlink()
            except Exception as e:
                print(e)
                print(' There is an exception ')
                pass
        if self.communication_data.id in partner_ids.ids:
            values = self.env['res.partner'].communication_data_merge(self.communication_data, dst_partner)
            special_values.update(values)

        if self.personal_information.id in partner_ids.ids:
            values = self.env['res.partner'].personal_info_merge(self.personal_information, dst_partner)
            special_values.update(values)
        if self.donor_attribute.id in partner_ids.ids:
            values = self.env['res.partner'].donor_attribute_merge(self.donor_attribute, dst_partner)
            special_values.update(values)

        self._merge(self.partner_ids.ids, self.dst_partner_id)
        dst_partner.write(special_values)

        if dst_partner:
            if dst_partner.phone_number_ids:
                for phone_data in dst_partner.phone_number_ids:
                    if phone_data.type == '10':
                        phone_data.unlink()



                for phone_data in dst_partner.phone_number_ids:
                    if phone_data.type == '9':
                        duplicate_email = request.env['prt.phone'].search([('id', '!=', phone_data.id),
                                                                           ('partner_id', '=', dst_partner.id),
                                                                           ('type', '=', '9'),
                                                                           ('number', '=', phone_data.number)])
                        if duplicate_email:
                            phone_data.unlink()

                    # if phone_data.type == '1':
                    #     duplicate_phone = request.env['prt.phone'].search([('id', '!=', phone_data.id),
                    #                                                    ('partner_id', '=', dst_partner.id),
                    #                                                    ('type', '=', '6'),
                    #                                                    ('number', '=', phone_data.number)])
                    #     if duplicate_phone:
                    #         phone_data.unlink()
                    #         duplicate_phone.write({'type': '1'})


                # for phone_rec in dst_partner.phone_number_ids:
                #     duplicate_phone = None
                #     if phone_rec.type == '1':
                #         duplicate_phone = request.env['prt.phone'].search([('id', '!=', phone_rec.id),
                #                                                        ('partner_id', '=', dst_partner.id),
                #                                                        ('type', '=', '6'),
                #                                                        ('number', '=', phone_rec.number)])
                #         if duplicate_phone:
                #             duplicate_phone.unlink()


        return self._action_next_screen()


    @api.multi
    def action_remove(self):
        """ Remove Contact button. Add the selected partners in the remove merge table , and redirect to
            the end screen (since there is no other wizard line to process.
        """

        merged_list = []
        company_id = self.env.user.company_id.sudo()
        partner_ids = self.partner_ids


        check1_field = company_id.check1_fields
        check1_percent = company_id.check1_proposed_merge

        check2_field = company_id.check2_fields
        check2_percent = company_id.check2_proposed_merge

        check3_field = company_id.check3_fields
        check3_percent = company_id.check3_proposed_merge

        check4_field = company_id.check4_fields
        check4_percent = company_id.check4_proposed_merge


        selected_merge_records = partner_ids

        login = self.env.user.name
        record_ids = []
        record_dict = {}
        for val in selected_merge_records:
            record_ids.append(val[0]['id'])
            record_dict[val[0]['id']] = val

        if self.min_id.id in record_ids:
            record_ids.remove(self.min_id.id)

        matched_records = self.dst_partner_id._get_similarity_result_fuzzy_percent(check1_field, self.min_id.id,\
                                                                                   check1_percent, \
                                                                                   active_ids=record_ids, \
                                                                                   from_wizard=True)
        for record in matched_records:
            if record['id'] in record_dict.keys():
                secondary_id = record['id']
                merged_list.append(record['id'])
                record_ids.remove(record['id'])

                name_rec = record_dict[record['id']]
                record = self.env['remove.match'].create({'primary_contact': self.min_id.id,
                                                          'secondary_contact': secondary_id,
                                                          'match_percentage': HelperMethods.decimal_to_percentage_helper(
                                                              record['result']),
                                                          'match_description': 'Check 1',
                                                          'user_login': login,
                                                          })

        # Check2
        if len(record_ids) == 0:
            if self.current_line_id:
                self.current_line_id.unlink()
            return self._action_next_screen()

        matched_records = self.dst_partner_id._get_similarity_result_fuzzy_percent(check2_field, self.min_id.id, \
                                                                                   check2_percent, \
                                                                                   active_ids=record_ids, \
                                                                                   from_wizard=True)
        for record in matched_records:
            if record['id'] in record_dict.keys():
                secondary_id = record['id']
                merged_list.append(record['id'])
                record_ids.remove(record['id'])

                name_rec = record_dict[record['id']]
                record = self.env['remove.match'].create({'primary_contact': self.min_id.id,
                                                          'secondary_contact': secondary_id,
                                                          'match_percentage': HelperMethods.decimal_to_percentage_helper(
                                                              record['result']),
                                                          'match_description': 'Check 2',
                                                          'user_login': login,
                                                          })

        ######### CHECK 3
        if len(record_ids) == 0:
            if self.current_line_id:
                self.current_line_id.unlink()
            return self._action_next_screen()

        matched_records = self.dst_partner_id._get_similarity_result_fuzzy_percent(check3_field, self.min_id.id, \
                                                                                   check3_percent, \
                                                                                   active_ids=record_ids, \
                                                                                   from_wizard=True)
        for record in matched_records:
            if record['id'] in record_dict.keys():
                secondary_id = record['id']
                merged_list.append(record['id'])
                record_ids.remove(record['id'])

                name_rec = record_dict[record['id']]
                record = self.env['remove.match'].create({'primary_contact': self.min_id.id,
                                                          'secondary_contact': secondary_id,
                                                          'match_percentage': HelperMethods.decimal_to_percentage_helper(
                                                              record['result']),
                                                          'match_description': 'Check 3',
                                                          'user_login': login,
                                                          })


        ####### check4
        if len(record_ids) == 0:
            if self.current_line_id:
                self.current_line_id.unlink()
            return self._action_next_screen()


        matched_records = self.dst_partner_id._get_similarity_result_fuzzy_percent(check4_field, self.min_id.id, \
                                                                                   check4_percent, \
                                                                                   active_ids=record_ids, \
                                                                                   from_wizard=True)
        for record in matched_records:
            if record['id'] in record_dict.keys():
                secondary_id = record['id']
                merged_list.append(record['id'])
                record_ids.remove(record['id'])
                record = self.env['remove.match'].create({'primary_contact': self.min_id.id,
                                                          'secondary_contact': secondary_id,
                                                          'match_percentage': HelperMethods.decimal_to_percentage_helper(record['result']),
                                                          'match_description': 'Check 4',
                                                          'user_login': login,
                                                          })

        if len(record_ids) > 0:
            for record in record_ids:
                res = self.env['remove.match'].create({'primary_contact': self.min_id.id,
                                                          'secondary_contact': record,
                                                          'match_description': 'N/A',
                                                          'user_login': login,
                                                          })
        if self.current_line_id:
            self.current_line_id.unlink()
        return self._action_next_screen()

    @api.multi
    def _merge_records_save(self, company_id, partner_ids):

        check1_field = company_id.check1_fields
        check1_percent = company_id.check1_proposed_merge

        check2_field = company_id.check2_fields
        check2_percent = company_id.check2_proposed_merge

        check3_field = company_id.check3_fields
        check3_percent = company_id.check3_proposed_merge

        check4_field = company_id.check4_fields
        check4_percent = company_id.check4_proposed_merge

        selected_merge_records = partner_ids

        login = self.env.user.name
        record_ids = []
        record_dict = {}
        for val in selected_merge_records:
            record_ids.append(val[0]['id'])
            record_dict[val[0]['id']] = val

        if self.dst_partner_id.id in record_ids:
            record_ids.remove(self.dst_partner_id.id)

        record_ids = self.save_merge_list(check1_field, check1_percent, login, record_dict, record_ids,\
                                          check_type='Check1')# Check1

        #CHeck2
        if len(record_ids) == 0:
            return

        record_ids = self.save_merge_list(check2_field, check2_percent, login, record_dict, record_ids,
                                          check_type='Check2')  # Check2

        ######### CHECK 3
        if len(record_ids) == 0:
            return
        record_ids = self.save_merge_list(check3_field, check3_percent, login, record_dict, record_ids,
                                          check_type='Check3')  # Check3

        ####### check4
        if len(record_ids) == 0:
            return

        record_ids = self.save_merge_list(check4_field, check4_percent, login, record_dict, record_ids,
                                          check_type='Check4')  # Check4

        if len(record_ids) > 0:
            for record in record_ids:
                name_rec = record_dict[record]
                res = self.env['merge.list'].create({'primary_contact_merge': self.dst_partner_id.id,
                                                        'primary_customer_code': self.dst_partner_id.code,
                                                        'secondary_contact': record,
                                                        'match_description_merge': 'N/A',
                                                        'secondary_customer_code': name_rec[0]['code'],
                                                        'secondary_contact_name': name_rec[0]['name'],
                                                        'user_login': login
                                                        })

            return

    def save_merge_list(self, check1_field, check1_percent, login, record_dict, record_ids, check_type):

        matched_records = self.dst_partner_id._get_similarity_result_fuzzy_percent(check1_field, self.dst_partner_id.id, \
                                                                                   check1_percent, \
                                                                                   active_ids=record_ids, \
                                                                                   from_wizard=True)
        for record in matched_records:
            if record['id'] in record_dict.keys():
                secondary_id = record['id']
                record_ids.remove(record['id'])

                name_rec = record_dict[record['id']]
                record = self.env['merge.list'].create({'primary_contact_merge': self.dst_partner_id.id,
                                                        'primary_customer_code': self.dst_partner_id.code,
                                                        'secondary_contact': secondary_id,
                                                        'match_percentage_merge': HelperMethods.decimal_to_percentage_helper(
                                                            record['result']),
                                                        'match_description_merge': check_type,
                                                        'secondary_customer_code': name_rec[0]['code'],
                                                        'secondary_contact_name': name_rec[0]['name'],
                                                        'user_login': login
                                                        })
        return record_ids

    @api.onchange('partner_ids')
    def onchange_order_line_proposed_wizard(self):
        for record in self:
            if len(record.partner_ids) == 2:
                record.show_remove = True

    @api.multi
    def action_relate(self):

        partner_ids = self.partner_ids
        dst_partner = self.dst_partner_id
        partner_id = partner_ids - dst_partner
        relation = self.type_relation_id
        if not relation.id:
            raise UserError(_("Please Select Relation Type"))

        if len(partner_id) > 1:
            for partner in partner_id:
                self.env['res.partner.relation.all'].create({'this_partner_id': dst_partner.id,
                                                             'other_partner_id': partner.id,
                                                             'type_selection_id': relation.id})
        else:
            self.env['res.partner.relation.all'].create({'this_partner_id': dst_partner.id,
                                                         'other_partner_id': partner_id.id,
                                                         'type_selection_id': relation.id})

        return self.action_remove()

    @api.multi
    def action_auto_merge(self):
        checked_ids = []
        _logger.info("Auto merge cron start ")
        _logger.info(datetime.datetime.now())

        company_id = self.env.user.company_id.sudo()
        check1_fields = company_id.check1_fields
        check1_percent = company_id.check1_auto_merge

        if not check1_fields:
            _logger.info("Please configure the fields for check1_fields ")
            return

        check2_fields = company_id.check2_fields
        check2_percent = company_id.check2_auto_merge

        if not check2_fields:
            _logger.info("Please configure the fields for check2_fields ")
            return

        check3_fields = company_id.check3_fields
        check3_percent = company_id.check3_auto_merge
        if not check3_fields:
            _logger.info("Please configure the fields for check3_fields ")
            return

        check4_fields = company_id.check4_fields
        check4_percent = company_id.check4_auto_merge
        if not check4_fields:
            _logger.info("Please configure the fields for check4_fields ")
            return

        ignore_matches = self._get_ignore_match()

        checked_ids = self._combine_match(check1_fields, check1_percent, checked_ids, ignore_matches)
        _logger.info("Records done in Check 1")
        _logger.info(checked_ids)
        checked_ids = self._combine_match(check2_fields, check2_percent, checked_ids, ignore_matches)
        _logger.info("Records done in Check 2")
        _logger.info(checked_ids)
        checked_ids = self._combine_match(check3_fields, check3_percent, checked_ids, ignore_matches)
        _logger.info("Records done in Check 3")
        _logger.info(checked_ids)
        checked_ids = self._combine_match(check4_fields, check4_percent, checked_ids, ignore_matches)
        _logger.info("Records done in Check 4")
        _logger.info(checked_ids)
        _logger.info("--- Cron Job END---")

    def _combine_match(self, check1_field, check1_percent, checked_ids, ignore_matches):
        special_values = dict()
        groups1 = self._compute_selected_groupby(check1_field)
        query1 = self._generate_query_auto_merge(groups1, self.maximum_group)
        self._cr.execute(query1)
        for min_id, aggr_ids in self._cr.fetchall():
            primary_record = request.env['res.partner'].search([('id', '=', min_id)], limit=1)

            matched_records = primary_record._get_similarity_result_fuzzy_percent(check1_field, primary_record.id, \
                                                                                  check1_percent)
            ids = []
            matched_record_dict = {}
            for rec in matched_records:
                if primary_record.id in ignore_matches.keys() and rec['id'] in ignore_matches[primary_record.id]:
                    pass
                else:
                    if rec['id'] not in ids and rec['id'] not in checked_ids:
                        ids.append(rec['id'])
                        checked_ids.append(rec['id'])
                        matched_record_dict[rec['id']] = rec['result']
            sorted_ids = sorted(ids)
            records = request.env['res.partner'].browse(sorted_ids)

            if len(sorted_ids)>2:
                oldest = sorted_ids[0]
            else:
                continue

            login = self.env.user.name
            dest_partner = request.env['res.partner'].browse(oldest)

            for record in records:
                if record.id != oldest:

                    merge_record = self.env['merge.list'].create({'primary_contact_merge': dest_partner.id,
                                                                  'primary_customer_code': dest_partner.code,
                                                                  'secondary_contact': record.id,
                                                                  'match_percentage_merge': HelperMethods.decimal_to_percentage_helper(
                                                                      matched_record_dict[record.id]),
                                                                  'match_description_merge': 'Check1',
                                                                  'secondary_customer_code': record.code,
                                                                  'secondary_contact_name': record.name,
                                                                  'user_login': login,
                                                                  'auto_merge': "Yes"
                                                                  })

                    try:
                        street = record.street
                        street2 = record.street2
                        city = record.city
                        state_id = record.state_id.id
                        zip = record.zip
                        country_id = record.country_id.id
                        phone = record.phone
                        email = record.email
                        add_phone = record.x_studio_field_zbWQS
                        add_email = record.x_studio_field_Vvzww
                        mobile = record.mobile

                        self.env['res.partner'].main_merge(city, country_id, email, record, dest_partner, phone,
                                                           state_id, street, \
                                                           street2, add_phone, add_email, zip, mobile)

                        self.env['merged.partner'].merge_partner(dest_partner, record)
                        values = self.env['res.partner'].personal_info_merge(record, dest_partner)
                        special_values.update(values)

                        values = self.env['res.partner'].communication_data_merge(record, dest_partner)
                        special_values.update(values)

                    except Exception as e:
                        print(e)
                        print(record.id)
                        print(' There is an exception ')
                        pass
            dest_partner.write(special_values)
            self._merge(sorted_ids, dest_partner, auto_merge=True)
        return checked_ids
