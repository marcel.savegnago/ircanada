# -*- coding: utf-8 -*-
{
    'name': "custom_crm_duplicates",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # for the full list
    'category': 'Uncategorized',
    'version': '0.4',

    # any module necessary for this one to work correctly
    'depends': ['base', "crm", "crm_duplicates","partner_multi_relation"],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/res_config_view.xml',
        'views/templates.xml',
        'views/duplicates_views.xml',
        'wizard/deduplicate_contact.xml',
        'views/merged_partner.xml',
        'views/data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}