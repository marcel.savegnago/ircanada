# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime

class RemoveMatch(models.Model):
    _name = 'remove.match'

    primary_contact = fields.Many2one('res.partner', string="Primary Contact")
    secondary_contact = fields.Many2one('res.partner', string="Secondary Contact")
    primary_customer_code = fields.Char(string='Primary Customer Code', store=True, compute='_compute_primary_customer_code')
    secondary_customer_code = fields.Char(string='Secondary Customer Code', store=True, compute='_compute_secondary_customer_code')
    match_percentage = fields.Float(string="Match Percentage")
    match_description = fields.Char(string="Description")
    date_remove = fields.Date(default=datetime.today())
    user = fields.Many2one('res.users', string='User id')
    user_login = fields.Char(string="User")

    @api.one
    @api.depends('primary_contact')
    def _compute_primary_customer_code(self):
        self.primary_customer_code = self.primary_contact.code

    @api.one
    @api.depends('secondary_contact')
    def _compute_secondary_customer_code(self):
        self.secondary_customer_code = self.secondary_contact.code