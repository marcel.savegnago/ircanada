# -*- coding: utf-8 -*-

from odoo import _, models, exceptions, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.osv.expression import OR
from odoo.http import request


ADDRESS_FIELDS_CHAR = ['street', 'street2', 'city', 'zip']
ADDRESS_FIELDS_MANY2ONE = ['state_id', 'country_id']
ADDRESS_FIELDS = ADDRESS_FIELDS_CHAR + ADDRESS_FIELDS_MANY2ONE

class res_partner(models.Model):
    _inherit = "res.partner"

    # combine_address = fields.Char(compute='_compute_address', store=True, string='Address')
    fuzzy_percent = fields.Float(compute='_fuzzy_percentage', string='Fuzzy Match', digits=(6,2))
    fuzzy_percent1 = fields.Float(string='Fuzzy Match1', digits=(6, 2))
    compute_fuzzy_percent = fields.Float(string='Fuzzy Match', digits=(6, 2), compute='_compute_fuzzy_percentage')
    match_description = fields.Char(string="Description")
    match_description_display = fields.Char(string="Check", compute='_compute_fuzzy_percentage')

    # @api.one
    # @api.depends('street', 'street2', 'city', 'state_id', 'zip', 'country_id')
    # def _compute_address(self):
    #     self.combine_address = (self.street or '') + ',' + (self.street2 or '') + ',' + (self.city or '') + ',' + \
    #                            (self.state_id.name or '') + ',' + (self.zip or '') + ',' + (self.country_id.name or '')

    @api.multi
    def name_get(self, context=None):
        res = []
        if self._context.get('flag') == 1:
            for partner in self:
                name = ''
                if partner.code:
                    name += partner.code
                name += ' | ' + partner.name
                if partner.mobile:
                    name += ' | ' + partner.mobile
                elif partner.phone:
                    name += ' | ' + partner.phone
                if partner.email:
                    name += ' | ' + partner.email
                if partner.comment:
                    name += ' | ' + partner.comment
                res.append((partner.id, name))
            return res
        else:
            return super(res_partner, self).name_get()


    def _set_description(self):
        pass

    @api.multi
    def _compute_duplicates_count(self):
        """
        Compute method for duplicates_count
        """
        for record in self:
            record.duplicates_count = 0  # To hide

    @api.multi
    def _compute_proposed_count(self):
        """
                Compute method for proposed_count

                Methods:
                 * _construct_domain
                 * search_count
                """

        # company_id = self.env.user.company_id.sudo()
        # only_companies = company_id.search_duplicates_for_companies_only
        for record in self:
            # if only_companies and record.parent_id:
            #     continue
            # else:
            #     check_one_record = record._proposed_merge_records()
            record.proposed_count = 0  #len(check_one_record)

    @api.multi
    def open_proposed_merge(self):

        self.ensure_one()
        view_list = self._proposed_merge_records()
        view_list.append(self.id)
        return {
            'name': 'Proposed Merge',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'res_model': 'res.partner',
            'domain': [('id', 'in', view_list)],
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
        }

    @api.model
    def search_duplicates_count(self, operator, value):
        """
        Search method for duplicates_count
        Introduced since the field is not stored
        """
        partners = self.search([])
        potential_dupplicates = []
        for partner in partners:
            if partner.duplicates_count > 0:
                potential_dupplicates.append(partner.id)
        return [('id', 'in', potential_dupplicates)]

    duplicates_count = fields.Integer(
        string='Duplicates Count',
        compute=_compute_duplicates_count,
        search='search_duplicates_count',
    )
    proposed_count = fields.Integer(
        string='Proposed Count',
        compute=_compute_proposed_count,
        search='search_duplicates_count',
    )


    @api.model
    def create(self, values):
        """
        Overwrite to force 'write' in 'create'
        """
        partner_id = super(res_partner, self).create(values)


        # Auto merge is not required on create
        # match_records = partner_id._custom_check1_duplicate()
        # if not match_records['proposed_merge_records'] and not match_records['auto_merge_records']:
        #     match_records = self._custom_check2_duplicate()
        # if not match_records['proposed_merge_records'] and not match_records['auto_merge_records']:
        #     match_records = self._custom_check3_duplicate()
        # if not match_records['proposed_merge_records'] and not match_records['auto_merge_records']:
        #     match_records = self._custom_check4_duplicate()
        #
        # if len(match_records['auto_merge_records']) > 0:
        #     parent_id = partner_id._auto_merge(match_records['auto_merge_records'])
        #     print("parent_id ---")
        #     print(parent_id)
        #     self.main_merge(city, country_id, email, parent_id, partner_id, phone, state_id, street1, street2,\
        #                     add_phone, add_email, zip)
        #
        return partner_id

    def personal_info_merge(self, source, destination):
        values = dict()
        values['x_studio_field_UsVrp'] = source['x_studio_field_UsVrp']
        dest_city = destination.city
        dest_street = destination.street
        if source['title']:
            values['title'] = source['title'].id
        values['street'] = source['street']
        values['street2'] = source['street2']
        values['city'] = source['city']
        values['state_id'] = source['state_id'].id
        values['zip'] = source['zip']
        values['country_id'] = source['country_id'].id
        address_vals = {}
        empty_vals = 0
        for key in ADDRESS_FIELDS:
            val = values.get(key, False)
            address_vals.update({key: val})
            if not val:
                empty_vals += 1

        if len(address_vals) == empty_vals:
            return {}
        else:
            rec = request.env['cx.address'].search([('partner_id', '=', destination.id),\
                                                    ('city', '=', values['city']),('street', '=', values['street'])])
            result = rec.unlink()
            rec = request.env['cx.address'].search([('partner_id', '=', destination.id),('type', '=', 'a'),\
                                                    ('city', '=', dest_city),('street', '=', dest_street)])
            result = rec.unlink()

            return values
        # destination.write(values)

    def communication_data_merge(self, source, destination):
        #
        values = dict()
        values['email'] = source['email']
        values['phone'] = source['phone']
        rec = request.env['prt.phone'].search([('partner_id', '=', destination.id), ('type', '=', '0')])
        result = rec.write({'type': '6'})
        # self._cr.commit()
        rec = request.env['prt.phone'].search([('partner_id', '=', destination.id),('number', '=', values['phone'])])
        result = rec.unlink()
        # old_main_phone = rec.number
        # rec.write({'number': source['phone']})
        if values['phone']:
            vals = dict()
            vals['partner_id'] = destination.id
            vals['type'] = '0'
            vals['number'] = values['phone']
            result = request.env['prt.phone'].create(vals)
        # self._cr.commit()
        #
        # rec = request.env['prt.phone'].search([('partner_id', '=', destination.id),('type', '=', 1)])
        # old_main_mobile = rec.number
        # if source['mobile']:
        #     rec.write({'number': source['mobile']})
        # if old_main_mobile:
        #     vals = dict()
        #     vals['partner_id'] = destination.id
        #     vals['type'] = '6'
        #     vals['number'] = old_main_mobile
        #     request.env['prt.phone'].create(vals)
        # rec = request.env['prt.phone'].search([('partner_id', '=', destination.id), ('number', '=', values['email'])])
        # result = rec.unlink()
        rec = request.env['prt.phone'].search([('partner_id', '=', destination.id), ('type', '=', '7')])
        # result = rec.unlink()
        rec.write({'type': '9'})
        rec = request.env['prt.phone'].search([('partner_id', '=', destination.id), ('number', '=', values['email'])])
        result = rec.unlink()
        #For Email
        if values['email']:
            vals = dict()
            vals['partner_id'] = destination.id
            vals['type'] = '7'
            vals['number'] = values['email']
            result = request.env['prt.phone'].create(vals)

        return {}

    def donor_attribute_merge(self, source, destination):
        values = dict()
        values['x_studio_field_dLazD'] = source['x_studio_field_dLazD']
        values['x_studio_field_83P92'] = source['x_studio_field_83P92']
        values['x_studio_field_x8j0L'] = source['x_studio_field_x8j0L']
        values['x_studio_field_3YKjQ'] = source['x_studio_field_3YKjQ']
        values['x_studio_field_MQaLQ'] = source['x_studio_field_MQaLQ']
        return values

    def main_merge(self, city, country_id, email, parent_id, partner_id, phone, state_id, street1, street2, add_phone,
                   add_email, zip, mobile):
        val_address = {}
        val_address['street'] = street1
        val_address['street2'] = street2
        val_address['city'] = city
        val_address['state_id'] = state_id
        val_address['zip'] = zip
        val_address['country_id'] = country_id
        val_address['partner_id'] = partner_id.id
        val_address['type'] = 'd'

        address_vals = {}
        empty_vals = 0
        for key in ADDRESS_FIELDS:
            val = val_address.get(key, False)
            address_vals.update({key: val})
            if not val:
                empty_vals += 1

        if len(address_vals) == empty_vals:
            pass
            # raise ValidationError(_("Please fill at list one address field to continue "
            #                         "or remove the address from address list to delete in permanently!"))
        else:
            request.env['cx.address'].create(val_address)
        domain = [
            ('res_id', '=', partner_id.id),
            ('res_model', '=', 'res.partner'),
        ]
        attachment = self.env['ir.attachment'].search(domain)
        if attachment:
            attachment.write({'red_id': partner_id.id})
        if phone:
            vals = {}
            if partner_id.id != parent_id.id:
                vals['partner_id'] = partner_id.id
                vals['type'] = '6'
                vals['number'] = phone
                request.env['prt.phone'].create(vals)
            else:
                vals['partner_id'] = partner_id.id
                vals['type'] = '10'
                vals['number'] = phone
                request.env['prt.phone'].create(vals)
        if email:
            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '9'
            vals['number'] = email
            request.env['prt.phone'].create(vals)
        if add_phone:  # Additional Phone number
            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '6'
            vals['number'] = add_phone
            request.env['prt.phone'].create(vals)
        # if mobile:  # Mobile Phone number
        #     vals = {}
        #     if partner_id.id != parent_id.id:
        #         vals['partner_id'] = partner_id.id
        #         vals['type'] = '6'
        #         vals['number'] = mobile
        #         request.env['prt.phone'].create(vals)
        #     else:
        #         vals['partner_id'] = partner_id.id
        #         vals['type'] = '10'
        #         vals['number'] = mobile
        #         request.env['prt.phone'].create(vals)

        if add_email:  # Additional Email

            vals = {}
            vals['partner_id'] = partner_id.id
            vals['type'] = '9'
            vals['number'] = add_email
            request.env['prt.phone'].create(vals)

    @api.multi
    def write(self, vals):
        for record in self:
            partner_id = super(res_partner, record).write(vals)
            duplicate_partners = record._return_duplicated_records(fields_names="check1_fields")
            if len(duplicate_partners) == 0:
                continue
            # else:
                # duplicate_partners_recordset = self.browse(duplicate_partners)
                # warning = _('Duplicates were found: \n')
                # for duplicate in duplicate_partners_recordset:
                #     fields = self.env.user.company_id.sudo().duplicate_fields_partner
                #     duplicated_fields = "; ".join(["{} - {}".format(field.name, record[field.name])
                #                                    for field in fields if record[field.name]
                #                                    and record[field.name] == duplicate[field.name]])
                #     warning += '"[ID {}] {} {} {} \n'.format(
                #         duplicate.id,
                #         duplicate.name,
                #         _(' by fields: '),
                #         duplicated_fields
                #     )
                # raise exceptions.UserError(warning)
        return True

    @api.multi
    def _construct_domain(self, fields, char_operator = '=', extra_domain=[]):
        self.ensure_one()
        domain = False
        fields_domain = []
        for field in fields:
            if self[field.name]:
                if field.ttype == 'many2one':
                    fields_domain = OR([fields_domain, [(field.name, 'in', self[field.name].ids)]])
                elif field.ttype == 'char':
                    fields_domain = OR([fields_domain, [(field.name, char_operator, self[field.name])]])
                else:
                    fields_domain = OR([fields_domain, [(field.name, '=', self[field.name])]])
        if fields_domain:
            domain = fields_domain + extra_domain + [('id', '!=', self.id)] \
                     + ["|", ("company_id", "=", False), ('company_id', '=', self.company_id.id)]
        return domain

    @api.multi
    def _return_duplicated_records(self, fields_names, char_operator="="):
        self.ensure_one()
        company_id = self.env.user.company_id.sudo()
        only_companies = company_id.search_duplicates_for_companies_only
        records = []
        if not (only_companies and self.parent_id):
            extra_domain = only_companies and [('parent_id', '=', False)] or []
            fields = company_id[fields_names]
            domain = self._construct_domain(fields=fields, char_operator=char_operator, extra_domain=extra_domain)
            if domain:
                records = self._search(domain)
        return records

    @api.multi
    def open_duplicates(self):
        """
        The method to open tree of potential duplicates

        Methods:
         * _return_duplicated_records

        Extra info:
         * Expected singleton

        Returns:
         * action to open partners duplicates list
        """
        self.ensure_one()
        duplicates = self._return_duplicated_records(
            fields_names="duplicate_fields_partner_soft",
            char_operator="ilike"
        )
        return {
            'name': 'Duplicates',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'res_model': 'res.partner',
            'domain': [('id', 'in', duplicates + self.ids)],
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
        }

    @api.multi
    def _custom_check1_duplicate(self, hard=True):
        company_id = self.env.user.company_id.sudo()
        check1_field = company_id.check1_fields
        check1_automerge = company_id.check1_auto_merge
        check1_proposedmerge = company_id.check1_proposed_merge
        check1_percent = check1_proposedmerge if check1_proposedmerge < check1_automerge else check1_automerge
        check1_criteria = company_id.check1_type_of_duplicates
        auto_merge_records = []
        proposed_merge_records = []
        matched_records_check1 = []
        if check1_field:
            matched_records_check1 = self._get_similarity_result(check1_field, self.id, check1_percent)
        for values in matched_records_check1:
            if HelperMethods.decimal_to_percentage_helper(values['result']) >= check1_automerge:
                auto_merge_records.append(values['id'])
            elif HelperMethods.decimal_to_percentage_helper(values['result']) >= check1_proposedmerge:
                proposed_merge_records.append(values['id'])
                
        if (auto_merge_records or proposed_merge_records) and check1_criteria == 'hard' and hard:
            raise UserError(_("Contact duplicate."))
        elif (auto_merge_records or proposed_merge_records) and check1_criteria == 'soft':
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records':auto_merge_records,'check1_criteria': check1_criteria}
        elif not auto_merge_records and not proposed_merge_records:
            return {'proposed_merge_records': [], 'auto_merge_records': [], 'check1_criteria': check1_criteria}
        else:
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records':auto_merge_records,'check1_criteria': check1_criteria}




    @api.multi
    def _custom_check2_duplicate(self, hard=True):
        company_id = self.env.user.company_id.sudo()
        check2_field = company_id.check2_fields
        check2_automerge = company_id.check2_auto_merge
        check2_proposedmerge = company_id.check2_proposed_merge
        check2_percent = check2_proposedmerge if check2_proposedmerge < check2_automerge else check2_automerge
        check2_criteria = company_id.check2_type_of_duplicates
        auto_merge_records = []
        proposed_merge_records = []
        matched_records_check1 = []
        if check2_field:
            matched_records_check1 = self._get_similarity_result(check2_field, self.id, check2_percent)
        for values in matched_records_check1:
            if HelperMethods.decimal_to_percentage_helper(values['result']) >= check2_automerge:
                auto_merge_records.append(values['id'])
            elif HelperMethods.decimal_to_percentage_helper(values['result']) >= check2_proposedmerge:
                proposed_merge_records.append(values['id'])

        if (auto_merge_records or proposed_merge_records) and check2_criteria == 'hard' and hard:
            raise UserError(_("Contact duplicate."))
        elif (auto_merge_records or proposed_merge_records) and check2_criteria == 'soft':
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records,'check2_criteria': check2_criteria}
        elif not auto_merge_records and not proposed_merge_records:
            return {'proposed_merge_records': [], 'auto_merge_records': [],'check2_criteria': check2_criteria}
        else:
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records,
                    'check1_criteria': check2_criteria}

    @api.multi
    def _custom_check3_duplicate(self, hard=True):
        company_id = self.env.user.company_id.sudo()
        check3_field = company_id.check3_fields
        check3_automerge = company_id.check3_auto_merge
        check3_proposedmerge = company_id.check3_proposed_merge
        check3_percent = check3_proposedmerge if check3_proposedmerge < check3_automerge else check3_automerge
        check3_criteria = company_id.check3_type_of_duplicates
        auto_merge_records = []
        proposed_merge_records = []
        matched_records_check1 = []
        if check3_field:
            matched_records_check1 = self._get_similarity_result(check3_field, self.id, check3_percent)
        for values in matched_records_check1:
            if HelperMethods.decimal_to_percentage_helper(values['result']) >= check3_automerge:
                auto_merge_records.append(values['id'])
            elif HelperMethods.decimal_to_percentage_helper(values['result']) >= check3_proposedmerge:
                proposed_merge_records.append(values['id'])
        if (auto_merge_records or proposed_merge_records) and check3_criteria == 'hard':
            raise UserError(_("Contact duplicate."))
        elif (auto_merge_records or proposed_merge_records) and check3_criteria == 'soft':
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records, 'check3_criteria': check3_criteria}
        elif not auto_merge_records and not proposed_merge_records:
            return {'proposed_merge_records': [], 'auto_merge_records': [], 'check3_criteria': check3_criteria}
        else:
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records,
                    'check3_criteria': check3_criteria}

    @api.multi
    def _custom_check4_duplicate(self, hard=True):
        company_id = self.env.user.company_id.sudo()
        check4_field = company_id.check4_fields
        check4_automerge = company_id.check4_auto_merge
        check4_proposedmerge = company_id.check4_proposed_merge
        check4_percent = check4_proposedmerge if check4_proposedmerge < check4_automerge else check4_automerge
        check4_criteria = company_id.check4_type_of_duplicates
        auto_merge_records = []
        proposed_merge_records = []
        matched_records_check4 = []
        if check4_field:
            matched_records_check4 = self._get_similarity_result(check4_field, self.id, check4_percent)
        for values in matched_records_check4:
            if HelperMethods.decimal_to_percentage_helper(values['result']) >= check4_automerge:
                auto_merge_records.append(values['id'])
            elif HelperMethods.decimal_to_percentage_helper(values['result']) >= check4_proposedmerge:
                proposed_merge_records.append(values['id'])
        if (auto_merge_records or proposed_merge_records) and check4_criteria == 'hard':
            raise UserError(_("Contact duplicate."))
        elif (auto_merge_records or proposed_merge_records) and check4_criteria == 'soft':
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records,
                    'check4_criteria': check4_criteria}
        elif not auto_merge_records and not proposed_merge_records:
            return {'proposed_merge_records': [], 'auto_merge_records': [], 'check4_criteria': check4_criteria}
        else:
            return {'proposed_merge_records': proposed_merge_records, 'auto_merge_records': auto_merge_records,
                    'check4_criteria': check4_criteria}


    @api.multi
    def _get_similarity_result(self, check_fields, record_id, check_percent, from_wizard=False):
        field_param = []
        field_names = "" + "\""
        match_string = ''
        for field in check_fields:
            field_param.append(field.name)
            if self[field.name]:
                field_names = field_names + field.name + ','
                match_string = match_string + self[field.name]
        field_names = field_names[:-1]  # Remove last comma
        match_string = match_string.replace("'", "''")
        field_names = field_names + "\""
        field_names = field_names.replace(",", "\",\"")
        if len(field_names) < 2:
            return []

        query_string = 'similarity(CONCAT( ' + field_names + '), \' ' + match_string + ' \' )'

        # TODO use ORM
        if from_wizard == False:
            self.env.cr.execute('select '+query_string+' as result, '
                                'id from res_partner where id != %s and active is True and '+query_string+' >= '+str(check_percent/100)+' order by result DESC ',
                                (record_id,))
        else:

            self.env.cr.execute('select '+query_string+' as result, '
                                'id from res_partner where active is True and '+query_string+' >= '+str(check_percent/100)+' order by result DESC ')
        matched_records = self.env.cr.dictfetchall()
        return matched_records

    def _get_similarity_result_fuzzy_percent(self, check_fields, record_id, check_percent, active_ids=None,\
                                             from_wizard=False):
        field_param = []
        field_names = "" + "\""
        match_string = ''

        primary_record = request.env['res.partner'].search([('id', '=', record_id)],order='create_date asc', limit=1)

        for field in check_fields:
            field_param.append(field.name)
            # if self[field.name]:
            field_names = field_names + field.name + ','
            if primary_record[field.name]:
                match_string = match_string + primary_record[field.name]
        field_names = field_names[:-1]  # Remove last comma
        if len(field_names) == 0:
            return []

        ids = ''
        if active_ids:
            for active_id in active_ids:
               ids = ids + str(active_id) + ','
            ids = ids[:-1]  # Remove last comma

        match_string = match_string.replace("'", "''")
        field_names = field_names + "\""
        field_names = field_names.replace(",", "\",\"")
        select_query_string = 'id, similarity(CONCAT( ' + field_names + '), \' ' + match_string + ' \' ) as result'
        where_query_string = 'code !=\'\' AND active is True AND  similarity(CONCAT( ' + field_names + '), \' ' + match_string + ' \' ) >= '+str(check_percent/100)

        print(where_query_string)
        # TODO use ORM

        if from_wizard is False:
            print('SELECT '+select_query_string+' from res_partner WHERE '+where_query_string+'  order by result DESC ')
            self.env.cr.execute('SELECT '+select_query_string+' from res_partner WHERE '+where_query_string+'  order by result DESC ')
        else:
            print('SELECT  '+select_query_string+' from res_partner WHERE id in ('+ids+') and '+where_query_string+'  order by result DESC ')
            self.env.cr.execute('SELECT  '+select_query_string+' from res_partner WHERE id in ('+ids+') and '+where_query_string+'  order by result DESC '),
        matched_records = self.env.cr.dictfetchall()
        return matched_records

    @api.multi
    def _auto_merge(self, match_records):
        uid = self._uid
        uid_login = request.env['res.users'].search_read([('id', '=', uid)])
        login = uid_login[0]['name']
        primary_record = request.env['res.partner'].search([('id', 'in', match_records)],order='create_date asc',limit=1)
        company_id = self.env.user.company_id.sudo()
        check1_field = company_id.check1_fields
        check1_percent = company_id.check1_proposed_merge \
            if company_id.check1_proposed_merge < company_id.check1_auto_merge \
            else company_id.check1_auto_merge

        match_results = self._get_similarity_result(check1_field, self.id, check1_percent)
        if match_records:
            for id in match_results:
                if id['id'] == primary_record.id:
                    name_rec = self.env['res.partner'].search_read([('id', '=', self.id)])
                    record_create = self.env['merge.list'].create({'primary_contact_merge': primary_record.id,
                                                       'primary_customer_code': primary_record.code,
                                                       'secondary_contact': self.id,
                                                       'match_percentage_merge': HelperMethods.decimal_to_percentage_helper(id['result']),
                                                       'match_description_merge': 'Check1',
                                                       'secondary_customer_code': name_rec[0]['code'],
                                                       'secondary_contact_name': name_rec[0]['name'],
                                                       'user_login': login,
                                                        })
        return primary_record

    @api.multi
    def _proposed_merge_records(self):
        comb1 = []
        comb2 = []

        match_records = self._custom_check1_duplicate(hard=False)
        if not match_records['proposed_merge_records']:
            match_records = self._custom_check2_duplicate(hard=False)
        if not match_records['proposed_merge_records']:
            match_records = self._custom_check3_duplicate(hard=False)
        if not match_records['proposed_merge_records']:
            match_records = self._custom_check4_duplicate(hard=False)

        proposed_recs = match_records['proposed_merge_records']
        match_list = proposed_recs.copy()
        match_list.append(self.id)
        not_view_comb1 = request.env['remove.match'].search_read([('primary_contact', 'in', match_list)])
        for ids in not_view_comb1:
            comb1.append(ids['secondary_contact'][0])
        not_view_comb2 = request.env['remove.match'].search_read([('secondary_contact', 'in', match_list)])
        for ids in not_view_comb2:
            comb2.append(ids['primary_contact'][0])
        comb1.extend(comb2)
        view_list = [item for item in proposed_recs if item not in comb1]
        return view_list

    @api.multi
    def _fuzzy_percentage(self):
        for record in self:
            check_failed = True
            active_ids = list(record.env['custom.partner.merge.automatic.wizard']._get_ordered_partner(self.ids)._ids)
            company_id = record.env.user.company_id.sudo()
            dst_partner_id = record.env['custom.partner.merge.automatic.wizard']._get_ordered_partner(active_ids)[0].id
            active_ids = [record.id]
            check1_fields = company_id.check1_fields
            check1_percent = company_id.check1_proposed_merge \
                if company_id.check1_proposed_merge < company_id.check1_auto_merge \
                else company_id.check1_auto_merge
            check2_fields = company_id.check2_fields
            check2_percent = company_id.check2_proposed_merge \
                if company_id.check2_proposed_merge < company_id.check2_auto_merge \
                else company_id.check2_auto_merge
            check3_fields = company_id.check3_fields
            check3_percent = company_id.check3_proposed_merge \
                if company_id.check3_proposed_merge < company_id.check3_auto_merge \
                else company_id.check3_auto_merge
            check4_fields = company_id.check4_fields
            check4_percent = company_id.check4_proposed_merge \
                if company_id.check4_proposed_merge < company_id.check4_auto_merge \
                else company_id.check4_auto_merge

            # we have to get the percentage for only for selected record. the query is fetching all the activeIDs for the percentage.
            matched_records = record._get_similarity_result_fuzzy_percent(check1_fields, dst_partner_id, check1_percent,\
                                                                          active_ids, True)

            for val in matched_records:
                if val['id'] in active_ids and val['id'] == record.id:
                    record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(val['result'])
                    record.match_description = "Check 1"
                    check_failed = False

            if check_failed and check2_fields:
                matched_records = record._get_similarity_result_fuzzy_percent(check2_fields, dst_partner_id, check2_percent,\
                                                                              active_ids, True)
                for val in matched_records:
                    if val['id'] in active_ids and val['id'] == record.id:
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(val['result'])
                        record.match_description = "Check 2"
                        check_failed = False

            if check_failed and check3_fields:
                matched_records = record._get_similarity_result_fuzzy_percent(check3_fields, dst_partner_id, check3_percent,\
                                                                              active_ids, True)
                for val in matched_records:
                    if val['id'] in active_ids and val['id'] == record.id:
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(val['result'])
                        record.match_description = "Check 3"
                        check_failed = False
            if check_failed and check4_fields:
                matched_records = record._get_similarity_result_fuzzy_percent(check4_fields, dst_partner_id, check4_percent,\
                                                                              active_ids, True)
                for val in matched_records:
                    if val['id'] in active_ids and val['id'] == record.id:
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(val['result'])
                        record.match_description = "Check 4"
                        check_failed = False

            if check_failed:
                record.fuzzy_percent = float(0)
                record.match_description = "N/A"

            # matched_records = record._get_similarity_result_fuzzy_percent(check1_fields, dst_partner_id, active_ids, True)
            # for val in matched_records:
            #     if val['id'] in active_ids and val['id'] == record.id:
            #         record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(val['result'])

    @api.model
    def _generate_query(self, fields, partner, maximum_group=100):
        """ Build the SQL query on res.partner table to group them according to given criteria
            :param fields : list of column names to group by the partners
            :param maximum_group : limit of the query
        """
        # make the list of column to group by in sql query
        sql_fields = []
        for field in fields:
            if field in ['email', 'name']:
                sql_fields.append('lower(%s)' % field)
            elif field in ['vat']:
                sql_fields.append("replace(%s, ' ', '')" % field)
            else:
                sql_fields.append(field)
        group_fields = ', '.join(sql_fields)

        # where clause : for given group by columns, only keep the 'not null' record
        filters = []
        for field in fields:
            if field in ['email', 'name', 'vat']:
                filters.append((field, 'IS NOT', 'NULL'))
        criteria = ' AND '.join('%s %s %s' % (field, operator, value) for field, operator, value in filters)

        # build the query
        text = [
            "SELECT min(id), array_agg(id)",
            "FROM res_partner",
        ]

        if criteria:
            text.append('WHERE %s' % criteria)
        text.extend([
            "GROUP BY %s" % group_fields,
            "HAVING COUNT(*) >= 2",
            "ORDER BY min(id)",
        ])

        if maximum_group:
            text.append("LIMIT %s" % maximum_group, )

        return ' '.join(text)

    @api.multi
    def _compute_fuzzy_percentage(self):
        listed_ids = []
        company_id = self.env.user.company_id.sudo()

        # maximum_records_per_match = company_id.maximum_records_per_match
        check1_fields = company_id.check1_fields
        check1_percent = company_id.check1_proposed_merge

        check2_fields = company_id.check2_fields
        check2_percent = company_id.check2_proposed_merge
        check3_fields = company_id.check3_fields
        check3_percent = company_id.check3_proposed_merge
        check4_fields = company_id.check4_fields
        check4_percent = company_id.check4_proposed_merge

        # groups1 = self.env['custom.partner.merge.automatic.wizard']._compute_selected_groupby(check1_fields)
        # dest_part = request.env['res.partner'].search([('id', '=', self.env.context.get('dest_partner'))], limit=1)




        # query1 = self.env['custom.partner.merge.automatic.wizard']._generate_query(groups1)
        # groups2 = self.env['custom.partner.merge.automatic.wizard']._compute_selected_groupby(check2_fields)
        # query2 = self.env['custom.partner.merge.automatic.wizard']._generate_query(groups2)
        #
        # groups3 = self.env['custom.partner.merge.automatic.wizard']._compute_selected_groupby(check3_fields)
        # query3 = self.env['custom.partner.merge.automatic.wizard']._generate_query(groups3)
        #
        # groups4 = self.env['custom.partner.merge.automatic.wizard']._compute_selected_groupby(check4_fields)
        # query4 = self.env['custom.partner.merge.automatic.wizard']._generate_query(groups4)

        # self._cr.execute(query1)
        counter = 0
        # for min_id, aggr_ids in self._cr.fetchall():
        #     if counter == maximum_records_per_match:
        #         break
        primary_record = request.env['res.partner'].search([('id', '=', self.env.context.get('primary_contact'))], limit=1)
        print(self.env.context.get('check'))

        if self.env.context.get('check') == 'check_bypass' or self.env.context.get('check') is None:
            bypass = True
        else:
            bypass = False

        for record in self:
            if self.env.context.get('check') == 'check1' or bypass:
                matched_records1 = primary_record._get_similarity_result_fuzzy_percent(check1_fields, primary_record.id,\
                                                                                      check1_percent, active_ids=[record.id],\
                                                                                      from_wizard = True)
                for matched_record in matched_records1:
                    if matched_record['id'] == record.id:
                        record.compute_fuzzy_percent = float(matched_record['result']) * 100
                        record.match_description_display = 'Check 1'
                        listed_ids.append(record.id)

            if self.env.context.get('check') == 'check2' or bypass:
                matched_records2 = primary_record._get_similarity_result_fuzzy_percent(check2_fields, primary_record.id,
                                                                              check2_percent,
                                                                              active_ids=[record.id],
                                                                              from_wizard=True)

                for matched_record in matched_records2:
                    if matched_record['id'] == record.id and record.id not in listed_ids:
                        listed_ids.append(record.id)
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(matched_record['result'])
                        record.match_description_display = 'Check 2'
                        record.compute_fuzzy_percent = float(matched_record['result']) * 100
            if self.env.context.get('check') == 'check3' or bypass:
                matched_records3 = primary_record._get_similarity_result_fuzzy_percent(check3_fields, primary_record.id,
                                                                              check3_percent,
                                                                              active_ids=[record.id],
                                                                              from_wizard=True)
                for matched_record in matched_records3:
                    if matched_record['id'] == record.id and record.id not in listed_ids:
                        listed_ids.append(record.id)
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(matched_record['result'])
                        record.match_description_display = 'Check 3'
                        record.compute_fuzzy_percent = float(matched_record['result']) * 100

            if self.env.context.get('check') == 'check4' or bypass:
                matched_records4 = primary_record._get_similarity_result_fuzzy_percent(check4_fields, primary_record.id,
                                                                              check4_percent,
                                                                              active_ids=[record.id],
                                                                              from_wizard=True)
                for matched_record in matched_records4:
                    if matched_record['id'] == record.id and record.id not in listed_ids:
                        listed_ids.append(record.id)
                        record.fuzzy_percent = HelperMethods.decimal_to_percentage_helper(matched_record['result'])
                        record.match_description_display = 'Check 4'
                        record.compute_fuzzy_percent = float(matched_record['result']) * 100

class HelperMethods:

    @staticmethod
    def decimal_to_percentage_helper(value):
        return value * 100
