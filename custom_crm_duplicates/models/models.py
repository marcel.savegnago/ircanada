# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class custom_crm_duplicates(models.Model):
#     _name = 'custom_crm_duplicates.custom_crm_duplicates'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class ResPartnerRelationAll(models.AbstractModel):
    _inherit = "res.partner.relation.all"
    """Abstract model to show each relation from two sides."""

    this_partner_donor_id = fields.Char(compute="compute_this_partner_donor_id")
    other_partner_donor_id = fields.Char(compute="compute_other_partner_donor_id")

    @api.depends('this_partner_id')
    def compute_this_partner_donor_id(self):
        for rec in self:
            rec.this_partner_donor_id = rec.this_partner_id.code

    @api.depends('other_partner_id')
    def compute_other_partner_donor_id(self):
        for rec in self:
            rec.other_partner_donor_id = rec.other_partner_id.code