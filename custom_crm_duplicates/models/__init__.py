# -*- coding: utf-8 -*-

from . import models
from . import res_company
from . import res_config_settings
from . import custom_partner
from . import match_megre
from . import merge_list
from . import prt_phone
from . import merge_partner