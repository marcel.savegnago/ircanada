# -*- coding: utf-8 -*-

from odoo import fields, models


class res_config_settings(models.TransientModel):
    _inherit = 'res.config.settings'

    company_dup_id = fields.Many2one(
        'res.company',
        string='Company Duplicates',
        default=lambda self: self.env.user.company_id,
        required=True,
    )

    check1_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table1', 'rel1x', 'rel1y',
        related='company_dup_id.check1_fields',
        compute_sudo=True,
        string='Check 1 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check1_auto_merge = fields.Float(
        related='company_dup_id.check1_auto_merge',
        compute_sudo=True,
        string='Check 1 % Auto Merge',
    )

    check1_proposed_merge = fields.Float(
        related='company_dup_id.check1_proposed_merge',
        compute_sudo=True,
        string='Check 1 % Proposed Merge',
    )

    check1_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        related='company_dup_id.check1_type_of_duplicates',
        compute_sudo=True,
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check2_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table2', 'rel1x', 'rel1y',
        related='company_dup_id.check2_fields',
        compute_sudo=True,
        string='Check 2 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check2_auto_merge = fields.Float(
        related='company_dup_id.check2_auto_merge',
        compute_sudo=True,
        string='Check 2 % Auto Merge',
    )

    check2_proposed_merge = fields.Float(
        related='company_dup_id.check2_proposed_merge',
        compute_sudo=True,
        string='Check 2 % Proposed Merge',
    )

    check2_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        related='company_dup_id.check2_type_of_duplicates',
        compute_sudo=True,
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check3_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table3', 'rel1x', 'rel1y',
        related='company_dup_id.check3_fields',
        compute_sudo=True,
        string='Check 3 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check3_auto_merge = fields.Float(
        related='company_dup_id.check3_auto_merge',
        compute_sudo=True,
        string='Check 3 % Auto Merge',
    )

    check3_proposed_merge = fields.Float(
        related='company_dup_id.check3_proposed_merge',
        compute_sudo=True,
        string='Check 3 % Proposed Merge',
    )

    check3_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        related='company_dup_id.check3_type_of_duplicates',
        compute_sudo=True,
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check4_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table4', 'rel1x', 'rel1y',
        related='company_dup_id.check4_fields',
        compute_sudo=True,
        string='Check 4 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check4_auto_merge = fields.Float(
        related='company_dup_id.check4_auto_merge',
        compute_sudo=True,
        string='Check 4 % Auto Merge',
    )

    check4_proposed_merge = fields.Float(
        related='company_dup_id.check4_proposed_merge',
        compute_sudo=True,
        string='Check 4 % Proposed Merge',
    )

    check4_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        related='company_dup_id.check4_type_of_duplicates',
        compute_sudo=True,
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    maximum_records_per_match = fields.Integer(
        related='company_dup_id.maximum_records_per_match',
        compute_sudo=True,
        string='Maximum records per match',
    )
