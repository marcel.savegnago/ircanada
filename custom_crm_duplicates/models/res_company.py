# -*- coding: utf-8 -*-

from odoo import fields, models


class res_company(models.Model):
    _inherit = 'res.company'

    check1_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table1', 'rel1x', 'rel1y',
        string='Check 1 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check1_auto_merge = fields.Float(
        string='Check 1 % Auto Merge',
    )

    check1_proposed_merge = fields.Float(
        string='Check 1 % Proposed Merge',
    )

    check1_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check2_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table2', 'rel1x', 'rel1y',
        compute_sudo=True,
        string='Check 2 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check2_auto_merge = fields.Float(
        string='Check 2 % Auto Merge',
    )

    check2_proposed_merge = fields.Float(
        string='Check 2 % Proposed Merge',
    )

    check2_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check3_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table3', 'rel1x', 'rel1y',
        compute_sudo=True,
        string='Check 3 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check3_auto_merge = fields.Float(
        string='Check 3 % Auto Merge',
    )

    check3_proposed_merge = fields.Float(
        string='Check 3 % Proposed Merge',
    )

    check3_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )

    check4_fields = fields.Many2many(
        'ir.model.fields', 'custom_rel_table4', 'rel1x', 'rel1y',
        compute_sudo=True,
        string='Check 4 fields',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', 'not in', ['one2many', 'many2many', 'binary', 'reference', 'serialized']),
            ('store', '=', True),
        ],
    )

    check4_auto_merge = fields.Float(
        string='Check 4 % Auto Merge',
    )

    check4_proposed_merge = fields.Float(
        string='Check 4 % Proposed Merge',
    )

    check4_type_of_duplicates = fields.Selection(
        [('soft', 'Soft'), ('hard', 'Hard')],
        required=True,
        default='soft', string='Type of duplicate (Soft/Hard)',
    )


    maximum_records_per_match = fields.Integer(
        string='Maximum records per match',
    )
