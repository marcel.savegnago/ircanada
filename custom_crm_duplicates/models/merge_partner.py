# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime


class MergedPartner(models.Model):
    _name = 'merged.partner'

    destination_donor_id = fields.Many2one('res.partner', string="Destination Donor")
    merged_donor_id = fields.Integer(string="Merged Contact")
    merged_donor_name = fields.Char(string='Merged Donor Name')
    donor_type = fields.Char(string='Donor Type')
    address_fields = fields.Char(string="Address Fields")
    phone_number = fields.Char(string="Phone Number")
    email = fields.Char(string="Email")
    request_tax_receipt = fields.Char(string="Request Tax Receipt")
    mailed_tax_receipt = fields.Boolean(string='Mailed Tax Receipt')
    do_not_mail = fields.Boolean(string='Do not Mail')
    do_not_call = fields.Boolean(string='Do not Call')
    mailed_tax_receipt = fields.Boolean(string='Mailed Tax Receipt')
    deceased = fields.Boolean(string='Deceased')
    deceased_date = fields.Date(string="Deceased Date")
    raisers_edge_name = fields.Char(string="Raisers Edge Name")
    raisers_edge_id = fields.Char(string="Raisers Edge ID")
    constituent_import_id = fields.Char(string="Constituent Import ID")
    donor_since = fields.Date(string="Donor Since")
    merged_donor_id_compute = fields.Char(string="Merged Contact", compute='_save_as_char')

    @api.depends('merged_donor_id')
    def _save_as_char(self):
        for record in self:
            record.merged_donor_id_compute = record.merged_donor_id


    def merge_partner(self, destination_donor_id, merged_donor):
        if destination_donor_id.id != merged_donor.id:
            self.create({'destination_donor_id': destination_donor_id.id,
                        'merged_donor_id': merged_donor.code,
                        'merged_donor_name': merged_donor.name,
                        'donor_type': merged_donor.x_studio_field_UsVrp,
                        'address_fields': merged_donor.x_studio_field_VfR7W,
                        'phone_number': merged_donor.phone,
                        'email': merged_donor.email,
                        'request_tax_receipt': merged_donor.x_studio_field_dLazD,
                        'mailed_tax_receipt':merged_donor.x_studio_field_83P92,
                        'deceased':merged_donor.x_studio_field_3YKjQ,
                        'deceased_date':merged_donor.x_studio_field_MQaLQ,
                        'raisers_edge_name':merged_donor.x_studio_field_lNtH9,
                        'raisers_edge_id':merged_donor.x_studio_field_uzzbN,
                        'constituent_import_id':merged_donor.x_studio_field_e81gZ,
                        'donor_since':merged_donor.x_studio_field_chJhB,
                        })
        pass
