# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime

class MergeList(models.Model):
    _name = 'merge.list'

    primary_contact_merge = fields.Many2one('res.partner', string="Primary Contact")
    secondary_contact = fields.Integer(string="Secondary Contact")
    primary_customer_code = fields.Char(string='Primary Customer Code')
    secondary_customer_code = fields.Char(string='Secondary Customer Code')
    match_percentage_merge = fields.Float(string="Match Percentage")
    match_description_merge = fields.Char(string="Description")
    date_merge = fields.Date(default=datetime.today())
    user_merge = fields.Many2one('res.users', string='User id')
    secondary_contact_name = fields.Char(string="Secondary Contact")
    user_login = fields.Char(string="User")
    auto_merge = fields.Char(string="Auto Merge", default="No")
