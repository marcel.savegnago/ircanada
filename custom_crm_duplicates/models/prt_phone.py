from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError

from odoo.addons.prt_phone_numbers.models import prt_phone



import re

import logging
# Logger for debug
_logger = logging.getLogger(__name__)


# -- Format number to store as searchable
def prep_num_new(number, num_type):
    if num_type in ['7', '8', '9']:
        return number.replace(" ", "")
    else:
        return re.sub("[^0-9]", "", number)

prt_phone.prep_num = prep_num_new
########################
# Phone/Email/Username #
########################
class PRTPhone(models.Model):
    _inherit = "prt.phone"

    type = fields.Selection([
            ('0', 'Main phone'),
            ('1', 'Mobile phone'),
            ('2', 'Work phone'),
            ('3', 'Extension phone'),
            ('4', 'Home phone'),
            ('5', 'Fax'),
            ('6', 'Other phone'),
            ('7', 'Email'),
            ('8', 'Username'),
            ('9', 'Other email'),
            ('10', 'Previous Main phone'),
        ], string="Type", required=True,
            help="Phone type for number or 'Username' for username containing letters (e.g. Skype name)")