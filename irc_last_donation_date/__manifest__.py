# Copyright 2019 Eska Technology LLC (www.eskaweb.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Last Donation Date',
    'summary': 'Adds last donation date',
    'version': '11.0.1.0.0',
    'category': 'Accounting',
    'author': 'Eska',
    'website': 'http://www.eskaweb.com',
    'license': 'LGPL-3',
    'depends': [
        'account',
    ],
    'data': [
        'views/res_partner_view.xml',
    ],
    'installable': True,
}
