# Copyright 2019 Eska Technology LLC (www.eskaweb.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    last_donation_date = fields.Date(
        string="Last Donation Date",
        compute="compute_last_donation_date",
        store=True,
    )

    @api.depends('invoice_ids.state')
    def compute_last_donation_date(self):
        for rec in self:
            invoice = self.env['account.invoice'].search([
                ('partner_id', '=', rec.id),
                ('state', '=', 'paid'),
                ('type', '=', 'out_invoice'),
            ], order='date_invoice desc', limit=1)
            rec.last_donation_date = invoice and invoice.date_invoice or False
