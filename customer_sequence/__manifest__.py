{
    'name':'Customer code Sequence',
    'version':'11.1.1.0',
    'author':'A & Team Agaram Soft',
    'company':'Agram Soft',
    'website': "https://agaramtechs.com",
    'summary':' This module is used to genarate sequence code in Customer',
'depends': ['base',
                ],
    'data': [
            'views/customer_seq.xml',
            'data/customer_seq_data.xml',
    ],
    'demo': [

    ],
    'application': True,
    'installable': True,
    'auto_install': False,


}