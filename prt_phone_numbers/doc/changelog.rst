`11.0.1.40`
-----------

- **UPDATE:** Bug fixes and performance improvements


`11.0.1.0`
----------

- Release for Odoo 11
