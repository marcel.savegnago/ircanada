��    Y      �     �      �  '   �  ]   �  (   /  F   X  ,   �  0   �     �     	  	   	     	  
   !	  
   ,	     7	  
   D	     O	     U	     b	     {	     �	     �	     �	     �	  &   �	  "   �	     �	  0   
     =
  
   F
     Q
     U
  W   X
     �
     �
     �
     �
     �
  &   �
  "     "   8     [  
   z     �     �     �  
   �     �     �     �     �  8        =  -   Z     �     �  �   �     �  U   �     �          %     =     F     \     z     �     �     �  ,   �  6   �  1   �     (     /     5     :  R   C     �  "   �     �     �     �     �     �     �  
   �     	  	         *     8  ;  I  >   �  �   �  O   L  �   �  o   .  �   �     3     B     Z     k     z     �     �     �     �     �  >   �  -   -  -   [     �     �     �  3   �  (   �  '     f   @     �     �     �  
   �  �   �     �     �  %   �  (   �  '     ;   4  (   p  (   �  <   �     �  :        V     t     �  9   �     �      �  1     c   9  <   �  ^   �     9     Q  �  `  5   B  �   x  4   "     W  :   s     �     �  3   �               "     &  ?   9  �   y  f         t      {      �      �   �   �      M!  U   T!     �!     �!  /   �!     "     "     "     !"     ;"  	   R"     \"  (   j"     ;   *   .       /   A       !           &                     C   )          '              <   "       S                H                            B   R   E   V      D           I   W   3       	      %   M   4             9   #   O   T          @           +   U                        5          ,       K   X       7   >         -       ?      L       6          N       G      0       8          J   P       Y   $   
       =         (              F   :   2   Q   1    <strong>Duplicates found!</strong><br/> <strong>Please install 'qrcode' python library to generate QR codes for Vcards!</strong><br/> Any tags like 'WhatsApp', 'Telegram' etc Be careful! Company profiles are exported same way as Person profiles. Click to create new Phone Number or Username Click to create new Phone Number or Username Tag Close Color index Companies Contact Created by Created on Display Name Duplicates Email Email Thread Export contact's picture Export contacts Export picture Extension phone Fax File First name, Last name, Additional name First name, Middle name, Last name Format numbers Full access to Partner's Phone Numbers/Usernames Group By Home phone ICQ ID If you want to export Person profiles only please apply search filter in Partners view! Image Jabber Last Modified on Last Updated by Last Updated on Last name, First name, Additional name Last name, First name, Middle name Last name, Middle name, First name Lower value = Higher priority! Main phone Manage Numbers/Usernames Mobile phone Name Name order Name order for name parsing Note Number of duplicates Number or Username Numbers should start with country code (e.g. +1 202 ...) Odoo color index from 0 to 9 Only one "Main" number per partner can exist! Other phone Partner Phone number can contain any symbols but only digits will be used while searching! 
  Should start with country code e.g. +1 100 123-4567 
 Username can contain letters e.g. 'much_username' but whitespaces will be skipped while searching usernames Phone or Username Phone type for number or 'Username' for username containing letters (e.g. Skype name) Phone/Email/Username Phone/Username Tags Phones/Emails/Usernames Priority Put your comment here Put your note or comment here QR Code Reddit SIP Save Save '+123456789' instead of '1(234)56-7 89' Select 'Username' to store user name (e.g. Skype name) Select name format, save record and donwload file Signal Skype Tags Telegram This field holds the image used as avatar for this contact, limited to 1024x1024px Type Use this link to download file ==> Username Value Value formatted for search Viber WeChat WhatsApp Work phone prt.contact.export.wiz prt.phone prt.phone.tag qrcode installed Project-Id-Version: Odoo Server 11.0-20180424
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-04 15:35+0000
PO-Revision-Date: 2018-05-04 18:37+0300
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
X-Generator: Poedit 1.5.4
 <strong>Обнаружены дубликаты!</strong><br/> <strong>Для генерации QR кодов необходимо установить библиотеку 'qrcode' !</strong><br/> Метки для номера (например 'WhatsApp', 'Telegram' итд) Будьте внимательны! Профили людей экспортируются также как и профили компаний. Нажмите чтобы создать Телефонный номер или Имя пользователя Нажмите чтобы создать новую метку для Телефонного номера или Имени пользователя Закрыть Индекс цвета Компании Контакт Создано Создан Отображаемое Имя Дубликаты Email Цепочка эл.почты Экспортировать картинку контакта Эскпортировать контакты Экспортировать картинку Добавочный номер Факс Файл Имя, Фамилия, Добавочное имя Имя, Отчество, Фамилия Форматировать номера Полный доступ к Телефонным номерам/Именам пользователя Группировать по Домашний номер ICQ Номер Если нужно экспортировать только профили людей, используйте фильтр при отображение партнёров! Изображение Jabber Последнее изменение Последний раз обновил Последнее обновление Фамилия, Имя, Дополнительное имя Фамилия, Имя, Отчество Фамилия, Отчество, Имя Меньше значение = Выше приоритет! Основной номер Управлять Номерами/Юзернеймами Мобильный номер Название Формат имени Формат имени для распознавания Заметка Кол-во дубликатов Номер или Имя пользователя Номера следует начинать с кода страны (например +7 202 ...) Индекс цвета в системе Odoo от 0 до 9 У партнёра может быть только один  "Основной"  номер! Другой номер Партнёр Номер телефона может содержать любые символы, но поиск осуществляется только по цифрам! 
  Предпочтительнее в международном формате, например +7 495 123-4567 
 Имя пользователя может содержать любые символы (например 'much_username'), но при поиске пробелы учитываться не будут Телефон или Имя пользователя Тип номера для телефона или 'Имя пользователя' для имени, содержащего буквы (например имя Skype) Телефон/Email/Имя пользователя Тэги телефонов Телефоны/Email/Имена пользователя Приоритет Для комментария Для заметки или комментария QR Code Reddit SIP Сохранить Сохраняет '+7123456789' вместо  '7(1234)56-7 89' Выберите 'Имя пользователя' чтобы сохранить имя пользователя (например имя в Skype) Выберите формат имени, сохраните запись и скачайте файл Signal Skype Теги Telegram Это поле содержит изображение, используемое в качестве аватара для этого контакта, ограничено 1024x1024px Тип Используйте эту ссылку для скачивания файла ==> Имя пользователя Значение Значение в формате поиска Viber WeChat WhatsApp Рабочий номер prt.contact.export.wiz prt.phone prt.phone.tag модуль qrcode установлен 