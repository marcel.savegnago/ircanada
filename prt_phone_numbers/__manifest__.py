# -*- coding: utf-8 -*-
{
    'name': 'Improved Contacts: Several email addresses for partner, multiple phone numbers and usernames for partners. Export contacts vCard and QRCode',
    'version': '11.0.1.40',
    'summary': """Several email addresses, phone numbers and usernames for partner. Share contact as vCard and via QRCode""",
    'author': 'Ivan Sokolov',
    'category': 'Sales',
    'license': 'OPL-1',
    'price': 49.00,
    'currency': 'EUR',
    'support': 'odooapps@cetmix.com',
    'website': 'https://demo.cetmix.com',
    'live_test_url': 'https://demo.cetmix.com',
    'description': """
Multiple Email addresses Several Phone Numbers vCard and QRCode export contacts for Partners
============
1. Add several email addresses, multiple phone numbers or usernames to contacts
2. Choose which number or email to use by default
3. Add tags and notes for easier navigation
4. Export contacts to other apps or devices (such as Iphone, Android, Outlook etc) *
5. Use QR code scanner to add contact easily to your mobile device **

* Notice: please be advised that vCard support depends on the target application or device and may differ for some of them!
** Please install 'qrcode' python library to generate QR codes for Vcards!

""",
    'depends': ['base', 'mail'],
    'images': ['static/description/banner_contacts.png'],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'data/defaults.xml',
        'views/prt_phone.xml',
        'views/prt_vcard.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
