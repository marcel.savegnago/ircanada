from odoo import models, fields, api, tools


###############
# Mail.Thread #
###############
class PRTMailThread(models.AbstractModel):
    _name = "mail.thread"
    _inherit = "mail.thread"


# -- Find partners by email
    @api.multi
    def _find_partner_from_emails(self, emails, res_model=None, res_id=None, check_followers=True, force_create=False, exclude_aliases=True):
        """ Utility method to find partners from email addresses. The rules are :
            1 - check in document (model | self, id) followers
            1.5 - check in document (model | self, id) followers other email addresses
            2 - try to find a matching partner that is also an user
            3 - try to find a matching partner
            3.5. - try to find in related emails (phone/email/username type='7')
            4 - create a new one if force_create = True

            :param list emails: list of email addresses
            :param string model: model to fetch related record; by default self
                is used.
            :param boolean check_followers: check in document followers
            :param boolean force_create: create a new partner if not found
            :param boolean exclude_aliases: do not try to find a partner that could match an alias. Normally aliases
                                            should not be used as partner emails but it could be the case due to some
                                            strange manipulation
        """
        if res_model is None:
            res_model = self._name
        if res_id is None and self.ids:
            res_id = self.ids[0]
        followers = self.env['res.partner']
        if res_model and res_id:
            record = self.env[res_model].browse(res_id)
            if hasattr(record, 'message_partner_ids'):
                followers = record.message_partner_ids

        Partner = self.env['res.partner'].sudo()
        Users = self.env['res.users'].sudo()
        partner_ids = []

        for contact in emails:
            partner_id = False
            email_address = tools.email_split(contact)
            if not email_address:
                partner_ids.append(partner_id)
                continue
            if exclude_aliases and self.env['mail.alias'].search([('alias_name', 'ilike', email_address)], limit=1):
                partner_ids.append(partner_id)
                continue

            email_address = email_address[0]
            # first try: check in document's followers
            partner_id = next((partner.id for partner in followers if partner.email == email_address), False)

            # first and half try: check in document's followers (non default emails)
            if not partner_id:
                for partner in followers:
                    for prt_email in partner.phone_number_ids:
                        if prt_email.type == '7' and prt_email.number_searchable == email_address:
                            partner_id = partner.id
                            break

            # second try: check in partners that are also users
            # Escape special SQL characters in email_address to avoid invalid matches
            email_address = (email_address.replace('\\', '\\\\').replace('%', '\\%').replace('_', '\\_'))
            email_brackets = "<%s>" % email_address
            if not partner_id:
                # exact, case-insensitive match
                partners = Users.search([('email', '=ilike', email_address)], limit=1).mapped('partner_id')
                if not partners:
                    # if no match with addr-spec, attempt substring match within name-addr pair
                    partners = Users.search([('email', 'ilike', email_brackets)], limit=1).mapped('partner_id')
                partner_id = partners.id
            # third try: check in partners
            if not partner_id:
                # exact, case-insensitive match
                partners = Partner.search([('email', '=ilike', email_address)], limit=1)
                if not partners:
                    # if no match with addr-spec, attempt substring match within name-addr pair
                    partners = Partner.search([('email', 'ilike', email_brackets)], limit=1)
                partner_id = partners.id
            # third-and-half try: check in related emails
            if not partner_id:
                # exact, case-insensitive match
                prt_email = self.env['prt.phone'].sudo().search(['&',
                                                                 ('number_searchable', '=ilike', email_address),
                                                                 ('type', '=', '7')], limit=1)
                partner_id = prt_email.mapped('partner_id').id or False
            if not partner_id and force_create:
                partner_id = self.env['res.partner'].name_create(contact)[0]
            partner_ids.append(partner_id)
        return partner_ids
