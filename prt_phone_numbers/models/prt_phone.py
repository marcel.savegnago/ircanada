from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError

import re

import logging
_logger = logging.getLogger(__name__)


# -- Format number to store as searchable
def prep_num(number, num_type):
    if num_type in ['7', '8']:
        return number.replace(" ", "")
    else:
        return re.sub("[^0-9]", "", number)


# -- Remove duplicates from phone_number_ids
def remove_duplicates(phone_number_ids, env):
    new_phone_number_ids = []

    # Check every entry to find similar entries in already sanitized entries
    for phone_number_entry in phone_number_ids:

        # Proceed only create or update
        if phone_number_entry[0] not in [0, 1]:
            new_phone_number_ids.append(phone_number_entry)
            continue

        has_duplicate = False

        # If we update the number or type only get missing vals by is to do correct computations
        if phone_number_entry[0] == 1:

            # Get type and store it so we can use it for next iterations
            if 'type' not in phone_number_entry[2]:
                phone_type = env['prt.phone'].search([('id', '=', phone_number_entry[1])]).type or False
                phone_number_entry[2].update({'type': phone_type})
            else:
                phone_type = phone_number_entry[2].get('type', False)

            # Get number and store it so we can use it for next iterations
            if 'number' not in phone_number_entry[2]:
                phone_number = env['prt.phone'].search([('id', '=', phone_number_entry[1])]).number or False
                phone_number_entry[2].update({'number': phone_number})

        # Take from vals for new entry
        else:
            phone_type = phone_number_entry[2].get('type', False)

        number_formatted = prep_num(phone_number_entry[2].get('number'), phone_type)
        tags = phone_number_entry[2].get('tags', False)[0][2] if phone_number_entry[2].get('tags', False) else False
        len_tags = len(tags) if tags else 0
        note = phone_number_entry[2].get('note', False)
        len_note = len(note) if note else 0

        for new_entry in new_phone_number_ids:

            # Proceed only newly create or update
            if new_entry[0] not in [0, 1]:
                continue

            if prep_num(new_entry[2].get('number', False), new_entry[2].get('type', False)) == number_formatted:
                has_duplicate = True

                # Add tags
                if tags and len_tags > 0:
                    new_tags = tags + new_entry[2].get('tags', False)[0][2] or tags
                    new_entry[2].update({'tags': [[6, 0, new_tags]]})

                # Add note
                if note and len_note > 0:
                    new_note = '%s %s' % (note,  new_entry[2].get('note', False)) if new_entry[2].get('note', False) else note
                    new_entry[2].update({'note': new_note})
                break

        if not has_duplicate:
            new_phone_number_ids.append(phone_number_entry)
    return new_phone_number_ids


##############
# Phone tags #
##############
class PRTPhoneTag(models.Model):
    _name = "prt.phone.tag"

    name = fields.Char(string="Name", required=True, translate=True)
    color = fields.Integer(string="Color index",
                           help="Odoo color index from 0 to 9")


########################
# Phone/Email/Username #
########################
class PRTPhone(models.Model):
    _name = 'prt.phone'
    _description = "Phone numbers and email addresses"
    _order = 'partner_id, type, sequence'
    _rec_name = 'number'

    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner', required=False, ondelete='cascade')
    image = fields.Binary(related='partner_id.image', readonly=True)
    sequence = fields.Integer(string="Priority", default=0, help="Lower value = Higher priority!")
    number = fields.Char(string="Value", required=True,
                         help="Phone number can contain any symbols but only digits will be used while searching! \n "
                              " Should start with country code e.g. +1 100 123-4567 \n"
                              " Username can contain letters e.g. 'much_username'"
                              " but whitespaces will be skipped while searching usernames")
    number_searchable = fields.Char(string="Number or Username",
                                    compute='get_number_searchable',
                                    store='True',
                                    help="Value formatted for search",
                                    index=True)
    type = fields.Selection([
        ('0', 'Main phone'),
        ('1', 'Mobile phone'),
        ('2', 'Work phone'),
        ('3', 'Extension phone'),
        ('4', 'Home phone'),
        ('5', 'Fax'),
        ('6', 'Other phone'),
        ('7', 'Email'),
        ('8', 'Username'),
    ], string="Type", required=True,
        help="Phone type for number or 'Username' for username containing letters (e.g. Skype name)")

    note = fields.Char(string="Note", help="Put your note or comment here", translate=True)
    tags = fields.Many2many(string="Tags",
                            comodel_name='prt.phone.tag',
                            relation='prt_phone_tag_rel',
                            column1='phone_id',
                            column2='tag_id',
                            help="Any tags like 'WhatsApp', 'Telegram' etc")

    """ 
    Constraint used only when creating from own Form View.
    Only one "Main" phone can exist! 
    """
# -- Python Constraint
    @api.multi
    @api.constrains('type')
    def _check_main_count(self):
        for rec in self:
            if rec.partner_id:
                main_count = self.env['res.partner'].sudo().search_count(
                    ['&', ('id', '=', rec.partner_id.id), ('type', '=', '0')])
                if main_count > 0:
                    raise ValidationError(_('Only one "Main" number per partner can exist!'))

# -- Set formatted phone number
    @api.depends('number')
    @api.multi
    def get_number_searchable(self):
        for rec in self:
            if rec.number and len(rec.number) > 0:
                rec.number_searchable = prep_num(rec.number, rec.type)

# -- Compute number of runs need to init all data
    @api.model
    def configure_cron(self):
        self._cr.execute(""" SELECT COUNT (id) FROM (SELECT  id, phone, mobile, email FROM res_partner prt                            
                                                WHERE (SELECT COUNT(id) FROM prt_phone WHERE prt_phone.partner_id = prt.id) = 0
                                                AND (length(prt.phone)>0 OR length(prt.mobile)>0 OR length(prt.email)>0)) AS preselect """)
        rec_count = self._cr.fetchall()[0][0]
        if rec_count == 0:
            _logger.info(" >>> PHONE/EMAIL IMPORT: No records to import is found. Cron job will be disabled")
            self.env.ref('prt_phone_numbers.cx_store_phone_email_data').sudo().write({'numbercall': -1})
        batch_num = rec_count // 15000 + 1
        _logger.info(" >>> PHONE/EMAIL IMPORT: Total %s records found. Will do import in %s batches" % (str(rec_count), str(batch_num)))
        self.env.ref('prt_phone_numbers.cx_store_phone_email_data').sudo().write({'numbercall': batch_num})

# -- Store existing phone numbers and emails
    @api.model
    def init_data_direct(self):

        _logger.info(" >>> PHONE/EMAIL IMPORT STARTED")
        # Get list of ids of
        self._cr.execute(""" SELECT id, phone, mobile, email FROM res_partner prt                            
                                        WHERE (SELECT COUNT(id) FROM prt_phone WHERE prt_phone.partner_id = prt.id) = 0
                                        AND (length(prt.phone)>0 OR length(prt.mobile)>0 OR length(prt.email)>0) 
                                        LIMIT 2000""")
        # Get existing data
        res_len = 0
        for res in self._cr.fetchall():
            partner_id = res[0]
            # Main
            if res[1]:
                self.env['prt.phone'].sudo().create({'partner_id': partner_id, 'type': '0', 'number': res[1]})
            # Mobile
            if res[2]:
                # rec.append([0, False, {'type': '1', 'number': res[2]}])
                self.env['prt.phone'].sudo().create({'partner_id': partner_id, 'type': '1', 'number': res[2]})
            # Email
            if res[3]:
                # rec.append([0, False, {'type': '7', 'number': res[3]}])
                self.env['prt.phone'].sudo().create({'partner_id': partner_id, 'type': '7', 'number': res[3]})

            res_len += 1

        _logger.info(" >>> PHONE/EMAIL IMPORT COMPLETE. NUMBER OF RECORDS IMPORTED = %s", str(res_len))

# -- Store existing phone numbers and emails
    @api.model
    def init_data(self):

        _logger.info(" >>> PHONE/EMAIL IMPORT STARTED")
        # Get list of ids of
        self._cr.execute(""" SELECT id, phone, mobile, email FROM res_partner prt                            
                                        WHERE (SELECT COUNT(id) FROM prt_phone WHERE prt_phone.partner_id = prt.id) = 0
                                        AND (length(prt.phone)>0 OR length(prt.mobile)>0 OR length(prt.email)>0) 
                                        LIMIT 15000""")
        # Get existing data
        res_len = 0
        for res in self._cr.fetchall():
            partner_id = res[0]
            # Main
            if res[1]:
                sql = "INSERT INTO prt_phone (partner_id, type, number, number_searchable)" \
                      " VALUES (%s, %s, '%s', '%s')" % (partner_id, '0', res[1], prep_num(res[1], '0'))
                self._cr.execute(sql)
                # rec.append([0, False, {'type': '0', 'number': res[1]}])
            # Mobile
            if res[2]:
                sql = "INSERT INTO prt_phone (partner_id, type, number, number_searchable)" \
                      " VALUES (%s, %s, '%s', '%s')" % (partner_id, '1', res[2], prep_num(res[2], '1'))
                self._cr.execute(sql)
                # rec.append([0, False, {'type': '1', 'number': res[2]}])
            # Email
            if res[3]:
                sql = "INSERT INTO prt_phone (partner_id, type, number, number_searchable)" \
                      " VALUES (%s, %s, '%s', '%s')" % (partner_id, '7', res[3], prep_num(res[3], '7'))
                self._cr.execute(sql)
                # rec.append([0, False, {'type': '7', 'number': res[3]}])

            res_len += 1

        # Invalidate cache
        self.invalidate_cache()
        _logger.info(" >>> PHONE/EMAIL IMPORT COMPLETE. NUMBER OF RECORDS IMPORTED = %s", str(res_len))


# -- Create
    @api.model
    def create(self, vals):
        """
        Check if same number, email or username already exist for this partner
        Return False instead of creating new record if full duplicate is found.
        If unformatted number differs - update existing numbers instead of creating new ones.
        If number type is main, mobilem, work, home or other change type not to create a duplicate
         """
        number = vals.get('number', False)
        num_type = vals.get('type', False)
        partner_id = vals.get('partner_id', False)
        sequence = vals.get('sequence', 0)

        # Update duplicates instead of creating new record
        if number and num_type and partner_id:
            search_type = ['0', '1', '2', '4', '6'] if num_type in ['0', '1', '2', '4', '6'] else [num_type]
            duplicates = self.search(
                ['&', '&', ('partner_id', '=', partner_id),
                 ('type', 'in', search_type),
                 ('number_searchable', '=', prep_num(number, num_type))])

            if len(duplicates) > 0:
                # Check if unformatted number differs
                numbers_update_ids = []
                for duplicate in duplicates:
                    if not number == duplicate.number:
                        numbers_update_ids.append(duplicate.id)

                # Update duplicates with different unformatted numbers
                if len(numbers_update_ids) > 0:
                    self.browse(numbers_update_ids).sudo().write({'number': number, 'type': num_type})

                # Update all duplicates with note if note exist
                note = vals.get('note', False)
                if note and len(note) > 0:
                    for duplicate in duplicates:
                        duplicate.note = duplicate.note + ' ' + note if duplicate.note else note

                # If tags added - add them to duplicates
                tags = vals.get('tags', False)

                if tags and len(tags) > 0:
                    add_tags = []
                    for tag in tags[0][2]:
                        add_tags.append([4, tag, False])
                    duplicates.sudo().write({'tags': add_tags})

                return False  # Be careful! Some addons do not check create(vals) result for troubles

        # If new number is 'Main' change other 'Main' numbers to 'Work'
        if partner_id and num_type == '0':
            other_main = self.search(['&', ('partner_id', '=', partner_id), ('type', '=', '0')])
            if other_main:
                other_main.sudo().write({'type': '2'})

        """ If created from legacy form fields change sequence of existing records of same type
            so newly created record will be set as default.            
        """
        if sequence == -100500:
            vals['sequence'] = 0
            other_same = self.search(['&', ('partner_id', '=', partner_id), ('type', '=', num_type)])
            if other_same:
                other_same.sudo().write({'sequence': 1})

        # Create finally)
        return super(PRTPhone, self).create(vals)

# -- Write
    @api.multi
    def write(self, vals):
        """
        In case type is 'Main' change other 'Main' numbers to 'Work'
        """
        if vals.get('type', False) == '0':
            other_main = self.search(['&', ('partner_id', 'in', self.mapped('partner_id').ids),
                                            ('type', '=', '0')])
            if other_main:
                other_main.sudo().write({'type': '2'})

        return super(PRTPhone, self).write(vals)


###############
# Res.Partner #
###############
class PRTPartner(models.Model):
    _name = "res.partner"
    _inherit = "res.partner"

# -- Default email address from context
    @api.model
    def _default_email(self):
        email = self._context.get('default_email', False)
        if email:
            return [(0, False, {
                'type': '7',
                'number': email
            })]

    phone_number_ids = fields.One2many(string="Phones/Emails/Usernames", comodel_name='prt.phone',
                                       inverse_name='partner_id',
                                       default=_default_email)
    phone = fields.Char(compute='_get_phone', store=True, inverse="_dummy")
    mobile = fields.Char(compute='_get_mobile', store=True, inverse="_dummy")
    email = fields.Char(compute='_get_email', store=True, inverse="_dummy")
    phone_searchable = fields.Char(string="Phone/Email/Username", related='phone_number_ids.number_searchable')
    phone_number_duplicates = fields.One2many(string="Duplicates", comodel_name='prt.phone',
                                              compute='_get_phone_duplicates')
    phone_number_duplicates_count = fields.Integer(string="Number of duplicates", compute='_get_phone_duplicates_count')


# -- Dummy function
    @api.multi
    def _dummy(self):
        return

# -- Get duplicates of phone numbers, emails & usernames
    @api.multi
    def _get_phone_duplicates(self):
        for rec in self:
            rec.phone_number_duplicates = self.env['prt.phone'].search(
                ['&', ('partner_id', '!=', rec.id),
                 ('number_searchable', 'in', rec.phone_number_ids.mapped('number_searchable'))])

# -- Get count of duplicates of phone numbers, emails & usernames
    @api.depends('phone_number_ids.number')
    @api.multi
    def _get_phone_duplicates_count(self):
        for rec in self:
            rec.phone_number_duplicates_count = len(rec.phone_number_duplicates)

# -- Get phone number
    @api.depends('phone_number_ids', 'phone_number_ids.type', 'phone_number_ids.number')
    @api.multi
    def _get_phone(self):
        for rec in self:
            for number in rec.phone_number_ids:
                if number.type not in ['1', '3', '7', '8']:
                    rec.phone = number.number
                    break

# -- Get mobile number
    @api.depends('phone_number_ids', 'phone_number_ids.type', 'phone_number_ids.sequence', 'phone_number_ids.number')
    @api.multi
    def _get_mobile(self):
        for rec in self:
            for number in rec.phone_number_ids:
                if number.type == '1':
                    rec.mobile = number.number
                    break

# -- Get email
    @api.depends('phone_number_ids', 'phone_number_ids.type', 'phone_number_ids.sequence', 'phone_number_ids.number')
    @api.multi
    def _get_email(self):
        for rec in self:
            for number in rec.phone_number_ids:
                if number.type == '7':
                    rec.email = number.number_searchable
                    break

# -- Create
    @api.model
    def create(self, vals):
        """ Override 'create' in case Partner is created by some legacy method
        Check if phone, mobile or email in vals and modify them.
        Set sequence=-100500 so we can detect that number is created from original field (legacy method)
        """

        # Get vals if any and store them as needed
        phone_number_ids = vals.pop('phone_number_ids', [])

        # Add phone
        number = vals.pop('phone', False)
        if number and len(number) > 0:
            phone_number_ids.append([0, False, {'type': '0', 'number': number, 'sequence': -100500}])

        # Add mobile
        number = vals.pop('mobile', False)
        if number and len(number) > 0:
            phone_number_ids.append([0, False, {'type': '1', 'number': number, 'sequence': -100500}])

        # Add email
        number = vals.pop('email', False)
        if number and len(number) > 0:
            phone_number_ids.append([0, False, {'type': '7', 'number': number, 'sequence': -100500}])

        # Store phone numbers if any
        if len(phone_number_ids) > 0:
            vals.update({'phone_number_ids': remove_duplicates(phone_number_ids, self.env)})

        new_rec = super(PRTPartner, self).create(vals)

        return new_rec


# -- Write
    @api.multi
    def write(self, vals):
        """ Override 'write' in case Partner data is written by some legacy method
        Check if phone, mobile or email in vals and modify them.
        Set sequence=-100500 so we can detect that number is created from original field (legacy method)
        """

        # Check is writing to new record
        if self._context.get('on_create', False):
            return super(PRTPartner, self).write(vals)

        # Get vals if any and store them as needed
        phone_number_ids = vals.pop('phone_number_ids', [])

        # Get ids of modified numbers so we do not delete them occasionally
        modify_ids = []
        for phone_number in phone_number_ids:
            if phone_number[0] == 1:
                modify_ids.append(phone_number[1])

        # Add/remove phone
        if 'phone' in vals:
            number = vals.pop('phone', False)
            if number and len(number) > 0:
                phone_number_ids.append([0, False, {'type': '0', 'number': number, 'sequence': -100500}])
            else:
                # Get numbers to delete and add them to list to delete them on write
                for rec in self:
                    numbers2del = self.env['prt.phone'].search([('partner_id', '=', rec.id),
                                                                ('type', '=', '0'),
                                                                ('id', 'not in', modify_ids),
                                                                ('number', '=', rec.phone)])
                    if numbers2del:
                        for number2del in numbers2del:
                            phone_number_ids.append((2, number2del.id, False))

        # Add/remove mobile
        if 'mobile' in vals:
            number = vals.pop('mobile', False)
            if number and len(number) > 0:
                phone_number_ids.append([0, False, {'type': '1', 'number': number, 'sequence': -100500}])
            else:
                # Get numbers to delete and add them to list to delete them on write
                for rec in self:
                    numbers2del = self.env['prt.phone'].search([('partner_id', '=', rec.id),
                                                                ('type', '=', '1'),
                                                                ('id', 'not in', modify_ids),
                                                                ('number', '=', rec.mobile)])
                    if numbers2del:
                        for number2del in numbers2del:
                            phone_number_ids.append((2, number2del.id, False))

        # Add/remove email
        if 'email' in vals:
            number = vals.pop('email', False)
            if number and len(number) > 0:
                phone_number_ids.append([0, False, {'type': '7', 'number': number, 'sequence': -100500}])
            else:
                # Get numbers to delete and add them to list to delete them on write
                for rec in self:
                    numbers2del = self.env['prt.phone'].search([('partner_id', '=', rec.id),
                                                                ('type', '=', '7'),
                                                                ('id', 'not in', modify_ids),
                                                                ('number', '=', rec.email)])

                    if numbers2del:
                        for number2del in numbers2del:
                            phone_number_ids.append((2, number2del.id, False))

        # Sanitize phone number list

        if len(phone_number_ids) > 0:
            vals.update({'phone_number_ids': remove_duplicates(phone_number_ids, self.env)})

        return super(PRTPartner, self).write(vals)


###############
# Res.Company #
###############
class PRTCompany(models.Model):
    _name = "res.company"
    _inherit = "res.company"

    phone_number_ids = fields.One2many(related='partner_id.phone_number_ids', comodel_name='prt.phone')
    phone_number_duplicates = fields.One2many(string="Duplicates", comodel_name='prt.phone',
                                              related='partner_id.phone_number_duplicates')
    phone_number_duplicates_count = fields.Integer(string="Number of duplicates",
                                                   related='partner_id.phone_number_duplicates_count')


#############
# Res.Users #
#############
class PRTUsers(models.Model):
    _name = "res.users"
    _inherit = "res.users"

# -- Create
    @api.model
    def create(self, vals):
        vals['email'] = vals.get('login', False)
        return super(PRTUsers, self).create(vals)

# -- Write
    @api.multi
    def write(self, vals):
        login = vals.get('login', False)
        if login:
            vals['email'] = login
        return super(PRTUsers, self).write(vals)
