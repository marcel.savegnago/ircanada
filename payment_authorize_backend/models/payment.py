# -*- coding: utf-8 -*-
# Copyright 2017 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).
from odoo import _
from odoo.addons.sale_payment.models.payment import PaymentTransaction
import logging

_logger = logging.getLogger(__name__)


def confirm_sale_token(self):
    """ Confirm a transaction token and call SO confirmation if it is a success.

    :return: True if success; error string otherwise """
    self.ensure_one()
    if self.payment_token_id and self.partner_id == self.sale_order_id.partner_invoice_id:
        try:
            s2s_result = self.s2s_do_transaction()
        except Exception as e:
            _logger.warning(
                _("<%s> transaction (%s) failed: <%s>") %
                (self.acquirer_id.provider, self.id, str(e)))
            return 'pay_sale_tx_fail'

        valid_state = 'authorized' if self.acquirer_id.capture_manually else 'done'
        if not s2s_result or self.state != valid_state:
            _logger.warning(
                _("<%s> transaction (%s) invalid state: %s") %
                (self.acquirer_id.provider, self.id, self.state_message))
            return 'pay_sale_tx_state'

        try:
            return self._confirm_so()
        except Exception as e:
            _logger.warning(
                _("<%s> transaction (%s) order confirmation failed: <%s>") %
                (self.acquirer_id.provider, self.id, str(e)))
            return 'pay_sale_tx_confirm'
    return 'pay_sale_tx_token'
PaymentTransaction.confirm_sale_token = confirm_sale_token


def _check_or_create_sale_tx(self, order, acquirer, payment_token=None, tx_type='form', add_tx_values=None, reset_draft=True):
    tx = self
    if not tx:
        tx = self.search([('reference', '=', order.name)], limit=1)

    if tx.state in ['error', 'cancel']:  # filter incorrect states
        tx = False
    if (tx and tx.acquirer_id != acquirer) or (tx and tx.sale_order_id != order):  # filter unmatching
        tx = False
    if tx and payment_token and tx.payment_token_id and payment_token != tx.payment_token_id:  # new or distinct token
        tx = False

    # still draft tx, no more info -> rewrite on tx or create a new one depending on parameter
    if tx and tx.state == 'draft':
        if reset_draft:
            tx.write(dict(
                self.on_change_partner_id(order.partner_id.id).get('value', {}),
                amount=order.amount_total,
                type=tx_type)
            )
        else:
            tx = False

    if not tx:
        tx_values = {
            'acquirer_id': acquirer.id,
            'type': tx_type,
            'amount': order.amount_total,
            'currency_id': order.pricelist_id.currency_id.id,
            'partner_id': order.partner_invoice_id.id,
            'partner_country_id': order.partner_invoice_id.country_id.id,
            'reference': self._get_next_reference(order.name, acquirer=acquirer),
            'sale_order_id': order.id,
        }
        if add_tx_values:
            tx_values.update(add_tx_values)
        if payment_token and payment_token.sudo().partner_id == order.partner_invoice_id:
            tx_values['payment_token_id'] = payment_token.id

        tx = self.create(tx_values)

    # update quotation
    order.write({
        'payment_tx_id': tx.id,
    })

    return tx
PaymentTransaction._check_or_create_sale_tx = _check_or_create_sale_tx


