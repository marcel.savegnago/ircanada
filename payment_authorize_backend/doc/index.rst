=============================
Payment Authorize.net Backend
=============================

Configuration
=============

To configure this module follow the below steps.

* Go to Accounting -> Configuration -> Payment Acquirers -> Authorize.net and configure with your API login and transaction key and publish it.
* Create a Journal of type bank to use it Authorize.net transactions.
* In that Jounal Go to Advanced Settings.
* Under Miscellaneous Group choose the Debit method as Electronic.
* Choose Payment Acquirer as Authorize.net and leave Payment methods as unchecked.
