# -*- coding: utf-8 -*-
from odoo import api, fields, models

class MailChannel(models.Model):
    """
        It inherit base model and add id_offline_channel boolean field
    """
    _inherit = 'mail.channel'
    
    is_offline_channel = fields.Boolean('Is Offline Channel', help='')