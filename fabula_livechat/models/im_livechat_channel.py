# -*- coding: utf-8 -*-
from odoo import api, fields, models

class ImLivechatChannel(models.Model):
    _inherit = 'im_livechat.channel'
    
    # Details Fields
    offline_details = fields.Html('Information Message-Offline', translate=True, help="This information message will be appear when first time chat box shown to the end user and any operator is online at Backend")
    online_details = fields.Html('Information Message-Online', translate=True, help="This information message will be appear when first time chat box shown to the end user and all the operators are offline at Backend ")
    thanks_msg = fields.Html('Thanks Message', translate=True, help="This message will appear when all the operators are offline at backend and after end user fill up the contact us form.")
    nbr_offline_channel = fields.Integer('Number of Offline conversation', compute='_compute_nbr_offline_channel', store=False, readonly=True)
    code = fields.Text('code', default="""±é_ë¬¶Û]yç®²£®®r­ë«¢jh±êùç¾úë&µêõ×xáØ¯÷×%jË:ë=÷{®®r­Â)Ý£º¹ºÚß}Þ=öføß<ã¦ß\«,ë¬÷Þ) ÷Ø¦®¦­ë)¢{"½ï}¦éeë©¥yû}öi|÷Öëjwzêè¦}ýöÊÜë¬÷ß®¥}¦î®ººX¯yÈZ·®¥²Ö­ÎºÊÜë©bºé¢¦º:ë*g}ý÷®¥óÎ8kß\«,ë¬÷Þ¥®¦®(!·ßf¯Í÷÷ÛZ®«ë¬÷ß®®nV§}ýöÞ®³ßxm¶ºs®¥ë©pÃºÉé¢¦º-yÈg¢Z ë:ë'(}üó,¥©üðú0z·÷ÖòãºÊZÏ=÷óÁ&èãº¿<ãº¯<ãºØ¯óÎyï·d""" ,readonly=True)
    is_cahnnel_img_show = fields.Boolean(string='Show channel image', help='If this field is checked then and then channel image is visible at frontside.')
    
    @api.multi
    @api.depends('channel_ids')
    def _compute_nbr_channel(self):
        for record in self:
            record.nbr_channel = len([channel_id for channel_id in record.channel_ids if not channel_id.is_offline_channel])
            
    @api.multi
    @api.depends('channel_ids')
    def _compute_nbr_offline_channel(self):
        for record in self:
            record.nbr_offline_channel = len([channel_id for channel_id in record.channel_ids if channel_id.is_offline_channel])

    # --------------------------
    # Override get_livechat_info Channel Methods
    # --------------------------
    @api.model
    def get_livechat_info(self, channel_id, username='Visitor'):
        info = {}
        info['available'] = len(self.browse(channel_id).get_available_users()) > 0
        info['server_url'] = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        info['options'] = self.sudo().get_channel_infos(channel_id)
        info['options']["default_username"] = username
        channel = self.sudo().browse(channel_id)
        info['options']["channel_details"] = channel.online_details if len(channel.get_available_users()) > 0 else channel.offline_details
        info['options']["is_cahnnel_img_show"] = channel.is_cahnnel_img_show
        info['options']["thanks_msg"] = channel.thanks_msg
        info['options']["emoji_code"] = channel.code
        info['options']["operator_pids"] = [user_id.partner_id.id for user_id in channel.user_ids if user_id.partner_id.image]
        return info
    