# -*- coding: utf-8 -*-
from odoo.http import request, route
from odoo.addons.mail.controllers.bus import MailChatController

class FabulaLiveChat(MailChatController):

    # --------------------------
    # Anonymous routes (Common Methods)
    # --------------------------
    @route('/mail/chat_post', type="json", auth="none")
    def mail_chat_post(self, uuid, message_content, **kwargs):
        request_uid = self._default_request_uid()
        # find the author from the user session, which can be None
        
        author_id = False # message_post accept 'False' author_id, but not 'None'
        if request.session.get('listener_pid', False) and not request.session.uid:
            author_id = request.session.get('listener_pid')
          
        if request.session.uid:
            author_id = request.env['res.users'].sudo().browse(request.session.uid).partner_id.id
       
        # post a message without adding followers to the channel. email_from=False avoid to get author from email data
        mail_channel = request.env["mail.channel"].sudo(request_uid).search([('uuid', '=', uuid)], limit=1)
        message = mail_channel.sudo(request_uid).with_context(mail_create_nosubscribe=True).message_post(author_id=author_id, email_from=False, body=message_content, message_type='comment', subtype='mail.mt_comment', content_subtype='plaintext', **kwargs)
        return message and message.id or False
    
    # --------------------------
    # Add website ChatWindow Attachment id in mail_Chat_post method
    # --------------------------
    @route('/mail/chat_post_attachment', type="json", auth="none")
    def mail_chat_post_attachment(self, uuid, message_content, message_attachment, **kwargs):
        if len(message_attachment):
            kwargs.update({'attachment_ids' : message_attachment})
        
        response = self.mail_chat_post(uuid=uuid, message_content=message_content, **kwargs)
        return response
    
    @route(['/mail/chat_history'], type="json", auth="none")
    def mail_chat_history(self, uuid, last_id=False, limit=20):
        if request.session.uid:
            partner_id = request.env['res.users'].sudo().browse(request.session.uid).partner_id.id
            channel = request.env['mail.channel'].sudo().search([('uuid', '=', uuid)], limit=1)
            if channel and partner_id not in channel.channel_partner_ids.ids: 
                channel.with_context(lang=False).write({'channel_partner_ids': [(4, partner_id)]})
        response = super(FabulaLiveChat, self).mail_chat_history(uuid=uuid, last_id=last_id, limit=limit)
        return response
    
    
