# -*- coding: utf-8 -*-
import werkzeug.wrappers
import logging
import functools
import json
import base64

import werkzeug
import odoo
from odoo import http ,tools
from odoo.tools.translate import _
from odoo.http import request, \
                      serialize_exception as _serialize_exception
from odoo.addons.calendar.models.res_partner import Partner
from odoo.addons.im_livechat.controllers.main import LivechatController

_logger = logging.getLogger(__name__)

from odoo.addons.web.controllers.main import Binary
  
def serialize_exception(f):
    @functools.wraps(f)
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            _logger.exception("An exception occured during an http request")
            se = _serialize_exception(e)
            error = {
                'code': 200,
                'message': "Odoo Server Error",
                'data': se
            }
            return werkzeug.exceptions.InternalServerError(json.dumps(error))
    return wrap

class FabulaLivechatController(LivechatController):
    
    @http.route('/im_livechat/feedback', type='json', auth='public')
    def feedback(self, uuid, rate, reason=None, **kwargs):
        Rating = request.env['rating.rating']
        response = super(FabulaLivechatController, self).feedback(uuid=uuid, rate=rate, reason=reason, **kwargs)
        if response:
            rating = Rating.search([('id', '=', response)])
            author_id = False # message_post accept 'False' author_id, but not 'None'
            if request.session.get('listener_pid', False) and not request.session.uid:
                author_id = request.session.get('listener_pid')
                
            if request.session.uid:
                author_id = request.env.user.partner_id.id
        
            rating.write({'partner_id': author_id})        
            return rating.id
        return response

class PublicBinary(http.Controller):

    @http.route('/web/binary/upload_attachment/public', type='http', auth="public")
    @serialize_exception
    def upload_attachment_public(self, callback, model, id, ufile):
        Model = request.env['ir.attachment'].sudo()
        out = """<script language="javascript" type="text/javascript">
                            var win = window.top.window;
                            win.jQuery(win).trigger(%s, %s);
                        </script>"""
        try:
            attachment = Model.create({
                'name': ufile.filename,
                'datas': base64.encodestring(ufile.read()),
                'datas_fname': ufile.filename,
                'res_model': model,
                'res_id': int(id)
            })
            args = {
                'filename': ufile.filename,
                'mimetype': ufile.content_type,
                'id': attachment.id
            }
        except Exception:
            args = {'error': _("Something horrible happened")}
            _logger.exception("Fail to upload attachment %s" % ufile.filename)
        return out % (json.dumps(callback), json.dumps(args))

    @http.route('/web/session/chat_window/get_session_info', type='json', auth="none")
    def get_session_info(self):
        if not request.session.uid:
            session = {'uid': 0}
            return session
        request.session.check_security()
        request.uid = request.session.uid
        request.disable_db = False
        return request.env['ir.http'].session_info()

    @http.route('/livechat/startchat', type='json', auth='public')
    def ChatForm(self, channel_id, name, email, **kwargs):
        Channel = request.env['mail.channel'].sudo()
        Partner = request.env['res.partner'].sudo()

        channel_id = Channel.search([('id', '=', channel_id)], limit=1)
        partner_id = Partner.search([('email','=',email)], limit=1)

        if not partner_id:
            partner_id = Partner.create({'name': name,'email': email,'im_status': 'online'})
        
        channel_name = channel_id.name.split(',')
        channel_name[0] = partner_id.name
        channel_name = ' ,'.join(channel_name)
        
        channel_id.with_context(lang=False).write({'name': channel_id.name, 'channel_partner_ids': [(4, partner_id.id)],'anonymous_name': partner_id.name})
        
        request.session['listener_pid'] = partner_id.id
        return {'name': channel_id.name,'anonymous_name': partner_id.name, 'email': partner_id.email, 'partner_id': partner_id.id}

    @http.route('/get_operator_status', type='json', auth='public')
    def get_operator_status(self, channel_id, partner_id):
        MailChannel = request.env['mail.channel'].sudo()
        Partners = request.env['res.partner'].sudo()
        channel_id = MailChannel.search([('id', '=', channel_id)], limit=1)

        if channel_id and partner_id in channel_id.channel_partner_ids.ids:
            partner_id = Partners.search([('id', '=', partner_id)], limit=1)
            return {'status': partner_id.im_status , 'partner_id' : partner_id.id, 'image': partner_id.image}
        else:
            return {'status': False , 'partner_id' : partner_id, 'image': False}

    
    @http.route('/livechat/offline_channel', type='json', auth="public")
    def offline_channel(self, channel_id, name, email, message):
        LivechatChannel = request.env['im_livechat.channel'].sudo()
        MailChannel = request.env['mail.channel'].sudo()
        Partners = request.env['res.partner'].sudo()
        MailMessage = request.env['mail.message'].sudo()
        
        if not channel_id:
            return False
        
        Channel =  LivechatChannel.browse([channel_id])
        channel_name = []
        
        #check if partner is not exist then create it 
        partner_id = Partners.search([('email', '=', email)])
        if not partner_id:
            partner_id = Partners.create({'name': name, 'email': email})
        if len(partner_id) > 1:
            partner_id = partner_id.filtered(lambda x: not x.parent_id)
            if len(partner_id) > 1:
                partner_id = partner_id[0]

        channel_partner_to_add = [(4, partner_id.id)]
        channel_name.append(partner_id.name)
        #partner to add to the mail.channel
        for uses_id in Channel.user_ids:
            channel_partner_to_add.append((4, uses_id.partner_id.id))
            channel_name.append(uses_id.partner_id.name)
        
        values ={
            'channel_partner_ids': channel_partner_to_add,
            'livechat_channel_id': channel_id,
            'anonymous_name': name,
            'channel_type': 'livechat',
            'name': 'Visitor : ' + ', '.join(channel_name) + '-Offline',
            'description': message,
            'public': 'private',
            'email_send': True,
            'is_offline_channel': True,
        }
        mail_channel_id =MailChannel.with_context(mail_create_nosubscribe=False).create(values)
        MailMessage.create({
                'author_id': partner_id.id,
                'model': 'mail.channel',
                'res_id': mail_channel_id.id,
                'body': tools.plaintext2html(message),
                'subject': mail_channel_id.name,
                'message_type': 'comment',
                'subtype_id': request.env.ref('mail.mt_comment').id,
                'partner_ids': channel_partner_to_add,
            })
        return {'name': partner_id.name }

    @http.route('/fabula_livechat/emojis', type='json', auth='public')
    def mail_client_action(self):
        values = {
            'shortcodes': request.env['mail.shortcode'].sudo().search_read([], ['shortcode_type','unicode_source', 'source', 'substitution', 'description']),
        }
        return values
    
    @http.route('/email_transcript', type='json', auth='public')
    def email_transcript(self, operator_pid, subject, email_to, body, channel_id):
        MailMail = request.env['mail.mail'].sudo()
        Partners = request.env['res.partner'].sudo()
        partner_id = Partners.browse([operator_pid])
        
        MailMail.create({
                'subject': subject, 
                'author_id': partner_id.id,
                'email_from': partner_id.email,
                'email_to': email_to,
                'body_html': body,
                'model': 'mail.channel',
                'res_id': channel_id,
                'auto_delete': True,
            }).send()
        return True
    
class FabulaBinary(Binary):
        
    @http.route(['/fabula/image/<string:model>/<int:id>/<string:field>'], type='http', auth="public")
    def fabula_image(self, model='ir.attachment', id=None, field='datas', filename=None, filename_field='datas_fname', download=None, mimetype=None, width=0, height=0):
        status, headers, content = request.registry['ir.http'].publlic_binary_content(model=model, id=id, field=field,filename=filename, filename_field=filename_field, download=download, mimetype=mimetype, default_mimetype='image/png')
        
        if status == 304:
            return werkzeug.wrappers.Response(status=304, headers=headers)
        elif status == 301:
            return werkzeug.utils.redirect(content, code=301)
        elif status != 200:
            return request.not_found()

        if content and (width or height):
            # resize maximum 500*500
            if width > 500:
                width = 500
            if height > 500:
                height = 500
            content = odoo.tools.image_resize_image(base64_source=content, size=(width or None, height or None), encoding='base64', filetype='PNG')
            # resize force png as filetype
            headers = self.force_contenttype(headers, contenttype='image/png')

        if content:
            image_base64 = base64.b64decode(content)
        else:
            image_base64 = self.placeholder(image='placeholder.png')  # could return (contenttype, content) in master
            headers = self.force_contenttype(headers, contenttype='image/png')

        headers.append(('Content-Length', len(image_base64)))
        response = request.make_response(image_base64, headers)
        response.status_code = status
        return response
    
    @http.route(['/fabula/content/<int:id>'], type='http', auth="public")
    def fabula_content_common(self, model='ir.attachment', id=None, field='datas', filename=None, filename_field='datas_fname', mimetype=None, download=None, token=None):
        status, headers, content = request.registry['ir.http'].publlic_binary_content(model=model, id=id, field=field, filename=filename, filename_field=filename_field, download=download, mimetype=mimetype)
        
        if status == 304:
            response = werkzeug.wrappers.Response(status=status, headers=headers)
        elif status == 301:
            return werkzeug.utils.redirect(content, code=301)
        elif status != 200:
            response = request.not_found()
        else:
            content_base64 = base64.b64decode(content)
            headers.append(('Content-Length', len(content_base64)))
            response = request.make_response(content_base64, headers)
        if token:
            response.set_cookie('fileToken', token)
        return response