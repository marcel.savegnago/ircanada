# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
   # App information
   
    'name': 'Fabula Live Chat for Odoo Website',
    'version': '11.0.1',
    'category': 'Website',
    'summary': 'A modern Odoo Live Chat tool lets you chat with your customers and give them real-time support round-the-clock.',
    
    # Dependencies
    
    'depends': ['document', 'website_livechat'],

   # Views

	'data': [
        'views/assets.xml',
        'views/mail_channel_view.xml',
        'views/im_livechat_channel_view.xml',
        'views/fabula_livechat_channel_templates.xml',
        'data/mail_shortcode_data.xml',
        'data/im_livechat_channel_data.xml',
    ],
    'qweb': [
        '/fabula_livechat/static/src/xml/fabula_livechat.xml',
        '/fabula_livechat/static/src/xml/chat_thread.xml',
    ],
    
    
    # Odoo Store Specific
    
    'images': ['static/description/Live_Chat.jpg'],

    
    
     # Author

    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
   
   
    # Technical 
   
    'installable': True,
    'application': True,
    'auto_install': False,
    'live_test_url' : 'https://shop.emiprotechnologies.com/fabula-live-chat-for-odoo-website.html#request-a-demo', 
    'price': '199' ,
    'currency': 'EUR',
   
      
   
    
}


