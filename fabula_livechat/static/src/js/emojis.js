odoo.define('fabula_livechat.emojis', function (require) {
"use strict";

var session = require('web.session');

var emojis = [];
var emoji_substitutions = {};
var emoji_unicodes = {};

var chat_manager = {
    get_emojis: function() {
        return emojis;
    },
    get_emoji_substitutions: function () {
        return emoji_substitutions;
    },
    get_emoji_unicode: function () {
        return emoji_unicodes;
    },
};

// Initialization
// ---------------------------------------------------------------------------------
function init () {

    return session.rpc('/fabula_livechat/emojis').then(function (result) {
    	// Shortcodes: canned responses and emojis
        _.each(result.shortcodes, function (s) {
            if (s.shortcode_type === 'image') {
            	emojis.push(_.pick(s, ['id', 'source', 'unicode_source', 'substitution', 'description']));
                emoji_substitutions[_.escape(s.source)] = s.substitution;
                if (s.unicode_source) {
                    emoji_substitutions[_.escape(s.unicode_source)] = s.substitution;
                    emoji_unicodes[_.escape(s.source)] = s.unicode_source;
                }
            }
        });
    });
}

chat_manager.is_ready = init();
return chat_manager;
});
