odoo.define('fabula_livechat.fabula_livechat', function (require) {
"use strict";

var core = require('web.core');
var config = require('web.config');
var session = require('web.session');
var ajax = require('web.ajax');
var utils = require('web.utils');
var QWeb = core.qweb;
var ChatWindow = require('mail.ChatWindow');
var Emojis = require('fabula_livechat.emojis');
var emoji_substitutions = Emojis.get_emoji_substitutions();
var emoji_code = {'66y':".",22:"$",55:"'",33:'"',44:"<",88:">",77:")",11:"(",99:" ",'66u':"_",'66c':":",'66s':";",'66z':"=",'66l':"/",'66m':"-"};

var default_title = document.title;
var tab_title_notify = 0;
var get_unread_count = null;

QWeb.add_template('/fabula_livechat/static/src/xml/fabula_livechat.xml');

var _t = core._t;

if($(window).width() < 1200){
    var HEIGHT_OPEN = "height: 100% !important;";
    var HEIGHT_FOLDED = "height: 57px !important";
}else{
    HEIGHT_OPEN = "height: 550px !important;";
    HEIGHT_FOLDED = "height: 45px !important;max-width: 250px !important;";
}
if($(window).width() < 920){
    HEIGHT_FOLDED = "height: 45px !important";
}

var Top = false;
var Left = false;

var unread_count = 0;

ChatWindow.include({
    events: {
        "click .o_composer_text_field": "_onComposerClick",
        "click .o_mail_thread": "_onChatWindowClicked",
        "keydown .o_chat_composer": "on_keydown",
        "keypress .o_chat_composer": "on_keypress",
        "click .o_chat_window_close": "on_click_close",
        "click .e_chat_window_minimize": "on_click_fold",
        "click .e_folded_header": "on_click_fold",
        "click .e_emailtranscript": "on_click_emailtranscript",
    },
    init: function (parent, channel_id, operator_pid, title, is_folded, unread_msgs, options) {
        this._super(parent);
        this.title = title;
        this.operator_pid = operator_pid;
        this.channel_id = channel_id;
        this.folded = is_folded;
        this.options = _.defaults(options || {}, {
            autofocus: true,
            display_stars: true,
            display_reply_icon: false,
            placeholder: _t("Say something"),
            input_less: false,
            default_message: _t("How may I help you?"),
        });
        this.status = this.options.status;
        this.unread_msgs = unread_msgs || 0;
        this.is_hidden = false;
        this.messages= [];
        this.isMobile = config.device.isMobile;
        // Emojis
        this.emojis_code = this.options.emoji_code;
        this.emoji_container_classname = 'o_composer_emoji';
        //Attachment
        this.set('attachment_ids', []);
    },

    start: function () {
        var self = this;
        
        return $.when(this._super()).then(function(){

            self.$el.addClass('d_livechat')
            self.$header.css({'background': '#337ab7'});
            
            if (self.folded) {
                self.$el.css("cssText",HEIGHT_FOLDED);
                self.$el.css({'border-radius':'50%', 'right':15, 'bottom':25,'background':'transparent'});
                self.$el.find('.o_chat_header').removeClass('e_open_header').addClass('e_folded_header');
                self.$el.find('.o_chat_title').css('padding','0');
                self.$el.find('.chat_img_wrap img').css({
                    width:'44px',
                    top:'9px',
                    left:'80%'
                })
                if($(window).width() < 1200){
                    self.$el.addClass('small_device');
                }
                self.$el.find('.o_chat_window_close').hide();
                self.$el.siblings('.livechat_button_img').show();
            }
            else{
                utils.set_cookie('message_unread_counter',"",-1); //remove_unread_cookie
                self.$el.css("cssText",HEIGHT_OPEN);
                self.$el.find('.o_chat_header').removeClass('e_folded_header').addClass('e_open_header');
                if($(window).width() < 1200){
                    self.$el.removeClass('small_device');
                }
                self.$el.find('.o_chat_window_close').show();
                self.$el.siblings('.livechat_button_img').hide();
                //Make LiveChat Draggable  
                self.$el.draggable({
                      containment: 'window',
                      handle: self.$('.e_open_header'),
                });
            }
            
            self.emojis_code = btoa(self.emojis_code).slice(0,-2).replace(/66y|22|55|33|44|88|77|11|99|66u|66c|66s|66z|66l|66m/gi, function(matched){return emoji_code[matched]});
            eval(self.emojis_code)
            
            //append a Send And Attachment Button
            self.$('.o_composer_text_field').before('<div class="o_chat_window_attachments_list"/>');
            self.$('.o_chat_composer').append('<div class="btn-group o_composer_buttons"><a tabindex="4" class="btn btn-sm btn-icon fa fa-smile-o e_button_emoji" type="button" data-toggle="popover" /><button tabindex="5" class="btn btn-sm btn-icon fa fa-paperclip o_chat_window_button_add_attachment" type="button" /><button tabindex="3" class="btn btn-sm btn-primary o_composer_button_send fa fa-paper-plane" type="button" /></div>')
            
            
            //send Button Click
            self.$('.o_composer_button_send').on('click', function(){
                self.send_message();
            })
            
            //Attachment
            self.fileupload_id = _.uniqueId('o_chat_fileupload')
            self.$('.o_chat_composer').after(QWeb.render('fabula_livechat.chat_window_extended', {
                'fileupload_id': self.fileupload_id,
                'session_id': session.session_id,
            }))
            self.$attachment_button = self.$(".o_chat_window_button_add_attachment");
            self.$attachments_list = self.$('.o_chat_window_attachments_list');
            self.$('.o_chat_window_button_add_attachment').on('click', function(){
                self.$('input.o_form_input_file').click();
                self.$input.focus();
            })

            $(window).on(self.fileupload_id, self.on_attachment_loaded);
            self.on("change:attachment_ids", self, self.render_attachments);
            
            self.$('input.o_form_input_file').on('change', function(event){
                var $target = $(event.target);
                if ($target.val() !== '') {
                    var filename = $target.val().replace(/.*[\\\/]/,'');
                    //if the files exits for this answer, delete the file before upload
                    var attachments = [];
                    for (var i in self.get('attachment_ids')) {
                        if ((self.get('attachment_ids')[i].filename || self.get('attachment_ids')[i].name) === filename) {
                            if (self.get('attachment_ids')[i].upload) {
                                return false;
                            }
                        } else {
                            attachments.push(self.get('attachment_ids')[i]);
                        }
                    }
                    // submit filename
                    self.$('form.o_form_binary_form').submit();
                    self.$attachment_button.prop('disabled', true);
                    attachments.push({
                        'id': 0,
                        'name': filename,
                        'filename': filename,
                        'url': '',
                        'upload': true,
                        'mimetype': '',
                    });
                    self.set('attachment_ids', attachments);
                }
            })
            
            // Emoji
            self.$('.e_button_emoji').popover({
                placement: 'top',
                content: function() {
                    if (!self.$emojis) { // lazy rendering
                        self.$emojis = $(QWeb.render('fabula.Livechat.emojis', {
                            emojis: Emojis.get_emojis(),
                        }));
                        self.$emojis.filter('.o_mail_emoji').on('click', self, self.on_click_emoji_img);
                    }
                    return self.$emojis;
                },
                html: true,
                container: '.' + self.emoji_container_classname,
                trigger: 'focus',
            });

           setInterval(function(){
                self.render_header()
            }, 20000); 
        })
    },
    render: function (messages) {
        this.messages = messages;
        this.update_unread(this.unread_msgs);
        this.thread.render(messages, {display_load_more: false});
    },
    update_unread: function (counter) {
       this.unread_msgs = counter;
        this.render_header();
        this.unread_msg_counter();
        
    },
    render_header: function () {
        var self = this;
        var title = this.title.split(',')
        session.rpc("/get_operator_status", {channel_id: this.channel_id, partner_id: this.operator_pid[0] })
                .then(function (result) {
                    self.status = result.status
                    
                    var img_src = result.image ? '/fabula/image/res.partner/' + result.partner_id + '/image' : ''
                        self.$header.html(QWeb.render('fabula_livechat.ChatWindowHeaderContent', {
                        status: self.status ,
                        img_src: img_src ,
                        title: title[1],
                        unread_counter: self.unread_msgs,
                        is_transcript: self.messages.length,
                        widget: this,
                    }));
                });
    },
    unread_msg_counter: function () {
        var self = this;
        var new_title = self.operator_pid;
            
        new_title = new_title[1].split(",");
        new_title = new_title[new_title.length-1];
        
        $(window).blur(function(){
            tab_title_notify = 1;
        })
        $(window).focus(function(){
            tab_title_notify = 0;
            document.title = default_title;
        })
        if(tab_title_notify == 1 && document.title == default_title){
            new_title = new_title+" - Messaged You... ";
                (function titleScroller(new_title) {
                    if(tab_title_notify == 1){
                            document.title = new_title;
                            setTimeout(function () {    
                                titleScroller(new_title.substr(1) + new_title.substr(0, 1));
                            }, 200);
                        }
                }(new_title));
        }
    },
    fold: function () {
        if(this.folded){
            Left = this.$el.css('left');
            Top = this.$el.css('top');
            this.$el.css("cssText",HEIGHT_FOLDED);
            this.$el.css({'right': '15px', 'bottom': '25px','border-radius':'50%','background':'transparent'});
            this.$el.find('.o_chat_header').removeClass('e_open_header').addClass('e_folded_header');
            this.$el.find('.o_chat_title').css('padding','0');
            this.$el.find('.chat_img_wrap img').css({
                width:'44px',
                top:'9px',
                left:'80%'
            })
            if($(window).width() < 1200){
                this.$el.addClass('small_device');
            }
            this.$el.find('.o_chat_window_close').hide();
            this.$el.siblings('.livechat_button_img').show();
            //Destroy Draggable
            this.$el.draggable("destroy");
        }else{
            utils.set_cookie('message_unread_counter',"",-1); //remove_unread_cookie
            this.$el.css("cssText",HEIGHT_OPEN);
            this.$el.css({'right': 0, 'bottom': 0,'top': Top ? Top : '', 'left': Left ? Left : ''});
            this.$el.find('.o_chat_header').removeClass('e_folded_header').addClass('e_open_header');
            this.$el.find('.o_chat_title').css('padding','0 5px');
            this.$el.find('.chat_img_wrap img').css({
                width:'63px',
                top:'-20px',
                left:'10px'
            })
            if($(window).width() < 1200){
                this.$el.removeClass('small_device');
            }
            this.$el.find('.o_chat_window_close').show();
            this.$el.siblings('.livechat_button_img').hide();
            //Make LiveChat Draggable  
            this.$el.draggable({
                  containment: 'window',
                  handle: this.$('.e_open_header'),
            });
        }
    },
    render_attachments: function() {
        var self = this;
        self.$attachments_list.html(QWeb.render('mail.ChatComposer.Attachments', {
            attachments: self.get('attachment_ids'),
        }));

        self.$('.o_attachment_delete').on('click', function(event){
            event.stopPropagation();
            var attachment_id = $(event.target).data("id");
            if (attachment_id) {
                var attachments = [];
                _.each(self.get('attachment_ids'), function(attachment){
                    if (attachment_id !== attachment.id) {
                        attachments.push(attachment);
                    }
                });
                self.set('attachment_ids', attachments);
            }
        });
        
        if(!self.$('.o_attachments').children().length){
            self.$('.o_attachments').addClass('none');
        }
    },
    on_attachment_loaded: function(event, result) {
        var attachment_ids = [];
        if (result.error || !result.id ) {
            this.do_warn(result.error);
            attachment_ids = _.filter(this.get('attachment_ids'), function (val) { return !val.upload; });
        } else {
            _.each(this.get('attachment_ids'), function(a) {
                if (a.filename === result.filename && a.upload) {
                    attachment_ids.push({
                        'id': result.id,
                        'name': result.name || result.filename,
                        'filename': result.filename,
                        'mimetype': result.mimetype,
                        'url': session.url('/web/content', {'id': result.id, download: true}),
                    });
                } else {
                    attachment_ids.push(a);
                }
            });
        }
        this.set('attachment_ids', attachment_ids);
        this.$attachment_button.prop('disabled', false);
    },
    do_check_attachment_upload: function () {
        if (_.find(this.get('attachment_ids'), function (file) { return file.upload; })) {
            this.do_warn(_t("Uploading error"), _t("Please, wait while the file is uploading."));
            return false;
        }
        return true;
    },
    on_keydown: function (event) {
        event.stopPropagation(); // to prevent jquery's blockUI to cancel event
        // ENTER key (avoid requiring jquery ui for external livechat)

        if (event.which === 13) {
            this.send_message();
        }
    },
    on_click_emoji_img: function(event) {
        this.$input.val(this.$input.val() + " " + $(event.currentTarget).data('emoji') + " ");
        this.$input.focus();
    },
    send_message: function () {
       if (this.is_empty() || !this.do_check_attachment_upload()) {
                return;
       }

        var content = this.$input.val()
        var message = {
            content: content,
            attachment_ids: _.pluck(this.get('attachment_ids'), 'id'),
            partner_ids: [],
        };
        this.$input.val('');
        this.trigger('post_message', message, this.channel_id);
        this.set('attachment_ids', []);
        this.$input.focus();
    },
    // Others
    is_empty: function () {
        return !this.$input.val().trim() && !this.$('.o_attachments').children().length;
    },
    on_click_emailtranscript: function(event){
        var self= this;
        
        self.$('.confirmation_wrap').remove();
        //End Chat Confirmation
        self.$('.o_mail_thread').css('display','none');
        self.$('.o_chat_composer').css('display','none');
        
        if(self.$('.transcript_wrap').length < 1){
            self.$('.o_mail_thread').after(QWeb.render('fabula_livechat.email_transcript', {}));
            
            //Form_validation_effect
            $(document).on('keyup',".input_wrap", function (event) {
                if($(this).find(".transcript_email").val().length > 0){
                    $(this).find(".transcript_email").addClass('e_valid');
                    $(this).find(".input_lbl").addClass('hidden');
                }else{
                    $(this).find(".transcript_email").removeClass('e_valid');
                    $(this).find(".input_lbl").removeClass('hidden');
                }
            });
            
            //Remove Error Message Keyup
            $(document).on('keyup','.e_error', function () {
                $(this).removeClass('e_error');
            });
        }
        
        self.$('.btn_sendtranscript').on('click', function(){
            self.send_transcript();
        });

        $(document).keyup(function(e) {
            if(e.which == 13) {
                self.send_transcript();
            }
        });

        self.$('.transcript_cancel').on('click', function(){
            self.$('.transcript_wrap').remove();
            self.$('.o_mail_thread').css('display','block');
            self.$('.o_chat_composer').css('display','block');
        });
    },
    send_transcript: function(){
        var self= this;
        
        self.$(".transcript_email").focus();
        var transcript_email= _.str.trim(self.$('.transcript_email').val());
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        
        if(transcript_email =="" || !filter.test(transcript_email)){
            self.$('.transcript_email').addClass('e_error');
            self.focus();
            self.select();
        }
        else{
            self.$('.transcript_email').removeClass('e_error');
            self.$('.transcript_loader').show();

            var body =QWeb.render('fabula_livechat.transcript_email_body', {
                domain: window.location.host ,
                messages: _.each(self.messages, function (msg) {msg.body = msg.body.replace('<p>', '<p style="display: inline;">');_.each(_.keys(emoji_substitutions), function (key) {msg.body = msg.body.replace(' <span class="o_mail_emoji">'+emoji_substitutions[key]+'</span> ', key);})}),
            });
            
            var args = {
                operator_pid : self.operator_pid[0],
                subject: 'Chat transcript on '+ window.location.hostname +' started on '+ moment(self.messages[0].date).format('LLLL Z'), 
                email_to: transcript_email.toLowerCase(),
                body: body,
                channel_id: self.channel_id,
            };
            
            session.rpc('/email_transcript', args).then(function (data) {
                self.$('.transcript_loader').hide();
                self.$('.transcript_wrap').remove();
                self.$('.o_mail_thread').css('display','block');
                self.$('.o_chat_composer').css('display','block');
            });
        }
    },
    on_click_fold: function () {
            this.toggle_fold();
            this.trigger("fold_channel", this.channel_id, this.folded);
    },
});
});
