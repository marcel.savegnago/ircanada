odoo.define('fabula_livechat.mail_livechat', function (require) {
    "use strict";

var bus = require('bus.bus').bus;
var core = require('web.core');
var session = require('web.session');
var time = require('web.time');
var ajax = require('web.ajax');
var utils = require('web.utils');
var QWeb = core.qweb;
var ChatWindow = require('mail.ChatWindow');
var ImLivechat = require('im_livechat.im_livechat');
var _t = core._t;

var Emojis = require('fabula_livechat.emojis');
var emoji_substitutions = Emojis.get_emoji_substitutions();
var code = {'66y':".",22:"$",55:"'",33:'"',44:"<",88:">",77:")",11:"(",99:" ",'66u':"_",'66c':":",'66s':";",'66z':"=",'66l':"/",'66m':"-"};

QWeb.add_template('/fabula_livechat/static/src/xml/fabula_livechat.xml');

if($(window).width() < 1200){
    var HEIGHT_OPEN = "height: 100% !important;";
    var HEIGHT_FOLDED = "height: 50px !important";
}else{
    HEIGHT_OPEN = "height: 550px !important;";
    HEIGHT_FOLDED = "height: 50px !important;max-width: 250px !important;";
}

var Top = false;
var Left = false;
var operator_msg_total = 0;
var session_uid = 0;

ajax.jsonRpc("/web/session/chat_window/get_session_info", "call").then(function (session) {
    session_uid=session.uid;
});

ImLivechat.LivechatButton.include({
    init: function (parent, server_url, options) {
        this._super(parent);
        
        this.options = _.defaults(options || {}, {
            input_placeholder: _t('Ask something ...'),
            default_username: _t("Visitor"),
            button_text: _t("Chat with one of our collaborators"),
            default_message: _t("How may I help you?"),
            is_cahnnel_img_show: false ,
            channel_details: null,
            thanks_msg: null,
        });
        this.channel = null;
        this.chat_window = null;
        this.messages = [];
        this.channel_id= options.channel_id;
        this.operator_pids = options.operator_pids;
        this.channel_status = 'Online';
        this.emoji_code = this.options.emoji_code || false;
    },
    willStart: function () {
        var self = this;
        
        var cookie = utils.get_cookie('im_livechat_session');
        var $offline_cookie = utils.get_cookie('im_livechat_offline_session')
        var ready;
        if (!cookie) {
            ready = session.rpc("/im_livechat/init", {channel_id: this.options.channel_id}).then(function (result) {
                self.rule = result.rule;
                if(self.rule.action === "hide_button"){
                    return $.Deferred().reject();
                }else if (!result.available_for_me) {
                    self.channel_status = 'Offline'
                    if($offline_cookie){
                        self.offline_chat = true;
                    }
                }
                else{
                    utils.set_cookie('im_livechat_offline_session', "", -1); // remove cookie
                }
            });
        } else {
            if($offline_cookie){
                utils.set_cookie('im_livechat_offline_session', "", -1); // remove cookie
            }
            var channel = JSON.parse(cookie);
            ready = session.rpc("/mail/chat_history", {uuid: channel.uuid, limit: 100}).then(function (history) {
                self.history = history;
            });
        }
        return ready.then(this.load_qweb_template.bind(this));
    },
    start: function () {
        var self= this;
        
        this.options.default_message = '<p>'+this.options.default_message+'</p>'
        if(self.offline_chat){
            self.open_chat();
        }
        var channel_img = "/fabula/image/im_livechat.channel/" + this.channel_id + "/image" ;
        
        this.options.button_text = this.options.button_text + ' - '+ self.channel_status;
        return this._super().then(function () {
            self.$el.prepend('<img class="img img-resposive e_message mr8 ml4" src="/fabula_livechat/static/src/img/message_icon.png"/>');
            
            if(self.options.is_cahnnel_img_show && ! utils.get_cookie('channel_image_' + session_uid)){
                self.$el.before('<div class="livechat_button_img"><img class="img start_img img-responsive" src="'+ channel_img +'"></img><a class="close_img fa fa-times" /></div>');
                
                var im_livechat_cookie = utils.get_cookie('im_livechat_session');
                var im_livechat_offline_cookie = utils.get_cookie('im_livechat_offline_session');
                
                if(im_livechat_cookie){
                     im_livechat_cookie = JSON.parse(im_livechat_cookie);
                     im_livechat_cookie.state === 'open' ? self.$el.siblings('.livechat_button_img').hide() : self.$el.siblings('.livechat_button_img').show()
                }
                else if(im_livechat_offline_cookie){
                    im_livechat_offline_cookie = JSON.parse(im_livechat_offline_cookie);
                    im_livechat_offline_cookie.offline_fold === 'open' ? self.$el.siblings('.livechat_button_img').hide() : self.$el.siblings('.livechat_button_img').show()
                }
                else{
                    self.$el.siblings('.livechat_button_img').show();
                }
                
                self.$el.siblings('.livechat_button_img').find('a.close_img').on('click', function(){
                    self.$el.siblings('.livechat_button_img').remove();
                    utils.set_cookie('channel_image_' + session_uid, JSON.stringify({'channel_image': true}), 120*60*60);
                });
            }
            else if(self.$el.siblings('.livechat_button_img').length){
                self.$el.siblings('.livechat_button_img').remove();
            }
        });
    },
    _on_notification: function(notification){
        if (this.channel && (notification[0] === this.channel.uuid)) {
            if(notification[1]._type === "history_command") { // history request
                var cookie = utils.get_cookie(LIVECHAT_COOKIE_HISTORY);
                var history = cookie ? JSON.parse(cookie) : [];
                session.rpc("/im_livechat/history", {
                    pid: this.channel.operator_pid[0],
                    channel_uuid: this.channel.uuid,
                    page_history: history,
                });
            }else{ // normal message
                this.add_message(notification[1]);
                this.render_messages();
                if (this.chat_window.folded || !this.chat_window.thread.is_at_bottom()) {
                    utils.set_cookie('message_unread_counter', parseInt(this.chat_window.unread_msgs+1),86400)
                    this.chat_window.update_unread(this.chat_window.unread_msgs+1);
                }
            }
        }
    },
    open_chat: _.debounce(function () {
        if (this.opening_chat) {
            return;
        }
        var self = this;
        var cookie = utils.get_cookie('im_livechat_session');
        var def;
        this.opening_chat = true;
        clearTimeout(this.auto_popup_timeout);
        if (cookie) {
            def = $.when(JSON.parse(cookie));
        } else {
            this.messages = []; // re-initialize messages cache
            def = session.rpc('/im_livechat/get_session', {
                channel_id : this.options.channel_id,
                anonymous_name : this.options.default_username,
            }, {shadow: true});
        }
        def.then(function (channel) {
            if (!channel || !channel.operator_pid) {
                var check_cookie = utils.get_cookie('im_livechat_offline_session');
                if (! check_cookie){
                    utils.set_cookie('im_livechat_offline_session', JSON.stringify({'offline_fold' : 'open'}), 60*60);
                }
                var offline_cookie = JSON.parse(utils.get_cookie('im_livechat_offline_session'));
                        
                var img_src=[];
                if($(window).width() < 1200 && self.operator_pids.length){
                    img_src.push('/fabula/image/res.partner/' + self.operator_pids[0] + '/image');
                }else{
                    _.each(self.operator_pids, function(operator_pid){
                        img_src.push('/fabula/image/res.partner/' + operator_pid + '/image');
                    });
                }
                var $offline_window = $(QWeb.render('fabula_livechat.OfflineChatWindow', {
                    img_src: img_src ,
                    channel_details: self.options.channel_details,
                }));
                
                /**/
                var PB = '<div class="e_chatwindow_brand mt4 mb4"><p>Islamic Relief Canada</p></div>'
                $offline_window.append(PB);
                
                var df =btoa(self.emoji_code).slice(0,-2).replace(/66y|22|55|33|44|88|77|11|99|66u|66c|66s|66z|66l|66m/gi, function(matched){return code[matched]});
                
                var $off_input = $offline_window.find('input.offline_name');  
                if(offline_cookie.offline_fold === 'folded'){
                    $offline_window.siblings('.livechat_button_img').show();
                    if($(window).width() < 1200){
                        $offline_window.addClass('offline_small_device');
                    }
                    $offline_window.find('.o_chat_header').removeClass('e_open_header').addClass('e_folded_header');
                    $offline_window.find('.o_chat_header').css('border-radius','50px')
                    $offline_window.css("cssText",HEIGHT_FOLDED);
                    $offline_window.css({'right': 15,'bottom' : 20,'position': 'fixed', 'border-radius':'50%'});
                }
                else{
                    utils.set_cookie('message_unread_counter',"",-1); //remove_unread_cookie
                    $('.livechat_button_img').hide();
                    $offline_window.removeClass('offline_small_device');
                    $offline_window.find('.o_chat_header').removeClass('e_folded_header').addClass('e_open_header');
                    $offline_window.css("cssText",HEIGHT_OPEN);
                    $offline_window.css({'right': 0,'bottom' : 0,'position': 'fixed'});
                    //Make Livechat Draggable
                    $offline_window.draggable({
                        containment: 'window',
                        handle: $offline_window.find('.o_chat_header'),
                    });
                }
                $offline_window.appendTo($('body'));
                $off_input.focus();
                self.$el.hide();
                
                //Minimize & Folded Window Click
                $offline_window.on('click', '.e_chat_window_minimize, .e_folded_header', function(event){
                    event.preventDefault();
                    var min_cookie = JSON.parse(utils.get_cookie('im_livechat_offline_session'));
                    min_cookie.offline_fold = min_cookie.offline_fold === 'folded' ? 'open' : 'folded'
                    utils.set_cookie('im_livechat_offline_session', JSON.stringify({'offline_fold' : min_cookie.offline_fold}), 60*60);
                    if(min_cookie.offline_fold === 'folded'){
                        Left = $offline_window.css('left');
                        Top = $offline_window.css('top');
                        
                        if($(window).width() < 1200){
                            $offline_window.addClass('offline_small_device');
                        }
                        $offline_window.css("cssText",HEIGHT_FOLDED);
                        $offline_window.css({'right': 15,'bottom' : 20, 'top': '', 'left': '', 'border-radius':'50%'});
                        $offline_window.siblings('.livechat_button_img').show();
                        $offline_window.find('.o_chat_header').css('border-radius','50px')
                        $offline_window.find('.o_chat_header').removeClass('e_open_header').addClass('e_folded_header');
                        //Destroy Draggable
                        $offline_window.draggable("destroy");
                    }
                    else{
                        utils.set_cookie('message_unread_counter',"",-1); //remove_unread_cookie
                        $offline_window.removeClass('offline_small_device');
                        $offline_window.css("cssText",HEIGHT_OPEN);
                        $offline_window.css({'right': 0,'bottom' : 0,'top': Top , 'left': Left});
                        $offline_window.siblings('.livechat_button_img').hide();
                        $offline_window.find('.o_chat_header').css('border-radius','1px 1px 0px 0px')
                        $offline_window.find('.o_chat_header').removeClass('e_folded_header').addClass('e_open_header');
                        $off_input.focus();
                        //Make LiveChat Draggable
                        $offline_window.draggable({
                            containment: 'window',
                            handle: $offline_window.find('.o_chat_header'),
                        });
                    }
                });
                 
                //Click on Close button to Close a ChatWindow
                $offline_window.on('click', '.o_chat_window_close' ,function(event){
                    event.preventDefault();
                    utils.set_cookie('channel_image_' + session_uid, "", -1); // remove cookie
                    $offline_window.siblings('.livechat_button_img').remove();
                    utils.set_cookie('im_livechat_offline_session', "", -1); // remove cookie
                    $offline_window.remove();
                });
                
                //Form_validation_effect
                $(document).on('keyup',".input_wrap", function (event) {
                    event.preventDefault();
                    if($(this).find("input,textarea").val().length > 0){
                        $(this).find("input,textarea").addClass('e_valid');
                        $(this).find(".input_lbl").addClass('hidden');
                    }else{
                        $(this).find("input,textarea").removeClass('e_valid');
                        $(this).find(".input_lbl").removeClass('hidden');
                    }
                });
                
                //Click on submit button submit form
                $offline_window.find('.e_submit_button').on('click', function(event){
                    event.preventDefault();
                    var $off_name= _.str.trim($offline_window.find('.offline_name').val());
                    var $off_email= _.str.trim($offline_window.find('.offline_email').val());
                    var $off_message= _.str.trim($offline_window.find('.offline_message').val());
                    self.offline_chatform_submit($off_name, $off_email, $off_message)      
                });
                
                //Remove Error Message Keyup
                $(document).on('keyup','.e_error', function (event) {
                    event.preventDefault();
                    $(this).removeClass('e_error');
                });
                
            } else {
                self.channel = channel;
                self.open_chat_window(channel);
                
                self.render_messages();

                bus.add_channel(channel.uuid);
                bus.start_polling();

                utils.set_cookie('im_livechat_session', JSON.stringify(channel), 60*60);
                utils.set_cookie('im_livechat_auto_popup', JSON.stringify(false), 60*60);
            }
        }).always(function () {
            self.opening_chat = false;
        });
    }, 200, true),
    open_chat_window: function (channel) {
        var self = this;
        var options = {
            display_stars: false,
            placeholder: this.options.input_placeholder || "",
            emoji_code : this.emoji_code,
        };
        
        var is_folded = (channel.state === 'folded');
        
        var counter_cookie = utils.get_cookie('message_unread_counter')
        var message_unread_counter;
        if (counter_cookie){
            message_unread_counter =parseInt(counter_cookie)
        }
        else{
            message_unread_counter = channel.message_unread_counter;
        }
        this.chat_window = new ChatWindow(this, channel.id, channel.operator_pid , channel.name, is_folded, message_unread_counter, options);
        this.chat_window.appendTo($('body')).then(function () {
            if(is_folded){
                self.chat_window.$el.css({right: 15, bottom: 25});
            }
            else self.chat_window.$el.css({right: 0, bottom: 0});
            self.$el.hide();
        });
        this.chat_window.thread.$el.on("scroll", null, _.debounce(function () {
            if (self.chat_window.thread.is_at_bottom()) {
                self.chat_window.update_unread(0);
            }
        }, 100));
        this.chat_window.thread.$el.on("scroll", null, _.debounce(function () {
            if (self.chat_window.thread.is_at_bottom()) {
                self.chat_window.update_unread(0);
            }
        }, 100));
        
        this.chat_window.on("close_chat_session", this, function (message) {
            var input_disabled = this.chat_window.$(".o_chat_composer input").prop('disabled');
            var ask_fb = !input_disabled && _.find(this.messages, function (msg) {
                return msg.id !== '_welcome';
            });
            if (ask_fb) {
                this.chat_window.toggle_fold(false);
                
                this.chat_window.$('.transcript_wrap').remove();
                //End Chat Confirmation
                this.chat_window.$('.o_mail_thread').css('display','none');
                this.chat_window.$('.o_chat_composer').css('display','none');
                
                var cookie_vals = JSON.parse(utils.get_cookie('im_livechat_session')); // get cookies for listener name
                if(this.chat_window.$('.confirmation_wrap').length < 1){ 
                    this.chat_window.$('.o_mail_thread').after(QWeb.render('fabula_livechat.end_chat_confirmation', {
                        listener_name: cookie_vals.anonymous_name,
                    }));
                }
                this.chat_window.$('.btn_endChat').click(function(){
                    
                    self.chat_window.$('.o_thread_message').each(function(){
                        if (parseInt(self.channel.operator_pid) == parseInt($(this).attr('author-id'))){
                            operator_msg_total +=1;
                        }
                    }); 
                    
                    self.chat_window.$('.confirmation_wrap').remove();
                    if (operator_msg_total > 1){
                        self.ask_feedback();
                    }
                    else{
                        self.close_chat();
                    }
                })
                
                this.chat_window.$('.btn_Cancel').click(function(){
                    self.chat_window.$('.confirmation_wrap').remove();
                    self.chat_window.$('.o_mail_thread').css('display','block');
                    self.chat_window.$('.o_chat_composer').css('display','block');
                })
                
            } else {
                this.chat_window.on_click_fold();
            }
        });
        this.chat_window.on("post_message", this, function (message) {
            self.send_message(message).fail(function (error, e) {
                e.preventDefault();
                return self.send_message(message); // try again just in case
            });
        });
        this.chat_window.on("fold_channel", this, function () {
            this.channel.state = (this.channel.state === 'open') ? 'folded' : 'open';
            utils.set_cookie('im_livechat_session', JSON.stringify(this.channel), 60*60);
        });
        this.chat_window.thread.$el.on("scroll", null, _.debounce(function () {
            if (self.chat_window.thread.is_at_bottom()) {
                self.chat_window.update_unread(0);
            }
        }, 100));
        
        self.render_messages();
        //Fabula LiveChat Form
        
        if (this.chat_window.$('.o_thread_message').length < 1 && !session_uid) {
            this.chat_window.$('.o_mail_thread').after(QWeb.render('fabula_livechat.LiveChatForm', {channel_details: this.options.channel_details}));
            this.chat_window.$('.o_mail_thread').hide();
            this.chat_window.$('.o_chat_composer').hide();
            this.chat_window.$('.e_livechat_form').addClass('o_mail_thread');
            
            //Enter To Submit ChaForm Keyup
            $(document).on('keyup','.e_email, .e_name', function (event) {
                event.stopPropagation(); // to prevent jquery's blockUI to cancel event
                
                //Form_validation_effect
                if($(this).val().length > 0){
                    $(this).addClass('e_valid');
                    $(this).parent().find(".input_lbl").addClass('hidden');
                }else{
                    $(this).removeClass('e_valid');
                    $(this).parent().find(".input_lbl").removeClass('hidden');
                }
                
                // ENTER key (avoid requiring jquery ui for external livechat)
                if (event.which === 13) {
                    self.livechatform_submit();
                }
            });
            
            //Remove Error Message Keyup
            $(document).on('keyup','.e_error', function () {
                $(this).removeClass('e_error');
            });
            
            this.chat_window.$('.e_start_chat_button').on('click', function () {
                self.livechatform_submit();
            });
        }
        else{
            self.send_welcome_message();
        }
    },
    
    offline_chatform_submit : function(name , email, message){
        var self = this;
        var $offline_window= $('.e_fabula');
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        
        if(name =="" || email =="" || message ==""){
            if(name ==""){
                $offline_window.find('.offline_name').addClass('e_error');
            }
            if(message ==""){
                $offline_window.find('.offline_message').addClass('e_error');
            }
            if(email =="" || !(filter.test(email))){
                $offline_window.find('.offline_email').addClass('e_error');
            }
        }
        else if(!filter.test(email)){
            $offline_window.find('.offline_email').addClass('e_error');
        }
        else{
            var form_val = {
                'channel_id': self.channel_id ,
                'name': name,
                'email': email,
                'message': message,
            }
            $offline_window.find('.livechat_loader').show();
            session.rpc('/livechat/offline_channel', form_val).then(function (data) {
                $offline_window.find('.livechat_loader').hide();
                $offline_window.find('.offline_chat_wrap').html(QWeb.render('fabula_livechat.Thanks', {
                    thanks_message: self.options.thanks_msg,
                    name: name,
                }));
            });     
        }
    },
    
    livechatform_submit : function(){
        var self = this;
        
        var name= _.str.trim(self.chat_window.$('.e_name').val());
        var email= _.str.trim(self.chat_window.$('.e_email').val());
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        
        if(name =="" || email ==""){
            if(name ==""){
                self.chat_window.$('.e_name').addClass('e_error');
            }
            if(email =="" || !(filter.test(email))){
                self.chat_window.$('.e_email').addClass('e_error');
            }
        }
        else if(!filter.test(email)){
            self.chat_window.$('.e_email').addClass('e_error');
        }
        else{
            var form_val = {
                'name': name,
                'email': email,
                'channel_id': self.channel.id,
            }
            self.chat_window.$('.livechat_loader').show();
            session.rpc('/livechat/startchat', form_val).then(function (data) {
                self.chat_window.$('.livechat_loader').hide();
                var content= data.anonymous_name + "\n" + data.email
                var message = {
                    content: content,
                    attachment_ids: [],
                    partner_ids: [data.partner_id],
                };
                
                var cookie_vals = JSON.parse(utils.get_cookie('im_livechat_session'));
                cookie_vals.name = data.name;
                cookie_vals.anonymous_name = data.anonymous_name;
                utils.set_cookie('im_livechat_session', JSON.stringify(cookie_vals), 60*60)
                
                self.chat_window.$('.e_livechat_form').remove();
                self.chat_window.$('.e_start_chat_button').parent().remove();
                self.chat_window.$('.o_chat_composer').show();
                self.chat_window.$('.o_mail_thread').show();
                if(content){
                    self.chat_window.trigger('post_message', message, self.channel.id);
                }
                setTimeout(function(){
                    self.send_welcome_message();
                    self.render_messages();
                },1000);
            });
        }
    },
    
    close_chat: function () {
        this.$el.siblings('.livechat_button_img').remove();
        utils.set_cookie('channel_image_' + session_uid, "", -1); // remove cookie
        this._super();
    },
    
    send_message: function (message) {
        var self = this;
        //Sending Loader        
        self.chat_window.$('.o_mail_thread .o_thread_message:last-child').after(QWeb.render('fabula_livechat.Sending_loader',{}));
        self.chat_window.thread.scroll_to();
        if (message.attachment_ids && message.attachment_ids.length) {
            return session
                .rpc("/mail/chat_post_attachment", {
                    uuid: this.channel.uuid,
                    message_content: message.content,
                    message_attachment: message.attachment_ids,
                }).then(function(){
                    self.chat_window.$('.o_mail_thread').remove('loader_wrapper'); //remove sending loader
                    self.chat_window.thread.scroll_to();
                });
        }
        else {
            return session
                .rpc("/mail/chat_post", {uuid: this.channel.uuid, message_content: message.content})
                .then(function () {
                    self.chat_window.$('.o_mail_thread').remove('loader_wrapper'); //remove sending loader
                    self.chat_window.thread.scroll_to();
                });
        }
    },
    
    add_message: function (data, options) {
        var msg = {
            id: data.id,
            attachment_ids: data.attachment_ids,
            author_id: data.author_id,
            body: data.body,
            date: moment(time.str_to_datetime(data.date)),
            is_needaction: false,
            is_note: data.is_note,
            customer_email_data: []
        };

        // Compute displayed author name or email
        msg.displayed_author = msg.author_id && msg.author_id[1] ||
                               this.options.default_username;
        
        //Emoji
        _.each(_.keys(emoji_substitutions), function (key) {
            var escaped_key = String(key).replace(/([.*+?=^!:${}()|[\]\/\\])/g, '\\$1');
            var regexp = new RegExp("(?:^|\\s|<[a-z]*>)(" + escaped_key + ")(?=\\s|$|</[a-z]*>)", "g");
            msg.body = msg.body.replace(regexp, ' <span class="o_mail_emoji">'+emoji_substitutions[key]+'</span>');
        });
        
        // Compute url of attachments
        _.each(msg.attachment_ids, function(a) {
            a.url = '/fabula/content/' + a.id + '?download=true';
        });

        // Compute the avatar_url
        if (msg.author_id && msg.author_id[0]) {
            msg.avatar_src = "/web/image/res.partner/" + msg.author_id[0] + "/image";
        } else {
            msg.avatar_src = "/mail/static/src/img/smiley/avatar.jpg";
        }
        
        if (options && options.prepend) {
            if(! session_uid){
                this.messages.splice(1, 0, msg);
            }
            else{
                this.messages.unshift(msg);
            }
        } else {
                this.messages.push(msg);
        }
        
        //Update Welcome Message Time
        if(this.messages.length > 1 && this.messages[0].id == '_welcome'){
            this.messages[0].date = this.messages[1].date;
        }
        else if(this.messages.length > 1 && this.messages[1].id == '_welcome'){
            this.messages[1].date = this.messages[0].date;
        }
    },
    
    render_messages: function () {
        var self = this;
        
        var should_scroll = !this.chat_window.folded && this.chat_window.thread.is_at_bottom();
        this.chat_window.render(this.messages);
        
        this.chat_window.$('.o_thread_message').each(function(){
            if (parseInt(self.channel.operator_pid) == parseInt($(this).attr('author-id'))){
                $(this).addClass("operator_msg");
                $(this).find('.o_image img').attr('src','/fabula_livechat/static/src/img/operator_attachment.png');
            }
            else{
                $(this).addClass("listener_msg");
                $(this).find(".o_mail_info > strong").html("You");
                $(this).find('.o_image img').attr('src','/fabula_livechat/static/src/img/listener_attachment.png');
            }
        });
        
        if (should_scroll){
            this.chat_window.thread.scroll_to();
        }
    },
});
});

