# © 2019 Intelligenti <http://www.intelligenti.io>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Intel Website Customize",
    "summary": """
        Intel Website Sale""",
    "description": """
        Intel website customize extends irc_web_customized
    """,
    "author": "Intelligenti.io",
    "website": "http://www.intelligenti.io",
    "license": "AGPL-3",
    "category": "website",
    "version": "1.9",
    "depends": ["base", "website_sale", "irc_web_customized"],
    "data": [
        # 'security/ir.model.access.csv',
        "views/assets.xml",
        "views/templates.xml",
    ],
}
