odoo.define('intel_web_customized.events', function(require) {
"use strict";
$(function() {
        // Intel
        $(document).on('click', '.enter_manual_address', function(event){
            var checkBox = event.target;
            var checked = $('.enter_manual_address').is(':checked'); 
            var disabledFields = [
                $('input[name=city]'),
                $('input[name=zip]'),
                $('select[name=state_id]'),
                $('select[name=country_id]'),
            ];
            if (checked){
                for (var disabledField in disabledFields){
                    var disabledField = disabledFields[parseInt(disabledField)];
                    disabledField.prop('readonly', false);
                }
                $('#country_id').css({
                    'pointer-events':'auto',
                    'background': '#ffffff',
                });
                $('#state_id').css({
                    'pointer-events':'auto',
                    'background': '#ffffff',
                });

            }else{
                  for (var disabledField in disabledFields){
                    var disabledField = disabledFields[parseInt(disabledField)];
                    disabledField.prop('readonly', true);
                }
                $('#country_id').css({
                    'pointer-events':'none',
                    'background': '#eeeeee'
                });
                $('#state_id').css({
                    'pointer-events':'none',
                    'background': '#eeeeee'
                });
            }
            
        });

    // $(document).ready(function(){$(':input').live('focus',function(){
    //     $(this).attr('autocomplete', 'off');
    //     });
    // });


    });

    $(document).on('focus', 'input[name="street"]', function(event){
        $('input[name="street"]').attr('autocomplete', 'new-password');
     });
});
