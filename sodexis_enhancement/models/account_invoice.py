# -*- coding: utf-8 -*-

from odoo import models, api, _, fields
from werkzeug import url_encode
from odoo.exceptions import ValidationError
import datetime


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    acquirer_id = fields.Integer(compute="_get_acquirer")

    # @api.model
    # def create(self, values):
    #     reference = values['origin']
    #     reference_sub = reference[:3]
    #     if (reference_sub == 'PLG') or (reference_sub == 'WEB'):
    #         values['x_studio_field_XJgdz'] = datetime.datetime.today().date()
    #         actions = self.env['base.automation']._get_actions(self, ['on_create', 'on_create_or_write'])
    #     record = super(AccountInvoice, self.with_env(actions.env)).create(values)
    #     return record

    @api.one
    def _get_acquirer(self):
        if self.payment_method_id:
                self.acquirer_id = self.payment_method_id.payment_acquirer_id.id
        else:
            self.acquirer_id = None

    @api.onchange('payment_method_id')
    def onchange_payment_method_id(self):
        if self.payment_method_id:
                self.acquirer_id = self.payment_method_id.payment_acquirer_id.id
        else:
            self.acquirer_id = None

    @api.multi
    def payment_url_donation_invoice(self):
        self.ensure_one()
        self.partner_id.validate_partner()
        values_to_pass = dict(self._context.get('params', {}))
        values_to_pass.update({
            'model': self._name,
            'id': self.id,
            'debug': 'assets',
            'action_donation': values_to_pass['action'],
        })
        final_url = "user/payment_method/%s/?%s" % (
            self.partner_id.id, url_encode(values_to_pass))
        return {
            'type': 'ir.actions.act_url',
            'url': final_url,
            'nodestroy': True,
            'target': 'self'
        }

    def _compute_payment_tx_count(self):
        res = super(AccountInvoice, self)._compute_payment_tx_count()
        tx_data = self.env['payment.transaction'].read_group(
            [('invoice_id', 'in', self.ids)],
            ['invoice_id'], ['invoice_id']
        )
        if tx_data:
            mapped_data = dict([(m['invoice_id'][0], m['invoice_id_count'])   for m in tx_data])
            for invoice in self:
                invoice.payment_tx_count += mapped_data.get(invoice.id, 0)


    def action_view_transactions(self):

        action = super(AccountInvoice, self).action_view_transactions()
        if not action.get('res_id'):

            tx = self.env['payment.transaction'].search([('invoice_id', 'in', self.ids)])
            if len(tx) == 1:
                action['res_id'] = tx.ids[0]
                action['view_mode'] = 'form'
            else:
                action['view_mode'] = 'tree,form'
                action['domain'] = [('invoice_id', 'in', self.ids)]

        return action


class AccountPayment(models.Model):
    _inherit = "account.payment"

    def _do_payment(self):
        if self.payment_token_id.acquirer_id.capture_manually:
            raise ValidationError(_('This feature is not available for payment acquirers set to the "Authorize" mode.\n'
                                    'Please use a token from another provider than %s.') % self.payment_token_id.acquirer_id.name)
        reference = "P-%s-%s" % (self.id, datetime.datetime.now().strftime('%y%m%d_%H%M%S'))
        tx = self.env['payment.transaction'].create({
            'amount': self.amount,
            'acquirer_id': self.payment_token_id.acquirer_id.id,
            'type': 'server2server',
            'currency_id': self.currency_id.id,
            'reference': reference,
            'payment_token_id': self.payment_token_id.id,
            'partner_id': self.partner_id.id,
            'partner_country_id': self.partner_id.country_id.id,
            'invoice_id': self.invoice_ids.id,
        })

        s2s_result = tx.s2s_do_transaction()

        if (not s2s_result or tx.state != 'done') and tx.state != 'authorized':
            if not tx.state_message:
                raise ValidationError(_("Payment Transaction Failed. Either the card is expired or the card is no \
                longer valid. Please check with the card owner or Authorize.Net."))
            raise ValidationError(_("Payment transaction failed (%s)") % tx.state_message)

        self.payment_transaction_id = tx
