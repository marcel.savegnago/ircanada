# -*- coding: utf-8 -*-
# Copyright 2017 Sodexis
# License OPL-1 (See LICENSE file for full copyright and licensing details).

from lxml import etree
from uuid import uuid4

from odoo.exceptions import UserError, ValidationError
from odoo.addons.payment_authorize.models.authorize_request import AuthorizeAPI

create_original = AuthorizeAPI.create_customer_profile


def create_customer_profile(self, partner, cardnumber, expiration_date, card_code):
    """Create a payment and customer profile in the Authorize.net backend.

    Creates a customer profile for the partner/credit card combination and links
    a corresponding payment profile to it. Note that a single partner in the Odoo
    database can have multiple customer profiles in Authorize.net (i.e. a customer
    profile is created for every res.partner/payment.token couple).

    :param record partner: the res.partner record of the customer
    :param str cardnumber: cardnumber in string format (numbers only, no separator)
    :param str expiration_date: expiration date in 'YYYY-MM' string format
    :param str card_code: three- or four-digit verification number

    :return: a dict containing the profile_id and payment_profile_id of the
             newly created customer profile and payment profile
    :rtype: dict
    """

    root = self._base_tree('createCustomerProfileRequest')
    profile = etree.SubElement(root, "profile")
    etree.SubElement(profile, "merchantCustomerId").text = 'ODOO-%s-%s' % (partner.id, uuid4().hex[:8])
    etree.SubElement(profile, "description").text = partner.name or None
    etree.SubElement(profile, "email").text = partner.email or None
    payment_profile = etree.SubElement(profile, "paymentProfiles")
    etree.SubElement(payment_profile, "customerType").text = 'business' if partner.is_company else 'individual'
    billTo = etree.SubElement(payment_profile, "billTo")
    etree.SubElement(billTo, "firstName").text = partner.name or None
    etree.SubElement(billTo, "company").text = partner.company_id.name or None
    etree.SubElement(billTo, "address").text = (partner.street or '' + (
        partner.street2 if partner.street2 else '')) or None

    missing_fields = [partner._fields[field].string for field in ['city', 'country_id', 'zip'] if not partner[field]]
    if missing_fields:
        raise ValidationError({'missing_fields': missing_fields})

    etree.SubElement(billTo, "city").text = partner.city
    etree.SubElement(billTo, "state").text = partner.state_id.name or None
    etree.SubElement(billTo, "zip").text = partner.zip
    etree.SubElement(billTo, "country").text = partner.country_id.name or None
    etree.SubElement(billTo, "phoneNumber").text = partner.phone or None
    payment = etree.SubElement(payment_profile, "payment")
    creditCard = etree.SubElement(payment, "creditCard")
    etree.SubElement(creditCard, "cardNumber").text = cardnumber
    etree.SubElement(creditCard, "expirationDate").text = expiration_date
    etree.SubElement(creditCard, "cardCode").text = card_code
    etree.SubElement(root, "validationMode").text = 'liveMode'
    response = self._authorize_request(root)

    # If the user didn't set up authorize.net properly then the response
    # won't contain stuff like customerProfileId and accessing text
    # will raise a NoneType has no text attribute
    msg = response.find('messages')
    if msg is not None:
        rc = msg.find('resultCode')
        if rc is not None and rc.text == 'Error':
            err = msg.find('message')
            err_code = err.find('code').text
            err_msg = err.find('text').text
            raise UserError(
                "Authorize.net Error:\nCode: %s\nMessage: %s"
                % (err_code, err_msg)
            )

    res = dict()
    res['profile_id'] = response.find('customerProfileId').text
    res['payment_profile_id'] = response.find('customerPaymentProfileIdList/numericString').text
    return res


AuthorizeAPI.create_customer_profile = create_customer_profile


def error_check(elem):
    """Check if the response sent by Authorize.net contains an error.

    Errors can be a failure to try the transaction (in that case, the transasctionResponse
    is empty, and the meaningful error message will be in message/code) or a failure to process
    the transaction (in that case, the message/code content will be generic and the actual error
    message is in transactionResponse/errors/error/errorText).

    :param etree._Element elem: the root element of the response that will be parsed

    :rtype: tuple (bool, str)
    :return: tuple containnig a boolean indicating if the response should be considered
             as an error and the most meaningful error message found in it.
    """
    result_code = elem.find('messages/resultCode')
    msg = 'No meaningful error message found, please check logs or the Authorize.net backend'
    has_error = result_code is not None and result_code.text == 'Error'
    if has_error:
        # accumulate the most meangingful error
        error = elem.find('transactionResponse/errors/error')
        error = error if error is not None else elem.find('messages/message')
        if error is not None:
            code = error[0].text
            text = error[1].text
            msg = '%s: %s' % (code, text)
    return (has_error, msg)


create_original_capture = AuthorizeAPI.auth_and_capture


def auth_and_capture(self, token, amount, reference):
    """Authorize and capture a payment for the given amount.

    Authorize and immediately capture a payment for the given payment.token
    record for the specified amount with reference as communication.

    :param record token: the payment.token record that must be charged
    :param str amount: transaction amount (up to 15 digits with decimal point)
    :param str reference: used as "invoiceNumber" in the Authorize.net backend

    :return: a dict containing the response code, transaction id and transaction type
    :rtype: dict
    """
    root = self._base_tree('createTransactionRequest')
    tx = etree.SubElement(root, "transactionRequest")
    etree.SubElement(tx, "transactionType").text = "authCaptureTransaction"
    etree.SubElement(tx, "amount").text = str(amount)
    profile = etree.SubElement(tx, "profile")
    etree.SubElement(profile, "customerProfileId").text = token.authorize_profile
    payment_profile = etree.SubElement(profile, "paymentProfile")
    etree.SubElement(payment_profile, "paymentProfileId").text = token.acquirer_ref
    order = etree.SubElement(tx, "order")
    etree.SubElement(order, "invoiceNumber").text = reference
    etree.SubElement(order, "description").text = token.partner_id.code or ""
    response = self._authorize_request(root)
    res = dict()
    (has_error, error_msg) = error_check(response)
    if has_error:
        res['x_response_code'] = self.AUTH_ERROR_STATUS
        res['x_response_reason_text'] = error_msg
        return res
    res['x_response_code'] = response.find('transactionResponse/responseCode').text
    res['x_trans_id'] = response.find('transactionResponse/transId').text
    res['x_type'] = 'auth_capture'
    return res


AuthorizeAPI.auth_and_capture = auth_and_capture