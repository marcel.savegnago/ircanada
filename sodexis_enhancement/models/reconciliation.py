# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime, timedelta
from authorizenet import apicontractsv1
from authorizenet.apicontrollers import getSettledBatchListController, getTransactionListController
from authorizenet.apicontrollers import *
from odoo.http import request
import pytz
import time
import logging
import os
import sys
import imp

_logger = logging.getLogger(__name__)


class TransactionReconciliation(models.Model):
    _name = 'transaction.reconciliation'

    payment_transaction = fields.Many2one('payment.transaction', string="Transaction")
    status_field = fields.Char(string='Status')
    transaction_id = fields.Char(string='Transaction Id')
    invoice_number = fields.Char(string='Invoice Number')
    transaction_status = fields.Char(string='Transaction Status')
    submit_date = fields.Char(string='Submit Date/Time')
    customer = fields.Char(string='Name')
    card = fields.Char(string='Card')
    payment_method = fields.Char(string='Payment Method')
    payment_amount = fields.Float('Payment Amount')
    settlement_date = fields.Char(string='Settlement Date/Time')
    settlement_amount = fields.Float('Settlement Amount')
    batch_id = fields.Char(string = 'Batch ID')
    business_day = fields.Char(string = 'Business Day')
    auth_amount = fields.Float(string = 'Authorization Amount')
    auth_code = fields.Char(string = 'Authorization Code')
    expiry_date = fields.Char(string = 'Expiry Date')
    card_type = fields.Char(string = 'Card Type')
    card_number = fields.Char(string = 'Card Number')
    transaction_type = fields.Char(string = "Transaction Type")
    customer_ip = fields.Char(string = "Customer IP")



    @api.multi
    def action_transaction_reconciliation(self):
        _logger.info("Transaction Reconciliation cron start ")
        _logger.info(datetime.now())
        created_transaction_list = []
        payment_transaction_obj = self.env['payment.transaction']
        transaction_reconciliation_obj = self.env['transaction.reconciliation']
        batch_ids = self.get_settled_batch_list()
        transactions = self.get_transaction_list(batch_ids)
        if transactions:
            for transaction in transactions:
                if transaction['Transaction Status'] == 'Authorization Only':
                    transactions.remove(transaction)

            for transaction in transactions:
                trans_details = self.get_transaction_details(transaction['Transaction Id'])
                values = {'transaction_id': str(transaction['Transaction Id']),
                          'invoice_number': str(transaction['Invoice Number']),
                          'transaction_status': str(transaction['Transaction Status']),
                          'settlement_amount': float(transaction['Settle Amount']),
                          'batch_id' : str(batch_ids[0]),
                          'settlement_date' : str(trans_details['Settlement Date']).split('.', 1)[0],
                          'submit_date' : str(trans_details['Submit Date']).split('.', 1)[0],
                          'card' : str(transaction['Account Type']),
                          'business_day' : str(trans_details['Settlement Date']).split(' ', 1)[0],
                          'auth_amount' : float(trans_details['Auth Amount']),
                          'auth_code' : str(trans_details['Auth Code']),
                          'expiry_date' : trans_details['Expiry Date'],
                          'card_type' : str(trans_details['Card Type']),
                          'card_number' : str(trans_details['Card Number']),
                          'transaction_type' : str(trans_details['Transaction Type']),
                          'customer_ip' : str(trans_details['Customer IP']),
                          }
                print(values)
                if 'Name' in transaction:
                    values.update({'customer': str(transaction['Name']),})
                created_transaction = transaction_reconciliation_obj.create(values)
                if created_transaction:
                    created_transaction_list.append(created_transaction)
        if created_transaction_list:
            for transaction in created_transaction_list:
                matched_transaction = payment_transaction_obj.search([('reference', '=', transaction.invoice_number),('amount', '=', transaction.settlement_amount),('acquirer_reference', '=', transaction.transaction_id)])
                if matched_transaction:
                    transaction.status_field = 'MATCH'
                    transaction.payment_transaction = matched_transaction.id
                    matched_transaction.status_field = 'MATCH'
                    matched_transaction.authorize_transaction_reconcile = transaction.id

                else:
                    transaction.status_field = 'ERROR'

        _logger.info("== Cron job end ==")


    def get_settled_batch_list(self):
        """get settled batch list"""
        batch_ids = []
        company_id = self.env.user.company_id.sudo()
        payment_acquirer = self.env['payment.acquirer'].search([('company_id', '=', company_id.id),('provider','=','authorize')])
        api_login_id = payment_acquirer.authorize_login
        authorize_transaction_key = payment_acquirer.authorize_transaction_key

        merchantAuth = apicontractsv1.merchantAuthenticationType()
        merchantAuth.name = api_login_id
        merchantAuth.transactionKey = authorize_transaction_key

        settledBatchListRequest = apicontractsv1.getSettledBatchListRequest()
        settledBatchListRequest.merchantAuthentication = merchantAuth
        settledBatchListRequest.includeStatistics = True
        today_date = datetime.now().strftime("%Y-%m-%d")
        #today_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
        start_time = 'T00:00:01Z'
        end_time = 'T23:59:59Z'
        first_settlement_date = today_date + start_time
        last_settlement_date = today_date + end_time
        settledBatchListRequest.firstSettlementDate = first_settlement_date
        settledBatchListRequest.lastSettlementDate = last_settlement_date

        settledBatchListController = getSettledBatchListController(settledBatchListRequest)
        settledBatchListController.execute()

        response = settledBatchListController.getresponse()

        # Work on the response
        if response is not None:
            if response.messages.resultCode == apicontractsv1.messageTypeEnum.Ok:
                if hasattr(response, 'batchList'):
                    print('Successfully retrieved batch list.')
                    for batchEntry in response.batchList.batch:
                        print('Batch Id: %s' % batchEntry.batchId)
                        print('Settlement Time UTC: %s' % batchEntry.settlementTimeUTC)
                        print('Payment Method: %s' % batchEntry.paymentMethod)
                        batch_ids.append(batchEntry.batchId)

                else:
                    if response.messages is not None:
                        print('Failed to get transaction list.')
                        print('Code: %s' % (response.messages.message[0]['code'].text))
                        print('Text: %s' % (response.messages.message[0]['text'].text))
            else:
                if response.messages is not None:
                    print('Failed to get transaction list.')
                    print('Code: %s' % (response.messages.message[0]['code'].text))
                    print('Text: %s' % (response.messages.message[0]['text'].text))
        else:
            print('Error. No response received.')

        return batch_ids

    def get_transaction_list(self, batch_ids):
        """get transaction list for a specific batch"""
        transactions_list = []
        company_id = self.env.user.company_id.sudo()
        payment_acquirer = self.env['payment.acquirer'].search(
            [('company_id', '=', company_id.id), ('provider', '=', 'authorize')])
        api_login_id = payment_acquirer.authorize_login
        authorize_transaction_key = payment_acquirer.authorize_transaction_key
        merchantAuth = apicontractsv1.merchantAuthenticationType()
        merchantAuth.name = api_login_id
        merchantAuth.transactionKey = authorize_transaction_key

        # set sorting parameters
        sorting = apicontractsv1.TransactionListSorting()
        sorting.orderBy = apicontractsv1.TransactionListOrderFieldEnum.id
        sorting.orderDescending = True

        # set paging and offset parameters
        paging = apicontractsv1.Paging()
        # Paging limit can be up to 1000 for this request
        paging.limit = 100
        paging.offset = 1

        transactionListRequest = apicontractsv1.getTransactionListRequest()
        transactionListRequest.merchantAuthentication = merchantAuth
        transactionListRequest.sorting = sorting
        transactionListRequest.paging = paging
        if batch_ids:
            for batch_id in batch_ids:
                transactionListRequest.batchId = str(batch_id)
                transactionListController = getTransactionListController(transactionListRequest)
                transactionListController.execute()

                # Work on the response
                response = transactionListController.getresponse()

                if response is not None:
                    if response.messages.resultCode == apicontractsv1.messageTypeEnum.Ok:
                        if hasattr(response, 'transactions'):
                            print('Successfully retrieved transaction list.')
                            for transaction in response.transactions.transaction:
                                transaction_dict = {}
                                transaction_dict.update({'Transaction Id': transaction.transId,
                                                         'Transaction Status': transaction.transactionStatus,
                                                         'Invoice Number': transaction.invoiceNumber,
                                                         'Settle Amount': transaction.settleAmount,
                                                         'Submit Date' : transaction.submitTimeLocal})
                                if hasattr(transaction, 'firstName'):
                                    transaction_dict.update({'Name': transaction.firstName})
                                if hasattr(transaction, 'accountType'):
                                    transaction_dict.update({'Account Type': transaction.accountType})
                                if hasattr(transaction, 'profile'):
                                    transaction_dict.update({'Customer Profile ID': transaction.profile.customerProfileId})
                                transactions_list.append(transaction_dict)
                                print()
                        else:
                            if response.messages is not None:
                                print('Failed to get transaction list.')
                                print('Code: %s' % (response.messages.message[0]['code'].text))
                                print('Text: %s' % (response.messages.message[0]['text'].text))
                    else:
                        if response.messages is not None:
                            print('Failed to get transaction list.')
                            print('Code: %s' % (response.messages.message[0]['code'].text))
                            print('Text: %s' % (response.messages.message[0]['text'].text))
                else:
                    print('Error. No response received.')


        return transactions_list

    def get_transaction_details(self, transId):

        company_id = self.env.user.company_id.sudo()
        payment_acquirer = self.env['payment.acquirer'].search([('company_id', '=', company_id.id),('provider','=','authorize')])
        api_login_id = payment_acquirer.authorize_login
        authorize_transaction_key = payment_acquirer.authorize_transaction_key

        merchantAuth = apicontractsv1.merchantAuthenticationType()
        merchantAuth.name = api_login_id
        merchantAuth.transactionKey = authorize_transaction_key

        transactionDetailsRequest = apicontractsv1.getTransactionDetailsRequest()
        transactionDetailsRequest.merchantAuthentication = merchantAuth
        transactionDetailsRequest.transId = str(transId)

        transactionDetailsController = getTransactionDetailsController(transactionDetailsRequest)

        transactionDetailsController.execute()

        transactionDetailsResponse = transactionDetailsController.getresponse()

        result = {}

        if transactionDetailsResponse is not None:
            if transactionDetailsResponse.messages.resultCode == apicontractsv1.messageTypeEnum.Ok:
                print('Successfully got transaction details!')

                print('Transaction Id : %s' % transactionDetailsResponse.transaction.transId)
                print('Transaction Type : %s' % transactionDetailsResponse.transaction.transactionType)
                print('Transaction Status : %s' % transactionDetailsResponse.transaction.transactionStatus)
                print('Auth Amount : %.2f' % transactionDetailsResponse.transaction.authAmount)
                print('Settle Amount : %.2f' % transactionDetailsResponse.transaction.settleAmount)
                print('Settlement Date/Time Local : %s' % str(transactionDetailsResponse.transaction.batch.settlementTimeLocal).replace('T', ' '))
                print('Submit Date/Time Local : %s' % str(transactionDetailsResponse.transaction.submitTimeLocal).replace('T', ' '))
                result.update({'Transaction Id': transactionDetailsResponse.transaction.transId,
                               'Transaction Type' : transactionDetailsResponse.transaction.transactionType,
                               'Transaction Status' : transactionDetailsResponse.transaction.transactionStatus,
                               'Auth Amount' : transactionDetailsResponse.transaction.authAmount,
                               'Settle Amount' : transactionDetailsResponse.transaction.settleAmount,
                               'Settlement Date' : str(transactionDetailsResponse.transaction.batch.settlementTimeLocal).replace('T', ' '),
                               'Submit Date' : str(transactionDetailsResponse.transaction.submitTimeLocal).replace('T', ' '),
                               'Auth Code' : (transactionDetailsResponse.transaction.authCode \
                                  if transactionDetailsResponse.transaction.transactionStatus != ("voided" and "declined") \
                                  else "N/A"),
                               'Expiry Date' : transactionDetailsResponse.transaction.payment.creditCard.expirationDate,
                               'Card Type' : transactionDetailsResponse.transaction.payment.creditCard.cardType,
                               'Card Number' : transactionDetailsResponse.transaction.payment.creditCard.cardNumber,
                               'Customer IP' : transactionDetailsResponse.transaction.customerIP \
                                  if transactionDetailsResponse.transaction.transactionStatus != "voided" \
                                  else "N/A"
                               })
                if hasattr(transactionDetailsResponse.transaction, 'tax') == True:
                    print('Tax : %s' % transactionDetailsResponse.transaction.tax.amount)
                if hasattr(transactionDetailsResponse.transaction, 'profile'):
                    print('Customer Profile Id : %s' % transactionDetailsResponse.transaction.profile.customerProfileId)

                if transactionDetailsResponse.messages is not None:
                    print('Message Code : %s' % transactionDetailsResponse.messages.message[0]['code'].text)
                    print('Message Text : %s' % transactionDetailsResponse.messages.message[0]['text'].text)
            else:
                if transactionDetailsResponse.messages is not None:
                    print('Failed to get transaction details.\nCode:%s \nText:%s' % (
                    transactionDetailsResponse.messages.message[0]['code'].text,
                    transactionDetailsResponse.messages.message[0]['text'].text))

        return result

class PaymentTransaction(models.Model):

    _inherit = 'payment.transaction'

    status_field = fields.Char(string='Status')
    authorize_transaction_reconcile = fields.Many2one('transaction.reconciliation', string="Authorize Transaction")
