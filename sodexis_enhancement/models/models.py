# -*- coding: utf-8 -*-

import logging
import datetime
import uuid
import time
import traceback

from collections import Counter
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import format_date
from werkzeug import url_encode
from odoo.addons import decimal_precision as dp
from odoo.addons.payment_authorize.models.authorize_request import AuthorizeAPI
from odoo.addons.payment.models.payment_acquirer import ValidationError
from datetime import date
#from datetime import datetime
from pytz import timezone


_logger = logging.getLogger(__name__)


_logger = logging.getLogger(__name__)


class SaleSubscription(models.Model):
    _inherit = "sale.subscription"

    payment_method_id = fields.Many2one(
        'account.journal', domain=[('at_least_one_inbound', '=', True),
                                   ('type', 'in', ['bank', 'cash'])
                                   ],
        string='Payment Method')

    # acquirer_id = fields.Integer(compute="_get_acquirer")

    # hide_bank = fields.Boolean(compute="_hide_bank_account")
    #
    # @api.one
    # def _hide_bank_account(self):
    #     if self.payment_method_id:
    #         if self.payment_method_id.name == "Authorize.Net":
    #             self.hide_bank = True
    #         else:
    #             self.hide_bank = False
    #     else:
    #         self.hide_bank = False


    # @api.one
    # def _get_acquirer(self):
    #     if self.payment_method_id:
    #             self.acquirer_id = self.payment_method_id.payment_acquirer_id.id
    #     else:
    #         self.acquirer_id = None
    #
    # payment_token_id = fields.Many2one('payment.token', 'Payment Token',
    #                                    help='If not set, the default payment token of the partner will be used.',
    #                                    domain="[('partner_id','=',partner_id),('acquirer_id','=',acquirer_id) ]",
    #                                    oldname='payment_method_id')


    @api.multi
    def payment_url_invoice_new(self):
        self.ensure_one()
        self.partner_id.validate_partner()
        values_to_pass = dict(self._context.get('params', {}))
        values_to_pass.update({
            'model': self._name,
            'id': self.id,
            'debug': 'assets',
            'action_donation': values_to_pass['action'],
        })
        final_url = "user/payment_method/%s/?%s" % (
            self.partner_id.id, url_encode(values_to_pass))
        return {
            'type': 'ir.actions.act_url',
            'url': final_url,
            'nodestroy': True,
            'target': 'self'
        }

    def _prepare_invoice_line(self, line, fiscal_position):
        if 'force_company' in self.env.context:
            company = self.env['res.company'].browse(self.env.context['force_company'])
        else:
            company = line.analytic_account_id.company_id
            line = line.with_context(force_company=company.id, company_id=company.id)

        account = line.product_id.property_account_income_id
        if not account:
            account = line.product_id.categ_id.property_account_income_categ_id
        account_id = fiscal_position.map_account(account).id

        tax = line.product_id.taxes_id.filtered(lambda r: r.company_id == company)
        tax = fiscal_position.map_tax(tax, product=line.product_id, partner=self.partner_id)
        return {
            'name': line.name,
            'account_id': account_id,
            'account_analytic_id': line.analytic_account_id.analytic_account_id.id,
            'subscription_id': line.analytic_account_id.id,
            'price_unit': line.price_unit or 0.0,
            'discount': line.discount,
            'quantity': line.quantity,
            'uom_id': line.uom_id.id,
            'product_id': line.product_id.id,
            'invoice_line_tax_ids': [(6, 0, tax.ids)],
            'analytic_tag_ids': [(6, 0, line.analytic_account_id.tag_ids.ids)],
            'x_studio_field_YzI5J': line.x_studio_field_7MXC2,
            'x_studio_field_1XDXN': line.x_studio_field_SiMmU.id,
        }

    @api.one
    def _do_payment(self, payment_token, invoice, two_steps_sec=True):
        tx = super(SaleSubscription, self)._do_payment(payment_token, invoice, two_steps_sec)
        if len(tx) == 1:
            invoice.write({'payment_tx_id' : tx[0].id})
        return tx

    def _prepare_invoice_data(self):
        values = super(SaleSubscription,self)._prepare_invoice_data()
        values.update({'payment_method_id': self.payment_method_id.id, 'payment_token_id' : self.payment_token_id.id})
        if self.payment_method_id.is_electronic_payment_method:
            if self.payment_method_id.payment_acquirer_id:
                values.update({'payment_acquirer_id': self.payment_method_id.payment_acquirer_id.id})
        if self.x_studio_field_qkC0P:
            values.update({'x_studio_field_ZdhFd': self.x_studio_field_qkC0P.id})
        if self.x_studio_field_Ad3LC:
            values.update({'x_studio_field_7PrQt': self.x_studio_field_Ad3LC.id})
        if self.x_studio_field_D9BNs:
            values.update({'team_id': self.x_studio_field_D9BNs.id})
        # if self.recurring_next_date:
        #     values.update({'x_studio_field_XJgdz': date.today()})
        if self.x_studio_field_FT9kV:
            values.update({'x_studio_field_5sjV5': self.x_studio_field_FT9kV})
        return values


    def recurring_invoice(self):
        invoice = self._recurring_create_invoice()
        invoice.x_studio_field_5sjV5 = self.x_studio_field_FT9kV
        invoice.x_studio_field_7PrQt = self.x_studio_field_Ad3LC
        invoice.x_studio_field_ZdhFd = self.x_studio_field_qkC0P
        # invoice.x_studio_field_XJgdz = date.today()
        invoice.team_id = self.x_studio_field_D9BNs
        return self.action_subscription_invoice()

    @api.multi
    def _recurring_create_invoice(self, automatic=False):
        auto_commit = self.env.context.get('auto_commit', True)
        cr = self.env.cr
        invoices = self.env['account.invoice']
        current_date = time.strftime('%Y-%m-%d')
        imd_res = self.env['ir.model.data']
        template_res = self.env['mail.template']
        if len(self) > 0:
            subscriptions = self
        else:
            domain = [('recurring_next_date', '<=', current_date),
                      ('state', 'in', ['open', 'pending'])]
            subscriptions = self.search(domain)
        if subscriptions:
            sub_data = subscriptions.read(fields=['id', 'company_id'])
            for company_id in set(data['company_id'][0] for data in sub_data):
                sub_ids = [s['id'] for s in sub_data if s['company_id'][0] == company_id]
                subs = self.with_context(company_id=company_id, force_company=company_id).browse(sub_ids)
                context_company = dict(self.env.context, company_id=company_id, force_company=company_id)
                for subscription in subs:
                    subscription = subscription[0]
                    # Trick to not prefetch other subscriptions, as the cache is currently invalidated at each iteration
                    if automatic and auto_commit:
                        cr.commit()
                    # payment + invoice (only by cron)
                    if subscription.template_id.payment_mandatory and subscription.recurring_total and automatic:
                        try:
                            payment_token = subscription.payment_token_id
                            tx = None
                            if payment_token:
                                invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                                new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                                if new_invoice:
                                    est = timezone('EST')
                                    new_invoice.x_studio_field_XJgdz = datetime.datetime.now(est).date()     # This is method give EST date
                                new_invoice.message_post_with_view('mail.message_origin_link',
                                                                   values={'self': new_invoice, 'origin': subscription},
                                                                   subtype_id=self.env.ref('mail.mt_note').id)
                                tx = subscription._do_payment(payment_token, new_invoice, two_steps_sec=False)[0]
                                # commit change as soon as we try the payment so we have a trace somewhere
                                if auto_commit:
                                    cr.commit()
                                if tx.state in ['done', 'authorized']:
                                    subscription.send_success_mail(tx, new_invoice)
                                    msg_body = 'Automatic payment succeeded. Payment reference: <a href=# data-oe-model=payment.transaction data-oe-id=%d>%s</a>; Amount: %s. Invoice <a href=# data-oe-model=account.invoice data-oe-id=%d>View Invoice</a>.' % (
                                    tx.id, tx.reference, tx.amount, new_invoice.id)
                                    subscription.message_post(body=msg_body)
                                    if auto_commit:
                                        cr.commit()
                                else:
                                    _logger.error('Fail to create recurring invoice for subscription %s',subscription.code)
                                    if auto_commit:
                                        cr.rollback()
                                    new_invoice.unlink()
                            if tx is None or tx.state != 'done':
                                amount = subscription.recurring_total
                                date_close = datetime.datetime.strptime(subscription.recurring_next_date,"%Y-%m-%d") + relativedelta(days=15)
                                close_subscription = current_date >= date_close.strftime('%Y-%m-%d')
                                email_context = self.env.context.copy()
                                email_context.update({
                                    'payment_token': subscription.payment_token_id and subscription.payment_token_id.name,
                                    'renewed': False,
                                    'total_amount': amount,
                                    'email_to': subscription.partner_id.email,
                                    'code': subscription.code,
                                    'currency': subscription.pricelist_id.currency_id.name,
                                    'date_end': subscription.date,
                                    'date_close': date_close.date()
                                })
                                if close_subscription:
                                    _, template_id = imd_res.get_object_reference('sale_subscription',
                                                                                  'email_payment_close')
                                    template = template_res.browse(template_id)
                                    template.with_context(email_context).send_mail(subscription.id)
                                    _logger.debug("Sending Subscription Closure Mail to %s for subscription %s and closing subscription",subscription.partner_id.email, subscription.id)
                                    msg_body = 'Automatic payment failed after multiple attempts. Subscription closed automatically.'
                                    subscription.message_post(body=msg_body)
                                else:
                                    _, template_id = imd_res.get_object_reference('sale_subscription',
                                                                                  'email_payment_reminder')
                                    msg_body = 'Automatic payment failed. Subscription set to "To Renew".'
                                    if (datetime.datetime.today() - datetime.datetime.strptime(
                                            subscription.recurring_next_date, '%Y-%m-%d')).days in [0, 3, 7, 14]:
                                        template = template_res.browse(template_id)
                                        template.with_context(email_context).send_mail(subscription.id)
                                        _logger.debug(
                                            "Sending Payment Failure Mail to %s for subscription %s and setting subscription to pending",
                                            subscription.partner_id.email, subscription.id)
                                        msg_body += ' E-mail sent to customer.'
                                    subscription.message_post(body=msg_body)
                                subscription.write({'state': 'close' if close_subscription else 'pending'})
                            if auto_commit:
                                cr.commit()
                        except Exception:
                            if auto_commit:
                                cr.rollback()
                            # we assume that the payment is run only once a day
                            traceback_message = traceback.format_exc()
                            _logger.error(traceback_message)
                            last_tx = self.env['payment.transaction'].search([('reference', 'like','SUBSCRIPTION-%s-%s' % (subscription.id,datetime.date.today().strftime('%y%m%d')))],limit=1)
                            error_message = "Error during renewal of subscription %s (%s)" % (subscription.code,'Payment recorded: %s' % last_tx.reference if last_tx and last_tx.state == 'done' else 'No payment recorded.')
                            _logger.error(error_message)

                    # invoice only
                    else:
                        try:
                            invoice_values = subscription.with_context(lang=subscription.partner_id.lang)._prepare_invoice()
                            new_invoice = self.env['account.invoice'].with_context(context_company).create(invoice_values)
                            if new_invoice:
                                est = timezone('EST')
                                new_invoice.x_studio_field_XJgdz = datetime.datetime.now(est).date()     # This is method give EST date
                            new_invoice.message_post_with_view('mail.message_origin_link',
                                                               values={'self': new_invoice, 'origin': subscription},
                                                               subtype_id=self.env.ref('mail.mt_note').id)
                            new_invoice.with_context(context_company).compute_taxes()
                            invoices += new_invoice
                            next_date = datetime.datetime.strptime(subscription.recurring_next_date or current_date,
                                                                   "%Y-%m-%d")
                            periods = {'daily': 'days', 'weekly': 'weeks', 'monthly': 'months', 'yearly': 'years'}
                            invoicing_period = relativedelta(
                                **{periods[subscription.recurring_rule_type]: subscription.recurring_interval})
                            new_date = next_date + invoicing_period
                            subscription.write({'recurring_next_date': new_date.strftime('%Y-%m-%d')})
                            if automatic and auto_commit:
                                cr.commit()
                        except Exception:
                            if automatic and auto_commit:
                                cr.rollback()
                                _logger.exception('Fail to create recurring invoice for subscription %s',
                                                  subscription.code)
                            else:
                                raise
        return invoices

    class PaymentToken(models.Model):
        _inherit = 'payment.token'

        expiry_date = fields.Char(String="Expiry date")
        card_type = fields.Char(string = "Card Type")


        @api.model
        def authorize_create(self, values):
            if values.get('cc_number'):
                values['cc_number'] = values['cc_number'].replace(' ', '')

                acquirer = self.env['payment.acquirer'].browse(values['acquirer_id'])
                expiry = str(values['cc_expiry'][:2]) + str(values['cc_expiry'][-2:])
                partner = self.env['res.partner'].browse(values['partner_id'])
                transaction = AuthorizeAPI(acquirer)
                res = transaction.create_customer_profile(partner, values['cc_number'], expiry, values['cc_cvc'])
                print(str(values['cc_brand']))
                card_code = self._convert_name2code(str(values['cc_brand']))

                if res.get('profile_id') and res.get('payment_profile_id'):
                    return {
                        'authorize_profile': res.get('profile_id'),
                        'name': 'XXXXXXXXXXXX%s - %s - %s - %s' % (values['cc_number'][-4:], values['cc_expiry'], \
                                                         card_code, values['cc_holder_name']),
                        'acquirer_ref': res.get('payment_profile_id'),
                        'expiry_date': values['cc_expiry'],
                        'card_type' : str(values['cc_brand']).capitalize()
                    }
                else:
                    raise ValidationError(_('The Customer Profile creation in Authorize.NET failed.'))
            else:
                return values

        def _convert_name2code(self, name):

            if name == 'amex':
                return 'A'
            elif name == 'discover':
                return 'D'
            elif name == 'mastercard':
                return 'M'
            elif name == 'visa':
                return 'V'