# -*- coding: utf-8 -*-
{
    'name': "Sodexis Enhancement",

    'summary': """
        This module has been design to make enhancement for Sodexis.""",

    'description': """
        Long description of module's purpose
    """,

    'author': "KNYSYS LLC",
    'website': "http://knysys.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale_subscription', 'payment_authorize_addcard', 'payment_authorize_backend',\
                'payment_authorize_refund', 'sod_sale_payment_method', 'account', 'payment', 'website_sale',
                'web_fontawesome', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/email_template.xml',
        'views/reconciliation.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
