# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website.controllers.main import Website
from odoo.addons.portal.controllers.web import Home
from odoo.addons.website_sale_options.controllers.main import WebsiteSaleOptions
import json
import time
from datetime import datetime

class IrcWebCustomized(http.Controller):

    @http.route('/donate-now', type='http', auth='public', website=True)
    def render_example_page(self):
        return http.request.render('irc_web_customized.donate-now', {})

    @http.route(['/donate-now/product'], type='http', auth="public", website=True)
    def product(self, **kwargs):
        radio_buttons = ''
        prod_var = []
        product_tmpl_id = int(kwargs['product_tmpl_id'])
        is_variant = (kwargs['is_variant'])
        product_tmpl = request.env['product.template'].search([('id', '=', product_tmpl_id)])
        product = request.env['product.product'].search([('product_tmpl_id', '=', product_tmpl.id)], limit=1)
        values = {'product_id': product.id,
                  'product_name': product.name}
        if is_variant == 'true':
            product_variant_ids = request.env['product.product'].search([('product_tmpl_id', '=', product_tmpl.id)], order='id ASC')
            if product_variant_ids:
                for variant in (product_variant_ids):
                    data_content = ''
                    if variant.group_country:
                        for country in variant.group_country:
                            data_content += country.name+''' &lt;br&gt;'''
                        data_content = data_content[:-11]
                        span = '''<span data-toggle="popover" title="'''+str(variant.attribute_value_ids.name)+'''" data-html="true" data-placement="bottom" data-content="'''+data_content+'''"><img src="/irc_web_customized/static/description/info.png" class="radi0" />'''
                        radio_buttons += '''<li>
                                    <label>
                                    <input id='''+str(variant.id)+''' type="radio" name="rd-group" class="groupa"/>
                                    <div id='''+str(int(variant.variant_quantity))+''' class="radio-group" >'''+str(variant.attribute_value_ids.name)+'''</div>
                                    </label>
                                    </li>'''+span+'''
                                    </span>'''
                    else:
                        radio_buttons += '''<li>
                                            <label>
                                            <input id=''' + str(variant.id) + ''' type="radio" name="rd-group" class="groupa"/>
                                            <div id=''' + str(int(variant.variant_quantity)) + ''' class="radio-group" >''' + str(variant.attribute_value_ids.name) + '''</div>
                                            </label>
                                            </li>'''
                ul = '<ul>'
                radio_buttons += '</ul>'
                ul += radio_buttons
                return http.Response(json.dumps({"values": values, 'form_string': ul}),
                                     content_type='application/json;charset=utf-8', status=200)
        else:
            return http.Response(json.dumps({"values": values}),
                                 content_type='application/json;charset=utf-8', status=200)


class WebsiteSaleController(WebsiteSale):

    @http.route(['/shop/address'], type='http', methods=['GET', 'POST'], auth="public", website=True)
    def address(self, **kw):
        print ("====/shop/address===")
        Partner = request.env['res.partner'].with_context(show_address=1).sudo()
        order = request.website.sale_get_order()

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        mode = (False, False)
        def_country_id = order.partner_id.country_id
        values, errors = {}, {}

        partner_id = int(kw.get('partner_id', -1))

        # IF PUBLIC ORDER
        if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
            mode = ('new', 'billing')
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                def_country_id = request.env['res.country'].search([('code', '=', country_code)], limit=1)
            else:
                def_country_id = request.website.user_id.sudo().country_id
        # IF ORDER LINKED TO A PARTNER
        else:
            if partner_id > 0:
                if partner_id == order.partner_id.id:
                    mode = ('edit', 'billing')
                else:
                    shippings = Partner.search([('id', 'child_of', order.partner_id.commercial_partner_id.ids)])
                    if partner_id in shippings.mapped('id'):
                        mode = ('edit', 'shipping')
                    else:
                        return Forbidden()
                if mode:
                    values = Partner.browse(partner_id)
            elif partner_id == -1:
                mode = ('new', 'shipping')
            else:  # no mode - refresh without post?
                return request.redirect('/shop/checkout')

        # IF POSTED
        if 'submitted' in kw:
            if 'name' in kw.get('field_required') and 'opt-corporation' in kw:
                kw['field_required'] = kw.get('field_required').replace('name', 'company_name')
                kw['name'] = kw['company_name']
            if 'name' in kw.get('field_required') and 'opt-individual' in kw:
                kw['field_required'] = kw.get('field_required').replace('name', 'firstname,lastname')
                if kw['middlename']:
                    kw['name'] = kw['firstname'] + ' ' + kw['middlename'] + ' ' + kw['lastname']
                else:
                    kw['name'] = kw['firstname'] + ' ' + kw['lastname']
            pre_values = self.values_preprocess(order, mode, kw)
            errors, error_msg = self.checkout_form_validate(mode, kw, pre_values)
            post, errors, error_msg = self.values_postprocess(order, mode, pre_values, errors, error_msg)

            if errors:
                errors['error_message'] = error_msg
                values = kw
            else:
                partner_id = self._checkout_form_save(mode, post, kw)

                if mode[1] == 'billing':
                    order.partner_id = partner_id
                    order.onchange_partner_id()
                elif mode[1] == 'shipping':
                    order.partner_shipping_id = partner_id

                order.message_partner_ids = [(4, partner_id), (3, request.website.partner_id.id)]
                if not errors:
                    return request.redirect(kw.get('callback') or '/shop/checkout')

        country = 'country_id' in values and values['country_id'] != '' and request.env['res.country'].browse(
            int(values['country_id']))
        country = country and country.exists() or def_country_id
        render_values = {
            'website_sale_order': order,
            'partner_id': partner_id,
            'mode': mode,
            'checkout': values,
            'country': country,
            'countries': country.get_website_sale_countries(mode=mode[1]),
            "states": country.get_website_sale_states(mode=mode[1]),
            'error': errors,
            'callback': kw.get('callback'),
        }
        return request.render("irc_web_customized.address", render_values)

    def _checkout_form_save(self, mode, checkout, all_values):
        Partner = request.env['res.partner']
        if mode[0] == 'new':
            country = request.env['res.country'].search([('id', '=', all_values['country_id'])], limit=1)
            if 'opt-individual' in all_values:
                if country.name.lower() == 'canada':
                    checkout['x_studio_field_UsVrp'] = 'Canadian Donor'
                    checkout['is_company'] = False
                else:
                    checkout['x_studio_field_UsVrp'] = 'Foreign Donor'
                    checkout['is_company'] = False
            else:
                if country.name.lower() == 'canada':
                    checkout['x_studio_field_UsVrp'] = 'Canadian Corporation'
                    checkout['is_company'] = True
                else:
                    checkout['x_studio_field_UsVrp'] = 'Foreign Corporation'
                    checkout['is_company'] = True

            partner_id = Partner.sudo().create(checkout).id
        elif mode[0] == 'edit':
            partner_id = int(all_values.get('partner_id', 0))
            if partner_id:
                # double check
                order = request.website.sale_get_order()
                shippings = Partner.sudo().search([("id", "child_of", order.partner_id.commercial_partner_id.ids)])
                if partner_id not in shippings.mapped('id') and partner_id != order.partner_id.id:
                    return Forbidden()

                country = request.env['res.country'].search([('id', '=', all_values['country_id'])], limit=1)
                if 'opt-individual' in all_values:
                    if country.name.lower() == 'canada':
                        checkout['x_studio_field_UsVrp'] = 'Canadian Donor'
                        checkout['is_company'] = False
                    else:
                        checkout['x_studio_field_UsVrp'] = 'Foreign Donor'
                        checkout['is_company'] = False
                else:
                    if country.name.lower() == 'canada':
                        checkout['x_studio_field_UsVrp'] = 'Canadian Corporation'
                        checkout['is_company'] = True
                    else:
                        checkout['x_studio_field_UsVrp'] = 'Foreign Corporation'
                        checkout['is_company'] = True
                Partner.browse(partner_id).sudo().write(checkout)
        return partner_id


class WebsiteSaleOptions(WebsiteSaleOptions):

    @http.route(['/shop/cart/update_option'], type='http', auth="public", methods=['POST'], website=True,
                multilang=False)
    def cart_options_update_json(self, product_id, add_qty=1, set_qty=0, goto_shop=None, lang=None,frequency=None,**kw):

        res = super(WebsiteSaleOptions, self).cart_options_update_json(product_id,add_qty=add_qty, set_qty=set_qty, goto_shop=goto_shop,\
                                                                 lang=lang, **kw)

        order = request.website.sale_get_order()
        order.write({'is_website_order': True})
        order.write({'recurring_payment_freq': frequency})

        if frequency:
            frequency = int(frequency)
            order.write({'recurring_payment_freq': frequency})

        value = {}
        value['cart_quantity'] = order.cart_quantity
        return http.Response(json.dumps({"value": value}), content_type='application/json;charset=utf-8', status=res._status_code)


class Website(Home):

    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        homepage = request.website.homepage_id
        if homepage and (homepage.sudo().is_visible or request.env.user.has_group('base.group_user')) and homepage.url != '/':
            return request.env['ir.http'].reroute(homepage.url)

        website_page = request.env['ir.http']._serve_page()
        if website_page:
            return http.request.render('irc_web_customized.donate-now', {})
        else:
            top_menu = request.website.menu_id
            first_menu = top_menu and top_menu.child_id and top_menu.child_id.filtered(lambda menu: menu.is_visible)
            if first_menu and first_menu[0].url not in ('/', '') and (not (first_menu[0].url.startswith(('/?', '/#', ' ')))):
                return request.redirect(first_menu[0].url)

        raise request.not_found()
