# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import odoo
import time
from odoo.addons import decimal_precision as dp


class ProductProduct(models.Model):
    _inherit = 'product.product'

    group_country = fields.Many2many('variant.country', 'product_variant_country_rel', 'prod_variant', 'country')
    variant_quantity = fields.Float(
        'Quantity (as price)', compute='_compute_product_variant_quantity',
        digits=dp.get_precision('Product Price'),
        help="This exhibits the quantities of variant products but acts as sales price of them. This is managed from the product template. Click on the 'Variant Prices' button to set the quantities as price.")


    @api.depends('attribute_value_ids.price_ids.price_extra', 'attribute_value_ids.price_ids.product_tmpl_id')
    def _compute_product_variant_quantity(self):
        # TDE FIXME: do a real multi and optimize a bit ?
        for product in self:
            quantity_as_price = 0.0
            for attribute_price in product.mapped('attribute_value_ids.price_ids'):
                if attribute_price.product_tmpl_id == product.product_tmpl_id:
                    quantity_as_price += attribute_price.quantity_as_price
            product.variant_quantity = quantity_as_price

class VariantCountry(models.Model):
    _name = "variant.country"

    name = fields.Char(string='Country')