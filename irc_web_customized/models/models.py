# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import odoo
import time


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    web_so_donation_type = fields.Many2one(string='Donation Type',
                                      comodel_name='x_donation_type')

    web_so_trace = fields.Many2one(string='Trace',
                                    comodel_name='x_trace')

    web_so_appeal = fields.Many2one(string='Appeal',
                                   comodel_name='crm.team')

    web_so_batch = fields.Char(string='Batch')


    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()

        ICPSudo = self.env['ir.config_parameter'].sudo()
        if self.web_so_donation_type:
            ICPSudo.set_param("irc_web_customized.web_so_donation_type", self.web_so_donation_type.id)
        if self.web_so_trace.id:
            ICPSudo.set_param("irc_web_customized.web_so_trace", self.web_so_trace.id)
        if self.web_so_appeal.id:
            ICPSudo.set_param("irc_web_customized.web_so_appeal", self.web_so_appeal.id)
        if self.web_so_batch:
            ICPSudo.set_param("irc_web_customized.web_so_batch", self.web_so_batch)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()

        ICPSudo = self.env['ir.config_parameter'].sudo()

        if ICPSudo.get_param('irc_web_customized.web_so_donation_type'):
            res.update(
                web_so_donation_type=int(ICPSudo.get_param('irc_web_customized.web_so_donation_type')),
            )

        if ICPSudo.get_param('irc_web_customized.web_so_trace'):
            res.update(
                web_so_trace=int(ICPSudo.get_param('irc_web_customized.web_so_trace')),
            )

        if ICPSudo.get_param('irc_web_customized.web_so_appeal'):
            res.update(
                web_so_appeal=int(ICPSudo.get_param('irc_web_customized.web_so_appeal')),
            )
        if ICPSudo.get_param('irc_web_customized.web_so_batch'):
            res.update(
                web_so_batch=ICPSudo.get_param('irc_web_customized.web_so_batch'),
            )
        return res



class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_website_order = fields.Boolean(default=False)
    recurring_payment_freq = fields.Integer()

    @api.model
    def _prepare_subscription_data(self, template):
        values = super(SaleOrder, self)._prepare_subscription_data(template)
        date_order = datetime.strptime(self.date_order, '%Y-%m-%d %H:%M:%S')

        if self.is_website_order:

            ICPSudo = self.env['ir.config_parameter'].sudo()
            param = ICPSudo.get_param \
                ('irc_web_customized.web_so_donation_type')
            if ICPSudo.get_param('irc_web_customized.web_so_donation_type'):
                values.update({'x_studio_field_qkC0P': int(ICPSudo.get_param\
                                                               ('irc_web_customized.web_so_donation_type'))})

            if ICPSudo.get_param('irc_web_customized.web_so_trace'):
                values.update({'x_studio_field_Ad3LC': int(ICPSudo.get_param\
                                                            ('irc_web_customized.web_so_trace'))})
                if ICPSudo.get_param('irc_web_customized.web_so_appeal'):
                    values.update({'x_studio_field_D9BNs': int(ICPSudo.get_param\
                                                                   ('irc_web_customized.web_so_appeal'))})
            if ICPSudo.get_param('irc_web_customized.web_so_batch'):
                date_str = datetime.now().strftime('%Y-%m-%d')
                values.update({'x_studio_field_FT9kV': ICPSudo.get_param('irc_web_customized.web_so_batch') +\
                                                       ' ' +date_str})

            elif not ICPSudo.get_param('irc_web_customized.web_so_batch'):
                date_str = datetime.now().strftime('%Y-%m-%d')
                values.update({'x_studio_field_FT9kV': date_str})

            if self.recurring_payment_freq:
                current_month = int(date_order.strftime("%m"))
                if self.recurring_payment_freq == 15:

                    if int(date_order.strftime("%d")) > self.recurring_payment_freq:
                        next_month = current_month + 1 if current_month + 1 <= 12 else 1
                        adjusted_date = date_order.replace(month=next_month, day=15)
                    else:
                        adjusted_date = date_order.replace(month=current_month, day=15)

                elif self.recurring_payment_freq == 1:


                    if int(date_order.strftime("%d")) > self.recurring_payment_freq:
                        next_month = current_month + 1 if current_month + 1 <= 12 else 1
                        adjusted_date = date_order.replace(month=next_month, day=1)
                    else:
                        adjusted_date = date_order.replace(month=current_month, day=1)

                values.update({'date_start': adjusted_date})

        return values



    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        values = super(SaleOrder, self).action_invoice_create(grouped=grouped, final=final)
        ICPSudo = self.env['ir.config_parameter'].sudo()
        for order in self:
            param = ICPSudo.get_param('irc_web_customized.web_so_donation_type')
            if order.is_website_order:

                for invoice in order.invoice_ids:
                    query = 'UPDATE account_invoice SET'
                    updated = 0
                    if ICPSudo.get_param('irc_web_customized.web_so_donation_type'):

                        query += ' "x_studio_field_ZdhFd"=%s'%(param)
                        updated = 1
                        #Todo Convert all the raw queries into orm
                        #invoice.write({'"x_studio_field_ZdhFd"': param})

                    if ICPSudo.get_param('irc_web_customized.web_so_trace'):
                        #invoice.write({'x_studio_field_7PrQt': int(ICPSudo.get_param\
                        #                                               ('irc_web_customized.web_so_trace'))})

                        if updated == 1:
                            query+= ',"x_studio_field_7PrQt"=%s'%(ICPSudo.get_param('irc_web_customized.web_so_trace'))
                        else:
                            query += ' "x_studio_field_7PrQt"=%s'%(ICPSudo.get_param('irc_web_customized.web_so_trace'))
                            updated = 1

                    if updated ==1:

                        query += ' WHERE id=%s'%(str(invoice.id))
                        invoice._cr.execute(query)



                    if ICPSudo.get_param('irc_web_customized.web_so_appeal'):
                        invoice.write({'team_id': int(ICPSudo.get_param\
                                                                           ('irc_web_customized.web_so_appeal'))})
                    if ICPSudo.get_param('irc_web_customized.web_so_batch'):
                        date_str = datetime.now().strftime('%Y-%m-%d')
                        invoice.write({'x_studio_field_5sjV5': ICPSudo.get_param('irc_web_customized.web_so_batch') +\
                                                               ' ' +date_str})

                    elif not ICPSudo.get_param('irc_web_customized.web_so_batch'):
                        date_str = datetime.now().strftime('%Y-%m-%d')
                        invoice.write({'x_studio_field_5sjV5': date_str})


        return values
