# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.addons import decimal_precision as dp


class ProductAttributevalue(models.Model):
    _inherit = "product.attribute.value"

    quantity_as_price = fields.Float(
        'Quantity (as price)', compute='_quantity_as_price', inverse='_set_quantity_as_price',
        default=0.0, digits=dp.get_precision('Product Price'),
        help="Price Extra: Extra price for the variant with this attribute value on sale price. eg. 200 price extra, 1000 + 200 = 1200.")

    @api.one
    def _quantity_as_price(self):
        if self._context.get('active_id'):
            price = self.price_ids.filtered(lambda price: price.product_tmpl_id.id == self._context['active_id'])
            self.quantity_as_price = price.quantity_as_price
        else:
            self.quantity_as_price = 0.0

    def _set_quantity_as_price(self):
        if not self._context.get('active_id'):
            return

        AttributePrice = self.env['product.attribute.price']
        prices = AttributePrice.search(
            [('value_id', 'in', self.ids), ('product_tmpl_id', '=', self._context['active_id'])])
        updated = prices.mapped('value_id')
        if prices:
            prices.write({'quantity_as_price': self.quantity_as_price})
        else:
            for value in self - updated:
                AttributePrice.create({
                    'product_tmpl_id': self._context['active_id'],
                    'value_id': value.id,
                    'quantity_as_price': self.quantity_as_price,
                })


class ProductAttributePrice(models.Model):
    _inherit = "product.attribute.price"

    quantity_as_price = fields.Float('Quantity (as price)', digits=dp.get_precision('Product Price'))