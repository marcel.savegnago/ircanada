# -*- coding: utf-8 -*-
{
    'name': "irc_web_customized",

    'summary': """
        Islamic Relief Canada Website Enhancement """,

    'description': """
        This module is designed to improve the customer e-commerce shopping experience at 
        Islamic Relief Canada's Website
    """,

    'author': "KNYSYS LLC",
    'website': "https://www.knysys.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website_enterprise', 'website', 'website_sale', 'product'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/product_views.xml',
        'views/product_attribute_views.xml',
    ],
    'qweb': [

    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}