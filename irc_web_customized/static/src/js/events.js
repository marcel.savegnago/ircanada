odoo.define('irc_web_customized.events', function(require) {
"use strict";

var ajax = require('web.ajax');
require('web.dom_ready');
var weContext = require("web_editor.context");
var product_id;
var product_tmpl_id;
var product_variants;
var prod_var_id1;
var prod_var_id2;
var prod_var_id3;
var form;
var add_another_donation_is_pressed = false;

var modified_shopping_cart_link = $('ul#top_menu li a[href$="/shop/cart"]');
var modified_shopping_cart_link_counter;
modified_shopping_cart_link.popover({
    trigger: 'manual',
    animation: true,
    html: true,
    title: function () {
        return _t("My Cart");
    },
    container: 'body',
    placement: 'auto',
    template: '<div class="popover mycart-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
}).on("mouseenter_modified",function () {
    if(true){
        var self = this;
        clearTimeout(modified_shopping_cart_link_counter);
        modified_shopping_cart_link.not(self).popover('hide');
        modified_shopping_cart_link_counter = setTimeout(function(){
            $.get("/shop/cart", {'type': 'popover'})
                .then(function (data) {
                    $(self).data("bs.popover").options.content =  data;
                    $(self).popover("show");
                });
        }, 1000);
    }
}).on("mouseleave_modified", function () {
    if (true){
        var self = this;
        setTimeout(function () {
              $(self).popover('hide');
        }, 6000);
    }
});


$(function() {

        $(document).on("click", function(){
            $('[data-toggle="popover"]').popover();
        });

        $(".fund_buttons").on("click",function(e) {
        e.preventDefault();
        $("#non-variant-product-form").hide();
        $("#variant-product-form").hide();
        $("#checkout-btn").hide()
        $(".TypeOfFunds").hide();
        $("#"+this.id+"Funds").show();
        $("#non-variant-form").hide();
        $('html,body').animate({
        scrollTop: $("#"+this.id+"Funds").offset().top},
        'slow');

        $( ".fund_buttons" ).each(function() {
            $( this ).attr('value', '');
            $(this).removeClass("freq-btn-active" )
        });

        $('#'+this.id).attr('value','selected');
        $('#'+this.id).addClass('freq-btn-active')

        });

        $(".donation_type_variant").on("click",function(e) {
            e.preventDefault();
            console.log(this.id);
            $("#non-variant-product-form").hide();
            $("#variant-product-form").hide();
            $("#checkout-btn").hide()

            $( ".donation_type_non_variant" ).each(function() {
            $( this ).attr('value', '');
            $(this).removeClass("freq-btn-active" )
            });

            $( ".donation_type_variant" ).each(function() {
            $( this ).attr('value', '');
            $(this).removeClass("freq-btn-active" )
            });

            $('#'+this.id).attr('value','selected');
            $('#'+this.id).addClass('freq-btn-active')

            product_tmpl_id = this.id

            $.ajax({
                url: "/donate-now/product",
                type: "GET",
                data: {
                    product_tmpl_id: product_tmpl_id,
                    is_variant: true,
                },
                dataType: 'json',
                traditional: true,
                success: function(data){
                    $('.radi0-bg').empty();
                    product_id = data.values.product_id
                    form = data.form_string
                    $('.donation_name').text(data.values.product_name);
                    $('.radi0-bg').append(form);
                    $("#non-variant-product-form").hide();
                    $("#variant-product-form").show();
                    $("#checkout-btn").show()
                    $('html,body').animate({
                    scrollTop: $("#variant-product-form").offset().top},
                    'slow');
                },
                error: function(data){}

            });
        });

        $(".donation_type_non_variant").on("click",function(e) {
            e.preventDefault();

            $("#variant-product-form").hide();
            $("#non-variant-product-form").hide();
            $("#checkout-btn").hide()

            $( ".donation_type_variant" ).each(function() {
            $( this ).attr('value', '');
            $(this).removeClass("freq-btn-active" )
            });

            $( ".donation_type_non_variant" ).each(function() {
            $( this ).attr('value', '');
            $(this).removeClass("freq-btn-active" )
            });

            $('#'+this.id).attr('value','selected');
            $('#'+this.id).addClass('freq-btn-active')

            product_tmpl_id = this.id

             $.ajax({
                url: "/donate-now/product",
                type: "GET",
                data: {
                    product_tmpl_id: product_tmpl_id,
                    is_variant: false,
                },
                dataType: 'json',
                traditional: true,
                success: function(data){
                    product_id = data.values.product_id
                    $('.donation_name').text(data.values.product_name);

                    $("#variant-product-form").hide();
                    $("#non-variant-product-form").show();

                    $("#checkout-btn").show()
                    $('html,body').animate({
                    scrollTop: $("#non-variant-product-form").offset().top},
                    'slow');
                },
                error: function(data){}

            });
        });

        $(".add_to_cart").on("click",function(e) {
            e.preventDefault();
            add_donation(e, false);
        });

        $(".add_to_cart_and_proceed").on("click",function(e) {
            add_donation(e, true);
        });

        function add_donation(e, proceed) {

              if ($("#variant-product-form").is(":visible") == true)
            {
                var selected_var = $("input[name='rd-group']:checked").attr('id');
                product_id = selected_var;
            }

             ajax.jsonRpc("/shop/modal", 'call', {
                    'product_id': product_id,
                    'kwargs': {
                       'context': _.extend({'quantity': +document.getElementById('add_qty').value}, weContext.get())
                    },
                }).then(function (modal) {

                    $.ajax({
                        url: "/shop/cart/update_option",
                        type: "POST",
                        data: {
                            product_id: product_id,
                            lang: weContext.get().lang,
                            csrf_token: odoo.csrf_token,
                            add_qty: +document.getElementById('add_qty').value,
                        },
                        dataType: 'json',
                        traditional: true,
                        success: function(quantity){

                            e.preventDefault();
                            if (proceed == true){
                                window.location="/shop/cart";
                            }
                            else{
                                modified_shopping_cart_link.trigger("mouseenter_modified");
                                modified_shopping_cart_link.trigger("mouseleave_modified");

                                $('html,body').animate({
                                    scrollTop: 0},
                                    2000);

                                $( ".donation_type_variant" ).each(function() {
                                    $( this ).attr('value', '');
                                    $(this).removeClass("freq-btn-active" )
                                });

                                $( ".donation_type_non_variant" ).each(function() {
                                    $( this ).attr('value', '');
                                    $(this).removeClass("freq-btn-active" )
                                });

                                $( ".fund_buttons" ).each(function() {
                                    $( this ).attr('value', '');
                                    $(this).removeClass("freq-btn-active" )
                                });

                                document.getElementById('add_qty').value = ""
                                $("#add_qty").change();
                                $('.radio-group').removeClass('bg-color');

                                var $q = $(".my_cart_quantity");
                                $q.parent().parent().removeClass("hidden", !quantity);
                                $q.html(quantity.value["cart_quantity"]);
                                add_another_donation_is_pressed = true;
                            }
                        },
                        error: function(data){}

                    });

              });
        }

        $(window).on('scroll', function() {
            if($(this).scrollTop() == 0 && add_another_donation_is_pressed) {
                $("#non-variant-product-form").hide();
                $("#variant-product-form").hide();
                $("#checkout-btn").hide();
                $("#checkout-btn").hide();
                $(".TypeOfFunds").hide();
                $("#"+this.id+"Funds").show();
                $("#non-variant-form").hide();
                add_another_donation_is_pressed = false;
            }
        });

        if ($('input[name=opt-individual]:checked').attr('checked') != "checked" && $('input[name=opt-corporation]:checked').attr('checked') != "checked"){
            $('input[name=opt-individual]').attr('checked', true)
        }

        if ($('input[name=opt-individual]:checked').attr('checked') == "checked"){
            $("#div_company_name").hide()
            document.getElementsByName('company_name')[0].value = ""
            $('input[name=opt-corporation]:checked').removeAttr('checked');
        }

        if ($('input[name=opt-corporation]:checked').attr('checked') == "checked"){
            $("#div_individual_firstname").hide()
            $("#div_individual_middlename").hide()
            $("#div_individual_lastname").hide()
            document.getElementsByName('firstname')[0].value = ""
            document.getElementsByName('middlename')[0].value = ""
            document.getElementsByName('lastname')[0].value = ""
            $('input[name=opt-individual]:checked').removeAttr('checked');
        }


        $(".rd-individual").on("click",function(e) {
            $("#div_company_name").hide()
            document.getElementsByName('company_name')[0].value = ""
            $('input[name=opt-corporation]:checked').removeAttr('checked');
            $("#div_individual_firstname").show()
            $("#div_individual_middlename").show()
            $("#div_individual_lastname").show()
        });

        $(".rd-corporation").on("click",function(e) {
            $("#div_individual_firstname").hide()
            $("#div_individual_middlename").hide()
            $("#div_individual_lastname").hide()
            document.getElementsByName('firstname')[0].value = ""
            document.getElementsByName('middlename')[0].value = ""
            document.getElementsByName('lastname')[0].value = ""
            $('input[name=opt-individual]:checked').removeAttr('checked');
            $("#div_company_name").show()
        });

        // This jQuery controls donation buttons visibility
        $("#add_qty").on("change keyup paste click", function(){
            $('.pro-disabled').removeClass('pro-disabled')
            if (document.getElementById('add_qty').value == '' || parseInt(document.getElementById('add_qty').value) <= 0) {
                $('.add_to_cart').addClass('pro-disabled');
                $('.add_to_cart_and_proceed').addClass('pro-disabled');
            }
        });

        $(document).on('click', '.radio-group', function(){
            $('.radio-group').removeClass('bg-color');
            console.log('in function')
            $(this).addClass('bg-color');
            document.getElementById('add_qty').value = $(this)[0].id;
            $("#add_qty").change();
        })

    });
});
