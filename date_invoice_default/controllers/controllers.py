# -*- coding: utf-8 -*-
from odoo import http

class DateInvoiceDefault(http.Controller):
    @http.route('/date_invoice_default/', auth='public')
    def index(self, **kw):
        return "Hello, default"


#     @http.route('/date_invoice_default/date_invoice_default/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('date_invoice_default.listing', {
#             'root': '/date_invoice_default/date_invoice_default',
#             'objects': http.request.env['date_invoice_default.date_invoice_default'].search([]),
#         })

#     @http.route('/date_invoice_default/date_invoice_default/objects/<model("date_invoice_default.date_invoice_default"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('date_invoice_default.object', {
#             'object': obj
#         })