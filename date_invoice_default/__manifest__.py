# -*- coding: utf-8 -*-
{
    'name': "Change Defaults",

    'summary': """
        This module sets the date_invoice field to have todays date as a defaut value""",

    'description': """
        date_invoice field will default to todays date after installing this module. Depends on account.invoice module
    """,

    'author': "Cloudypedia",
    'website': "http://www.cloudypedia.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Addon',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}