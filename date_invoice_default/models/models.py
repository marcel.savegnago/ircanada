# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

# class AccountInvoice(models.Model)
class date_invoice_default(models.Model):

    _inherit = 'account.invoice'

    date_invoice = fields.Date(string='Invoice Date', default=fields.Date.today(),
                                        readonly=True, states={'draft': [('readonly', False)]},
                                        index=True, help="Did it work?", copy=False)


# class type_selection_donors(models.Model):
#
#     _inherit = 'res.partner'
#
#     type = fields.Selection(
#         [('contact', 'Contact'),
#          ('invoice', 'Invoice address'),
#          ('delivery', 'Shipping address'),
#          ('history', 'Historical address'),
#          ("private", "Private Address"),
#          ], string='Address Type',
#         default='history',
#         help="Did it work!!!")

    # type = fields.Selection(selection_add=[('history', 'Historical address')])